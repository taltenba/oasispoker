# OasisPoker
OasisPoker is an application enabling to play Texas hold 'em poker againts AIs or, online, against other human players.

This application has been developed as a personal project in 2015 when I was in high school. I wanted in particular through this project to improve my Java programming skills in addition to experiment with JavaFX library and AI development. Please note I was a total beginner in AI programming and the goal was therefore obviously not to create a very powerful AI.

The developed AI uses a Monte Carlo method to evaluates its odds of winning and chooses its actions depending on the evaluation result.

![Imgur Image](https://i.imgur.com/jfaRL43.png)

## Technologies
* Java 8
* JavaFX 8
* Gson

## Getting started
### Prerequisites
* Windows is recommended. The application should also work on Linux and macOS but has not been tested on those platforms.
* Java Runtime Environment 8

### Installation
An archive containing the compiled application can be download from the [Releases](https://gitlab.com/taltenba/oasispoker/-/releases) page. After extracting all the archive content the game can be launched by running `Launcher.jar`.
```sh
java -jar Launcher.jar
```

## Gallery
<img src="https://i.imgur.com/2WdKwkG.png" width="33%"/>
<img src="https://i.imgur.com/8L0DbuH.png" width="33%"/>
<img src="https://i.imgur.com/BWM6vct.png" width="33%"/>

<img src="https://i.imgur.com/25GJELL.png" width="33%"/>
<img src="https://i.imgur.com/jfaRL43.png" width="33%"/>
<img src="https://i.imgur.com/fp8Cmw8.png" width="33%"/>

<img src="https://i.imgur.com/hAOB92Z.png" width="33%"/>
<img src="https://i.imgur.com/BrL8rb2.png" width="33%"/>

## Authors
* ALTENBACH Thomas - @taltenba
