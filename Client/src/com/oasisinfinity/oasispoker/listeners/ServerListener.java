package com.oasisinfinity.oasispoker.listeners;

import java.util.EventListener;

import com.oasisinfinity.oasispoker.network.Message;

public interface ServerListener extends EventListener {
	public void disconnected(boolean isClientQuit);
	public void newMessage(Message msg);
}
