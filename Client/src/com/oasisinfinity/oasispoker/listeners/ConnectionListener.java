package com.oasisinfinity.oasispoker.listeners;

import com.oasisinfinity.oasispoker.models.ServerInfos;
import com.oasisinfinity.oasispoker.network.Message.Reason;

public interface ConnectionListener {
	void connectionRejected(Reason reason);
	void connectionFailed(Exception e);
	void connected(ServerInfos serverInfos);
	void disconnected(boolean isClientQuit);
	void newPlayersCount(int count);
	void serverEngineReady();
}
