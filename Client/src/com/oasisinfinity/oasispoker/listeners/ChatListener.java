package com.oasisinfinity.oasispoker.listeners;

import com.oasisinfinity.oasispoker.models.ChatMessage;

public interface ChatListener {
	void newChatMessage(ChatMessage msg);
}
