package com.oasisinfinity.oasispoker.listeners;

import com.oasisinfinity.oasispoker.models.ServerInfos;
import com.oasisinfinity.oasispoker.network.Message.Reason;

public abstract class ConnectionAdapter implements ConnectionListener {
	@Override
	public void connectionRejected(Reason reason) { /* Empty */ }
	
	@Override
	public void connectionFailed(Exception e) { /* Empty */ }

	@Override
	public void connected(ServerInfos serverInfos) { /* Empty */ }

	@Override
	public void disconnected(boolean isClientQuit) { /* Empty */ }

	@Override
	public void newPlayersCount(int count) { /* Empty */ }
	
	@Override
	public void serverEngineReady() { /* Empty */ }
}
