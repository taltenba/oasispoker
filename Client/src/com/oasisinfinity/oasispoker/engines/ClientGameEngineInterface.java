package com.oasisinfinity.oasispoker.engines;

import java.util.concurrent.Executor;

import com.oasisinfinity.oasispoker.listeners.ChatListener;
import com.oasisinfinity.oasispoker.listeners.ConnectionListener;
import com.oasisinfinity.oasispoker.listeners.GameProgressListener;
import com.oasisinfinity.oasispoker.models.ChatMessage;
import com.oasisinfinity.oasispoker.models.ServerInfos;
import com.oasisinfinity.oasispoker.network.Message.Reason;

import net.jcip.annotations.NotThreadSafe;

/**
 * All public methods must be called in the interface executor's thread
 * @author OasisInfinity
 */
@NotThreadSafe
public class ClientGameEngineInterface extends GameEngineInterface {
	private final ConnectionListener _connectionListener;
	private final ChatListener _chatListener;
	
	ClientGameEngineInterface(Executor interfaceExec, GameProgressListener gameProgressListener, ConnectionListener connectionListener, ChatListener chatListener) {
		super(interfaceExec, gameProgressListener);
		
		_connectionListener = connectionListener;
		_chatListener = chatListener;
	}
	
	void onConnectionRejected(final Reason reason) {
		if (_connectionListener != null)
			execute(() -> _connectionListener.connectionRejected(reason));
	}
	
	void onConnectionFailed(final Exception e) {
		if (_connectionListener != null)
			execute(() -> _connectionListener.connectionFailed(e));
	}

	void onConnected(final ServerInfos serverInfos) {
		if (_connectionListener != null)
			execute(() -> _connectionListener.connected(serverInfos));
	}
	
	void onDisconnected(final boolean isClientQuit) {
		if (_connectionListener != null)
			execute(() -> _connectionListener.disconnected(isClientQuit));
	}
	
	void onNewPlayersCount(final int playersCount) {
		if (_connectionListener != null)
			execute(() -> _connectionListener.newPlayersCount(playersCount));
	}
	
	void onServerEngineReady() {
		if (_connectionListener != null)
			execute(() -> _connectionListener.serverEngineReady());
	}
	
	void onNewChatMessage(ChatMessage msg) {
		if (_chatListener != null)
			execute(() -> _chatListener.newChatMessage(msg));
	}
}
