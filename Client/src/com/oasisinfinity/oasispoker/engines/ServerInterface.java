package com.oasisinfinity.oasispoker.engines;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;

import javax.swing.event.EventListenerList;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.listeners.ServerListener;
import com.oasisinfinity.oasispoker.models.ClientInfos;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.oasispoker.network.Client;
import com.oasisinfinity.oasispoker.network.ClientTask;
import com.oasisinfinity.oasispoker.network.Message;
import com.oasisinfinity.oasispoker.network.Message.Instruction;
import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.io.IOUtil;

import net.jcip.annotations.NotThreadSafe;

/**
 * Must be confined in the Executor passed to the constructor.
 * The only method thread safe is {@code isConnected()}. 
 * @author OasisInfinity
 * @date 2016-07-01
 */
@NotThreadSafe
class ServerInterface {
	public final static int COMPATIBILITY_VERSION = 1;
	
	private final ExecutorService _exec;
	private final InetAddress _address;
	private final int _port;
	private final EventListenerList _listeners;
	
	private Client _client;
	
	private volatile boolean _isConnected;
	
	private final Thread _disconnectHookThread = new Thread(() -> disconnectClient(), "GameClientDisconnectHook");
	
	ServerInterface(ExecutorService executor, InetAddress address, int port) {
		_exec = executor;
		_listeners = new EventListenerList();
		_address = address;
		_port = port;
		
		Runtime.getRuntime().addShutdownHook(_disconnectHookThread);
	}
	
	void connect(String clientName, StatsInfos clientStats, boolean isSpectator) throws IOException {
		if (_isConnected)
			throw new IllegalStateException("Already connected");
		
		Socket s = new Socket();
		s.connect(new InetSocketAddress(_address, _port), 5000);
		
		Client.Builder clientBuilder = new Client.Builder(s);
		ClientInfos clientInfos = new ClientInfos(clientName, clientStats, COMPATIBILITY_VERSION, isSpectator);
		
		clientBuilder.setInfos(clientInfos);
		
		_client = clientBuilder.build();
		
		_isConnected = true;
		
		ClientTask task = new ClientTask(_client);
		
		task.addDisconnectedListener(c -> {
			if (_exec.isShutdown())
				return;
			
			_exec.execute(() -> {
				try {
					disconnect(false);
				} catch (IllegalStateException e) { /* Already disconnected */ }
			});
		});
		
		task.addNewMessageListener((c, msg) -> {
			_exec.execute(() -> onNewMessage(msg));
		});
		
		BasicThreadFactory.createNewThread(task, "ClientTaskThread", BaseApp.FX_CRASH_HANDLER).start();
		
		sendToServer(new Message(Instruction.ClientInfos, IOUtil.toJson(clientInfos)));
	}
	
	void disconnect() {
		disconnect(true);
	}
	
	private void disconnect(boolean isClientQuit) {
		if (!_isConnected)
			return;
		
		onDisconnected(isClientQuit);
		
		disconnectClient();
		
		_isConnected = false;
	}
	
	private void disconnectClient() {
		try {
			if (_client != null)
				_client.disconnect();
		} catch (IOException e) {
			BaseApp.logException(Level.WARNING, e);
		}
	}
	
	/**
	 * ThreadSafe
	 * @return true if connected to server, false otherwise
	 */
	boolean isConnected() {
		return _isConnected;
	}
	
	private void ensureConnected() {
		if (!_isConnected)
			throw new IllegalStateException("Not connected");
	}

	void sendToServer(Message msg) {
		ensureConnected();
		
		_client.write(msg.toString());
	}
	
	void addClientStateListener(ServerListener l) {
		_listeners.add(ServerListener.class, l);
	}
	
	void removeClientStateListener(ServerListener l) {
		_listeners.remove(ServerListener.class, l);
	}
	
	private void onDisconnected(boolean isClientQuit) {
		for (ServerListener l : _listeners.getListeners(ServerListener.class))
			l.disconnected(isClientQuit);
	}
	
	private void onNewMessage(Message msg) {
		for (ServerListener l : _listeners.getListeners(ServerListener.class))
			l.newMessage(msg);	
	}
}
