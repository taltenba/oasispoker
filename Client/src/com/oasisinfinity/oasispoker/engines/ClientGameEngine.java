package com.oasisinfinity.oasispoker.engines;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

import com.google.gson.reflect.TypeToken;
import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.listeners.ChatListener;
import com.oasisinfinity.oasispoker.listeners.ConnectionListener;
import com.oasisinfinity.oasispoker.listeners.GameProgressListener;
import com.oasisinfinity.oasispoker.listeners.ServerListener;
import com.oasisinfinity.oasispoker.models.ActionInfos;
import com.oasisinfinity.oasispoker.models.BustedPlayerInfos;
import com.oasisinfinity.oasispoker.models.ChatMessage;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.GameInfos;
import com.oasisinfinity.oasispoker.models.NewBoardCardsInfos;
import com.oasisinfinity.oasispoker.models.NewPosInfos;
import com.oasisinfinity.oasispoker.models.PlayerRank;
import com.oasisinfinity.oasispoker.models.ServerInfos;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.oasispoker.network.Message;
import com.oasisinfinity.oasispoker.network.Message.Instruction;
import com.oasisinfinity.oasispoker.network.Message.Reason;
import com.oasisinfinity.utils.concurrent.ConcurrentUtil;
import com.oasisinfinity.utils.io.IOUtil;

public class ClientGameEngine extends BaseGameEngine {
	private static final Type PLAYER_RANK_TYPE = new TypeToken<List<PlayerRank>>(){}.getType();
	
	private final ServerInterface _serverInterface;
	
	private volatile boolean _isServerEngineReady;
	private volatile GameConfig _config;
	
	private String _clientPlayerName;

	private ClientGameEngine(ScheduledExecutorService engineExec, InetAddress address, int port) {
		super(engineExec);
		
		_serverInterface = new ServerInterface(engineExec, address, port);
		
		_serverInterface.addClientStateListener(new ServerListener() {
			@Override
			public void disconnected(boolean isClientQuit) {
				onDisconnected(isClientQuit);
			}

			@Override
			public void newMessage(Message msg) {
				newServerMessage(msg);
			}
			
		});
	}
	
	public void connect(final String clientPlayerName, final StatsInfos clientStats, final boolean isSpectator) {
		execute(() -> {
			try {
				_clientPlayerName = clientPlayerName;
				_serverInterface.connect(clientPlayerName, clientStats, isSpectator);
			} catch (IOException | IllegalStateException e) {
				onConnectionFailed(e);
			}
		});
	}
	
	public void disconnect() {
		execute(() -> _serverInterface.disconnect());
	}
	
	public boolean isConnected() {
		return _serverInterface.isConnected();
	}
	
	public boolean isServerEngineReady() {
		return _isServerEngineReady;
	}
	
	public void setClientReady() {
		execute(() -> _serverInterface.sendToServer(Message.MSG_READY));
	}
	
	public void sendChatMessage(final ChatMessage msg) {
		execute(() -> _serverInterface.sendToServer(new Message(Instruction.NewChatMessage, msg)));
	}

	@Override
	protected void foldSafe() {
		_serverInterface.sendToServer(new Message(Instruction.NewAction, ActionInfos.newFoldActionInfos(_clientPlayerName)));
	}

	@Override
	protected void checkSafe() {
		_serverInterface.sendToServer(new Message(Instruction.NewAction, ActionInfos.newCheckActionInfos(_clientPlayerName)));
	}

	@Override
	protected void callSafe() {
		_serverInterface.sendToServer(new Message(Instruction.NewAction, ActionInfos.newCallActionInfos(_clientPlayerName)));
	}

	@Override
	protected void raiseSafe(int amount) {
		_serverInterface.sendToServer(new Message(Instruction.NewAction, ActionInfos.newRaiseActionInfos(_clientPlayerName, amount)));
	}
	
	/**
	 * Return null until this engine is connected to server
	 */
	@Override
	public GameConfig getGameConfig() {
		return _config;
	}
	
	@Override
	public void shutdown() {
		if (isShutdown())
			return;
		
		disconnect();
		
		super.shutdown();
	}

	private void newServerMessage(Message msg) {
		switch (msg.getInstruction()) {
			case NewGameInfos:
				updateGameInfos(IOUtil.loadJson(msg.getArg(), GameInfos.class));
				break;
			case Ready:
				updateGameInfos(IOUtil.loadJson(msg.getArg(), GameInfos.class));
				_isServerEngineReady = true;
				onServerEngineReady();
				break;
			case GameStarted:
				onGameStarted();
				break;
			case RoundStarted:
				onRoundStarted(Boolean.getBoolean(msg.getArg()));
				break;
			case TurnStarted:
				onTurnStarted(Integer.parseInt(msg.getArg()));
				break;
			case NextPos:
				NewPosInfos newPosInfos = IOUtil.loadJson(msg.getArg(), NewPosInfos.class);
				onNextPos(newPosInfos.getPlayerInfos(), newPosInfos.getOldPos(), newPosInfos.getNewPos());
				break;
			case TurnCompleted:
				onTurnCompleted(Integer.parseInt(msg.getArg()));
				break;
			case RoundCompleted:
				onRoundCompleted(Boolean.parseBoolean(msg.getArg()));
				break;
			case GameOver:
				onGameOver(Collections.unmodifiableList(IOUtil.loadJson(msg.getArg(), PLAYER_RANK_TYPE)));
				break;
			case NewBoardCards:
				NewBoardCardsInfos newBoardCardsInfos = IOUtil.loadJson(msg.getArg(), NewBoardCardsInfos.class);
				onNewBoardCards(newBoardCardsInfos.getAddIndex(), newBoardCardsInfos.getNewCards());
				break;
			case BoardCleared:
				onBoardCleared();
				break;
			case PlayerBusted:
				BustedPlayerInfos bustedPlayerInfos = IOUtil.loadJson(msg.getArg(), BustedPlayerInfos.class);
				onPlayerBusted(bustedPlayerInfos.getPos(), bustedPlayerInfos.getName());
				break;
			case ConnectionAccepted:
				ServerInfos serverInfos = IOUtil.loadJson(msg.getArg(), ServerInfos.class);
				_config = serverInfos.getGameConfig();
				onConnected(serverInfos);
				break;
			case ConnectionRejected:
				_serverInterface.disconnect();
				onConnectionRejected(Reason.valueOf(msg.getArg()));
				break;
			case NewPlayersCount:
				onNewPlayersCount(Integer.parseInt(msg.getArg()));
				break;
			case NewChatMessage:
				onNewChatMessage(IOUtil.loadJson(msg.getArg(), ChatMessage.class));
				break;
			default:
				break;
		}
	}
	
	@Override
	public ClientGameEngineInterface addEngineInterface(Executor interfaceExec, GameProgressListener gameProgressListener) {
		return addEngineInterface(interfaceExec, gameProgressListener, null, null);
	}
	
	public ClientGameEngineInterface addEngineInterface(Executor interfaceExec, GameProgressListener gameProgressListener, ConnectionListener connectionListener) {
		return addEngineInterface(interfaceExec, gameProgressListener, connectionListener, null);
	}
	
	public ClientGameEngineInterface addEngineInterface(Executor interfaceExec, GameProgressListener gameProgressListener, ConnectionListener connectionListener, ChatListener chatListener) {
		ClientGameEngineInterface newInterface = new ClientGameEngineInterface(interfaceExec, gameProgressListener, connectionListener, chatListener);
		
		addEngineInterface(newInterface);
		
		return newInterface;
	}
	
	private void onConnectionRejected(Reason reason) {
		List<GameEngineInterface> engineInterfaces = getEngineInterfaces();
		
		for (GameEngineInterface i : engineInterfaces)
			((ClientGameEngineInterface) i).onConnectionRejected(reason);
	}
	
	private void onConnectionFailed(Exception e) {
		List<GameEngineInterface> engineInterfaces = getEngineInterfaces();
		
		for (GameEngineInterface i : engineInterfaces)
			((ClientGameEngineInterface) i).onConnectionFailed(e);
	}
	
	private void onConnected(ServerInfos serverInfos) {
		List<GameEngineInterface> engineInterfaces = getEngineInterfaces();
		
		for (GameEngineInterface i : engineInterfaces)
			((ClientGameEngineInterface) i).onConnected(serverInfos);
	}
	
	private void onDisconnected(boolean isClientQuit) {
		List<GameEngineInterface> engineInterfaces = getEngineInterfaces();
		
		for (GameEngineInterface i : engineInterfaces)
			((ClientGameEngineInterface) i).onDisconnected(isClientQuit);
	}
	
	private void onNewPlayersCount(int playersCount) {
		List<GameEngineInterface> engineInterfaces = getEngineInterfaces();
		
		for (GameEngineInterface i : engineInterfaces)
			((ClientGameEngineInterface) i).onNewPlayersCount(playersCount);
	}
	
	private void onServerEngineReady() {
		List<GameEngineInterface> engineInterfaces = getEngineInterfaces();
		
		for (GameEngineInterface i : engineInterfaces)
			((ClientGameEngineInterface) i).onServerEngineReady();
	}
	
	private void onNewChatMessage(ChatMessage msg) {
		List<GameEngineInterface> engineInterfaces = getEngineInterfaces();
		
		for (GameEngineInterface i : engineInterfaces)
			((ClientGameEngineInterface) i).onNewChatMessage(msg);
	}
	
	public static ClientGameEngine newInstance(InetAddress address, int port) throws IOException {
		final ScheduledExecutorService engineExec = createEngineExecutor();
		
		return ConcurrentUtil.getSubmitResult(engineExec, () -> {
			return new ClientGameEngine(engineExec, address, port);
		}, BaseApp.LOGGER); 
	}
}
