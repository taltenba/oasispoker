package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.ClientApp;
import com.oasisinfinity.oasispoker.models.User;
import com.oasisinfinity.utils.javafx.NodeUtil;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class SettingsController extends ClientBaseController {
	private static final int PSEUDO_MAX_LENGTH = 15;
	
	@FXML private GridPane _settingsPane;
	@FXML private ScrollPane _settingsScrollPane;
	@FXML private TextField _pseudoField;
	@FXML private Slider _musicVolumeSlider;
	//@FXML private Slider _soundEffectsVolumeSlider;
	@FXML private Label _musicVolumePercentageLabel;
	//@FXML private Label _soundEffectsVolumePercentageLabel;
	@FXML private Button _validateButton;
	
	public SettingsController() {
		super();
	}

	@FXML
	@Override
	protected void initialize() {
		super.initialize();
		
		_settingsPane.minHeightProperty().bind(_settingsScrollPane.heightProperty().subtract(2.0));
		_validateButton.setOnAction(e -> onValidateButtonClicked());
		_musicVolumeSlider.valueProperty().addListener((observable, oldValue, newValue) -> onSliderValueChanged(_musicVolumePercentageLabel, newValue));
		//_soundEffectsVolumeSlider.valueProperty().addListener((observable, oldValue, newValue) -> onSliderValueChanged(_soundEffectsVolumePercentageLabel, newValue));
		
		NodeUtil.addTextLimiter(_pseudoField, PSEUDO_MAX_LENGTH);
		
		_pseudoField.textProperty().addListener((observable, oldValue, newValue) -> {
			_validateButton.setDisable(newValue.isEmpty() || !newValue.matches("[\\w\\p{L}]+"));
		});
	}
	
	@Override
	public void setMainApp(BaseApp mainApp) {
		super.setMainApp(mainApp);
		
		User u = getMainApp().getUser();
		
		_pseudoField.setText(u.getPseudo());
		
		float musicVol = u.getMusicVolume();
		//float soundEffectsVol = u.getSoundEffectsVolume();
		
		_musicVolumeSlider.setValue(musicVol);
		//_soundEffectsVolumeSlider.setValue(soundEffectsVol);
		
		onSliderValueChanged(_musicVolumePercentageLabel, musicVol);
		//onSliderValueChanged(_soundEffectsVolumePercentageLabel, soundEffectsVol);
	}
	
	private void onSliderValueChanged(Label label, Number newValue) {
		label.setText(Math.round(newValue.floatValue() * 100.0f) + " %");
	}
	
	private void onValidateButtonClicked() {
		User u = getMainApp().getUser();
		
		u.setPseudo(_pseudoField.getText());
		u.setMusicVolume((float) _musicVolumeSlider.getValue());
		//u.setSoundEffectsVolume((float) _soundEffectsVolumeSlider.getValue());
		
		goToNewScene(ClientApp.MAIN_MENU_SCENE);
	}
}
