package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.ClientApp;
import com.oasisinfinity.oasispoker.engines.ClientGameEngine;
import com.oasisinfinity.oasispoker.engines.ClientGameEngineInterface;
import com.oasisinfinity.oasispoker.listeners.ConnectionAdapter;
import com.oasisinfinity.oasispoker.listeners.ConnectionListener;
import com.oasisinfinity.utils.javafx.FxThreadExecutor;

public class ClientBaseController extends BaseController {
	private ClientGameEngine _engine;
	private ClientGameEngineInterface _engineInterface;
	private boolean _connectionLost;
	
	private final ConnectionListener _disconnectListener = new ConnectionAdapter() {
		@Override
		public void disconnected(boolean isClientQuit) {
			ClientBaseController.this.disconnected(isClientQuit);
		}
	};
	
	public ClientBaseController() {
		super();
	}

	@Override
	public void setMainApp(BaseApp mainApp) {
		if (!(mainApp instanceof ClientApp))
			throw new IllegalArgumentException("mainApp must be instance of ClientApp class");
		
		super.setMainApp(mainApp);
	}
	
	@Override
	protected ClientApp getMainApp() {
		return (ClientApp) super.getMainApp();
	}
	
	protected void setClientGameEngine(ClientGameEngine engine) {
		_engine = engine;
		
		_connectionLost = false;
		
		_engineInterface = _engine.addEngineInterface(FxThreadExecutor.getInstance(), null, _disconnectListener);
	}
	
	private void disconnected(boolean isClientQuit) {
		if (_engine == null)
			return;
		
		_engine.shutdown();
		_engine = null;
		
		if (isClientQuit)
			return;
		
		_connectionLost = true;
		
		BaseApp.ALERT_UTIL.showErrorMsg("Connexion perdue", "La connexion au server a �t� interrompue");
		goToNewScene(ClientApp.MAIN_MENU_SCENE);
	}
	
	protected ClientGameEngine getGameEngine() {
		return _engine;
	}
	
	protected void disconnect() {
		if (isConnected()) {
			_engine.disconnect();
			disconnected(true);
		}
	}
	
	protected boolean isConnected() {
		return _engine != null && _engine.isConnected();
	}
	
	protected boolean connectionLost() {
		return _connectionLost;
	}
	
	@Override
	protected boolean onNavigateFrom() {
		if (_engine != null)
			_engine.removeEngineInterface(_engineInterface);
		
		return super.onNavigateFrom();
	}
}
