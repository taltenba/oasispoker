package com.oasisinfinity.oasispoker.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;

public class StakeController {
	@FXML private Region _rootLayout;
	@FXML private Label _stakeValueLabel;
	
	public StakeController() {
		// Empty
	}

	@FXML
	private void initialize() {
		// Empty
	}
	
	public void setStake(int stake) {
		_rootLayout.setVisible(stake != 0);
		
		_stakeValueLabel.setText(String.format("%,d", stake));
	}
}
