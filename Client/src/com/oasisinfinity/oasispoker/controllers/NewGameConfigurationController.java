package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.controls.NoDelayTooltip;
import com.oasisinfinity.oasispoker.models.GameConfig;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;

public class NewGameConfigurationController extends ClientBaseController {
	@FXML private ScrollPane _configScrollPane;
	@FXML private GridPane _configPane;
	@FXML private Label _initialSmallBlindLabel;
	@FXML private Label _initialAntesLabel;
	@FXML private Button _startButton;
	@FXML private Spinner<Integer> _playersCountSpinner;
	@FXML private Spinner<Integer> _initialCashSpinner;
	@FXML private Spinner<Integer> _initialSmallBlindSpinner;
	@FXML private Spinner<Integer> _initialAntesSpinner;
	@FXML private Spinner<Integer> _blindsIncreaseFrequencySpinner;
	@FXML private Spinner<Integer> _antesIncreaseFrequencySpinner;
	@FXML private Spinner<Integer> _blindsIncreaseMultiplierSpinner;
	@FXML private Spinner<Integer> _antesIncreaseMultiplierSpinner;
	@FXML private Spinner<Integer> _antesDelaySpinner;
	@FXML private Spinner<Integer> _playingTimeSpinner;
		
	public NewGameConfigurationController() {
		super();
	}

	@FXML
	@Override
	protected void initialize() {
		super.initialize();
		
		_configPane.minHeightProperty().bind(_configScrollPane.heightProperty().subtract(2.0));
		_initialSmallBlindLabel.setTooltip(new NoDelayTooltip(_initialSmallBlindLabel, "La valeur de la grosse blind sera le double de celle-ci"));
		_initialAntesLabel.setTooltip(new NoDelayTooltip(_initialAntesLabel, "Les antes sont des mises initiales obligatoires pour tous les joueurs"));
		_startButton.setOnAction(e -> onStartButtonClicked());
	}
	
	private void onStartButtonClicked() {
		_startButton.setDisable(true);
		
		getMainApp().startGame(getGameConfig());
	}
	
	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		_startButton.setDisable(false);
	}
	
	private GameConfig getGameConfig() {
		return new GameConfig.Builder()
				.setHumanPlayersCount(1)
				.setBotsCount(_playersCountSpinner.getValue() - 1)
				.setInitialCash(_initialCashSpinner.getValue())
				.setInitialSmallBlind(_initialSmallBlindSpinner.getValue())
				.setInitialAntes(_initialAntesSpinner.getValue())
				.setBlindsIncreaseFrequency(_blindsIncreaseFrequencySpinner.getValue())
				.setAntesIncreaseFrequency(_antesIncreaseFrequencySpinner.getValue())
				.setBlindsIncreaseMultiplier(_blindsIncreaseMultiplierSpinner.getValue())
				.setAntesIncreaseMultiplier(_antesIncreaseMultiplierSpinner.getValue())
				.setAntesDelay(_antesDelaySpinner.getValue())
				.setPlayingTime(_playingTimeSpinner.getValue())
				.setMaxAfkDuration(GameConfig.INFINITE_DUR)
				.build();
	}
}
