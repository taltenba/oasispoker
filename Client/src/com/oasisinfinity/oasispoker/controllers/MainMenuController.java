package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.ClientApp;
import com.oasisinfinity.oasispoker.models.AppInfos.AppName;
import com.oasisinfinity.utils.javafx.AnimUtil;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public class MainMenuController extends ClientBaseController {
	private static final Duration HALF_MENU_TRANSITION_DUR = Duration.millis(500.0);
	
	@FXML private ImageView _titleImageView;
	@FXML private Button _redChipButton;
	@FXML private Button _blueChipButton;
	@FXML private Button _greenChipButton;
	@FXML private Label _versionLabel;
	@FXML private GridPane _buttonsPane;
	@FXML private VBox _rootPane;
	
	private final SequentialTransition _mouseHoverAnim;
	
	private enum Menu {
		Main (new String[] {"Jouer", "Options", "Stats"}),
		Play (new String[] {null, "Hors ligne", "En ligne"});
		
		private final String[] _buttonsTexts;
		
		private Menu(String... buttonsTexts) {
			_buttonsTexts = buttonsTexts;
		}
		
		public String[] getButtonsTexts() {
			return _buttonsTexts.clone();
		}
	}
	
	public MainMenuController() {
		super();
		
		_mouseHoverAnim = new SequentialTransition();
		
		RotateTransition[] rotateAnims = new RotateTransition[2];
		
		for (int i = 0; i < rotateAnims.length; ++i) {
			RotateTransition rt = new RotateTransition();
			rt.setAutoReverse(true);
			rt.setCycleCount(2);
			rt.setFromAngle(0.0);
			rt.setByAngle(i == 0 ? 40.0 : -40.0);
			rt.setDuration(Duration.millis(500.0));
			
			rotateAnims[i] = rt;
			_mouseHoverAnim.getChildren().add(rt);
		}
		
		_mouseHoverAnim.setInterpolator(Interpolator.EASE_IN);
		_mouseHoverAnim.setCycleCount(SequentialTransition.INDEFINITE);
	}
	
	@FXML
	@Override
	protected void initialize() {
		super.initialize();
		
		for (Node n : _buttonsPane.getChildren()) {
			n.setOnMouseClicked(e -> onChipButtonClicked(e));
			n.setOnMouseMoved(e -> onChipButtonMouseMoved(e));
			n.setOnMouseExited(e -> onChipButtonMouseExited(e));
		}
		
		_titleImageView.setFitHeight(_rootPane.getHeight() / 3.0);
		_buttonsPane.setMaxWidth(_rootPane.getWidth() / 1.26);
		
		_rootPane.heightProperty().addListener((observable, oldValue, newValue) -> {
			_titleImageView.setFitHeight(_rootPane.getHeight() / 3.0);
		});
		
		_rootPane.widthProperty().addListener((observable, oldValue, newValue) -> {
			_buttonsPane.setMaxWidth(_rootPane.getWidth() / 1.26);
		});
	}
	
	@Override
	public void setMainApp(BaseApp mainApp) {
		super.setMainApp(mainApp);
		
		_versionLabel.setText("v" + getMainApp().getVersion(AppName.Client) + " - by OasisInfinity");
	}
	
	private void onChipButtonClicked(MouseEvent e) {
		Button source = (Button) e.getSource();
		
		if (!isOnTheButton(source, e.getX(), e.getY()))
			return;
		
		switch (source.getText()) {
			case "Jouer":
				goToNewMenu(Menu.Play);
				return;
			case "Options":
				goToNewScene(ClientApp.SETTINGS_SCENE);
				break;
			case "Stats":
				goToNewScene(ClientApp.STATS_SCENE);
				break;
			case "Hors ligne":
				goToNewScene(ClientApp.NEW_GAME_CONFIGURATION_SCENE);
				break;
			case "En ligne":
				goToNewScene(ClientApp.SERVER_CONNECTION_SCENE);
		}
		
		//goToNewMenu(Menu.Main); //TODO: test if useless
	}
	
	private void onChipButtonMouseMoved(MouseEvent e) {
		Button source = (Button) e.getSource();
		
		if (!isOnTheButton(source, e.getX(), e.getY())) {
			if (_mouseHoverAnim.getNode() != null && _mouseHoverAnim.getNode().equals(source))
				stopChipButtonAnim(source);
			
			return;
		}
		
		_mouseHoverAnim.setNode(source);
		_mouseHoverAnim.play();
	}
	
	private void onChipButtonMouseExited(MouseEvent e) {
		stopChipButtonAnim((Button) e.getSource());
	}
	
	private void stopChipButtonAnim(Button btn) {
		_mouseHoverAnim.stop();
		
		double currentBtnRotate = btn.getRotate();
		
		if (currentBtnRotate == 0)
			return;
		
		RotateTransition rotateAnim = new RotateTransition(Duration.millis(Math.abs(currentBtnRotate * 6.25)), btn);
		rotateAnim.setFromAngle(currentBtnRotate);
		rotateAnim.setToAngle(0);
		
		rotateAnim.play();
	}
	
	@Override
	protected boolean onNavigateFrom() {
		goToNewMenu(Menu.Main);
		return true;
	}
	
	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		ClientApp clientApp = getMainApp();
		
		if (clientApp.getUser().hasBeenModified())
			clientApp.saveUser();
	}
	
	private void goToNewMenu(Menu menu) {
		/*FadeTransition fadeOutAnim = new FadeTransition(Duration.millis(500), _buttonsPane);
		fadeOutAnim.setFromValue(1.0);
		fadeOutAnim.setToValue(0.0);*/
		
		FadeTransition fadeOutAnim = AnimUtil.fadeOutAnim(_buttonsPane, HALF_MENU_TRANSITION_DUR);
		
		fadeOutAnim.setOnFinished(e -> {	
			String[] btnsTexts = menu.getButtonsTexts();
			ObservableList<Node> btnsPaneChildren = _buttonsPane.getChildren();
			
			for (int i = 0; i < btnsTexts.length; ++i) {
				String btnText = btnsTexts[i];
				Button btn = (Button) btnsPaneChildren.get(i);
				
				if (btnText == null) {
					btn.setVisible(false);
					continue;
				}
				
				((Button) btnsPaneChildren.get(i)).setText(btnText);
				btn.setVisible(true);
			}
			
			ObservableList<RowConstraints> btnsPaneRowsContraints = _buttonsPane.getRowConstraints();
			btnsPaneRowsContraints.get(0).setPercentHeight(btnsPaneChildren.get(0).isVisible() ? 55 : 0);
			
			/*FadeTransition fadeInAnim = new FadeTransition(Duration.millis(500), _buttonsPane);
			fadeInAnim.setFromValue(0.0);
			fadeInAnim.setToValue(1.0);
			
			fadeInAnim.play();*/
			
			AnimUtil.fadeInAnim(_buttonsPane, HALF_MENU_TRANSITION_DUR).play();
			
			boolean isMainMenu = menu == Menu.Main;
			Button goBackButton = getGoBackButton();
			
			goBackButton.setVisible(!isMainMenu);
			goBackButton.setManaged(!isMainMenu);
			_versionLabel.setVisible(isMainMenu);
			_versionLabel.setManaged(isMainMenu);
		});
		
		fadeOutAnim.play();
	}
	
	/***
	 * Calculate if the position is on the square button
	 * @param btn : a square button
	 * @param x : x position relative to the button
	 * @param y : y position relative to the button
	 * @return
	 */
	private static boolean isOnTheButton(Button btn, double x, double y) {
		double buttonWidth = btn.getWidth();
		double buttonHeight = btn.getHeight();
		double realButtonSize = Math.min(buttonWidth, buttonHeight);
		double halfRealButtonSize = realButtonSize / 2;
		
		double halfButtonWidth = buttonWidth / 2;
		double xMin = halfButtonWidth - halfRealButtonSize;
		double xMax = halfButtonWidth + halfRealButtonSize;
		
		double halfButtonHeight = buttonHeight / 2;
		double yMin = halfButtonHeight - halfRealButtonSize;
		double yMax = halfButtonHeight + halfRealButtonSize;
		
		return x >= xMin && x <= xMax && y >= yMin && y <= yMax;
	}
	
}
