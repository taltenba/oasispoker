package com.oasisinfinity.oasispoker.controllers;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.engines.Player.Action;
import com.oasisinfinity.oasispoker.engines.Player.Role;
import com.oasisinfinity.oasispoker.engines.Player.Status;
import com.oasisinfinity.oasispoker.engines.PlayerInterface;
import com.oasisinfinity.oasispoker.engines.ServerGameEngine;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.views.CashLabel;
import com.oasisinfinity.oasispoker.views.TimeLeftControl;
import com.oasisinfinity.utils.ColorUtil;
import com.oasisinfinity.utils.javafx.AnimUtil;
import com.oasisinfinity.utils.javafx.DialogUtil;
import com.oasisinfinity.utils.javafx.DialogUtil.StageInfos;

import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class PlayerInfosController {	
	private static final Color CURRENT_PLAYER_COLOR = Color.BROWN;
	
	public enum Position {
		TopLeft(0, 0),
		TopCenter(1, 0),
		TopRight(2, 0),
		CenterLeft(0, 1),
		CenterRight(2, 1),
		BottomLeft(0, 2),
		BottomCenter(1, 2),
		BottomRight(2, 2);
		
		private final int _column;
		private final int _row;
		
		private Position(int column, int row) {
			_column = column;
			_row = row;
		}
		
		public int getColumn() {
			return _column;
		}
		
		public int getRow() {
			return _row;
		}
	}
	
	@FXML private Label _nameLabel;
	@FXML private CashLabel _cashLabel;
	@FXML private Label _actionLabel;
	@FXML private Label _roleLabel;
	@FXML private TimeLeftControl _timeLeftControl;
	@FXML private Circle _roleCircle;
	@FXML private GridPane _infosPane;
	@FXML private Rectangle _backgroundRect;
	@FXML private GridPane _rootPane;
	@FXML private StackPane _rectanglePane;
	private ImageView[] _cardsImageViews;
	private Region _stakeControl;
	
	private PlayerInterface _player;
	private boolean _areCardsVisible;
	
	public PlayerInfosController() {
		_areCardsVisible = false;
	}

	@FXML
	private void initialize() {
		_cardsImageViews = new ImageView[] {
				(ImageView) _rectanglePane.lookup("#_leftCardImageView"), 
				(ImageView) _rectanglePane.lookup("#_rightCardImageView")};
	}
	
	private void refreshAction(Action action) {
		_rootPane.setOpacity(action == Action.Fold ? 0.5 : 1.0);
		
		if (action == null) {
			_actionLabel.setText("");
			return;
		}
			
		refreshActionLabel(action.getColor(), action.toString());
	}
	
	private void refreshHand(Hand hand) {
		if (hand == null) {
			refreshAction(_player.getAction());
			refreshRole(_player.getRole());
			return;
		}
		
		refreshRole(null);
		refreshActionLabel(Color.GREEN, hand.getType().toString());
	}
	
	private void refreshActionLabel(Color col, String text) {
		_actionLabel.setStyle("-fx-text-fill:" + ColorUtil.toHexColor(col) + ";");
		_actionLabel.setText(text);
	}
	
	private void refreshRole(Role role) {
		boolean isRoleNull = role == null;
		
		_infosPane.getColumnConstraints().get(0).setPercentWidth(isRoleNull ? -1 : 60);
		_roleCircle.setManaged(!isRoleNull);
		_roleCircle.setVisible(!isRoleNull);
		_roleLabel.setManaged(!isRoleNull);
		_roleLabel.setVisible(!isRoleNull);
		
		if (isRoleNull)
			return;
		
		_roleCircle.setFill(role.getColor());
		_roleLabel.setText(role.getAbbreviation());
	}
	
	private void refreshStatus(Status status) {
		boolean isStatusNull = status == null;
		
		_nameLabel.setText(isStatusNull ? _player.getName() : status.toString());
		_nameLabel.setStyle(isStatusNull ? "" : "-fx-text-fill:" + ColorUtil.toHexColor(status.getColor()));
	}
	
	private void setBackgroundColor(Color color) {
		_backgroundRect.setFill(color);
	}
	
	private void showStatsDialog() {
		try {
			StageInfos<StatsDialogController> dialogInfos = DialogUtil.createDialogStage(
					StageStyle.UNDECORATED,
					PlayerInfosController.class.getResource("/fxml/StatsDialog.fxml"),
					"",
					false,
					Modality.WINDOW_MODAL,
					_infosPane.getScene().getWindow());
			
			Stage dialogStage = dialogInfos.getStage();
			
	        StatsDialogController controller = dialogInfos.getController();
	        controller.setDialogStage(dialogStage);
	        controller.setPlayer(_player);
	        
	        DialogUtil.showCentered(dialogStage);
	    } catch (IOException e) {
	        BaseApp.logException(Level.SEVERE, e);
	        BaseApp.ALERT_UTIL.corruptedInstallation();
	    }
	}
	
	public void setPlayer(PlayerInterface player, int playingTimeSeconds) {
		_player = player;
		
		_timeLeftControl.setStartTime(playingTimeSeconds);
		
		if (_stakeControl == null) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(PlayerInfosController.class.getResource("/fxml/StakeControl.fxml"));
			
			try {
				_stakeControl = (HBox) loader.load();
			} catch (IOException e) {
				BaseApp.logException(Level.SEVERE, e);
				BaseApp.ALERT_UTIL.corruptedInstallation();
				return;
			}
			
			final StakeController controller = (StakeController) loader.getController();
			
			player.getStakeProperty().addListener((observable, oldValue, newValue) 
					-> controller.setStake(newValue.intValue()));
			
			controller.setStake(player.getStake());
		}	
		
		//_nameLabel.textProperty().bind(player.getNameProperty());
		//_cashLabel.textProperty().bind(player.getCashProperty().asString("$ %,d"));
		//_cashLabel.bindValueProperty(player.getCashProperty());
		
		if (_player.getStats().isPresent()) {
			_infosPane.setCursor(Cursor.HAND);
			
			_infosPane.setOnMouseClicked(e -> showStatsDialog());
		}
		
		player.getCashProperty().addListener((observable, oldValue, newValue) -> _cashLabel.setValue(newValue.intValue()));
		player.getActionProperty().addListener((observable, oldValue, newValue) -> refreshAction(newValue));
		player.getRoleProperty().addListener((observable, oldValue, newValue) -> refreshRole(newValue));
		player.getCardsProperty().addListener((observable, oldValue, newValue) -> updateCards(true));	
		player.getHandProperty().addListener((observable, oldValue, newValue) -> refreshHand(newValue));
		player.getStatusProperty().addListener((observable, oldValue, newValue) -> refreshStatus(newValue));
		
		_cashLabel.setValue(player.getCash());
		refreshAction(player.getAction());
		refreshRole(player.getRole());
		refreshHand(player.getHand());
		refreshStatus(player.getStatus());
	}
	
	public void isPlaying() {
		setBackgroundColor(CURRENT_PLAYER_COLOR);
		
		_actionLabel.setVisible(false);
		_actionLabel.setManaged(false);
		
		_timeLeftControl.setManaged(true);
		_timeLeftControl.setVisible(true);
		
		_timeLeftControl.start();
	}
	
	public void hasPlayed() {
		_timeLeftControl.stopAndReset();
		
		setBackgroundColor(Color.BLACK);
		
		_timeLeftControl.setVisible(false);
		_timeLeftControl.setManaged(false);
		
		_actionLabel.setManaged(true);
		_actionLabel.setVisible(true);
	}
	
	public void setStakePosition(Position pos) {
		int col = pos.getColumn();
		int row = pos.getRow();
	
		if (_rootPane.getChildrenUnmodifiable().contains(_stakeControl)) {
			GridPane.setColumnIndex(_stakeControl, col);
			GridPane.setRowIndex(_stakeControl, row);
		} else
			_rootPane.add(_stakeControl, col, row);
		
		GridPane.setHalignment(_stakeControl, HPos.CENTER);
		GridPane.setValignment(_stakeControl, VPos.CENTER);
		
		if (pos == Position.BottomRight || pos == Position.BottomLeft)
			GridPane.setRowSpan(_rectanglePane, 2);	
		else
			GridPane.setRowSpan(_rectanglePane, 1);

		ObservableList<ColumnConstraints> colConstraints = _rootPane.getColumnConstraints();
		ObservableList<RowConstraints> rowConstraints = _rootPane.getRowConstraints();
		
		for (int i = 0; i < colConstraints.size(); ++i) {
			if (i == 1)
				continue;
			
			colConstraints.get(i).setHgrow(i == col ? Priority.SOMETIMES : Priority.NEVER);
		}
		
		for (int i = 0; i < rowConstraints.size(); ++i) {
			if (i == 1)
				continue;
			
			rowConstraints.get(i).setPrefHeight(i == row ? 32.0 : 0.0);
		}
	}
	
	public void setCardsVisible(boolean areVisible) {
		if (_areCardsVisible == areVisible)
			return;
		
		_areCardsVisible = areVisible;
		
		updateCards(false);
		
		/*if (visibleFace)
			updateCards(false);
		else {
			Image cardBack = new Image("/images/card_back.png");
			
			for (ImageView iv : _cardsImageViews)
				iv.setImage(cardBack);
		}	*/
	}
	
	private void updateCards(boolean dealAnim) {
		if (_player == null)
			return;
		
		Image[] cardsImages = new Image[2];
		List<Card> cards = _player.getCards().toList();
		
		if (cards.get(0) == null) { // One card is null iff the both are null
			cardsImages[0] = null;
			cardsImages[1] = null;
		} else if (_areCardsVisible) {
			for (int i = 0, size = cards.size(); i < size; ++i)
				cardsImages[i] = cards.get(i).getImage();
		} else {
			Image cardBack = new Image("/images/card_back.png");
			
			cardsImages[0] = cardBack;
			cardsImages[1] = cardBack;
		}	
		
		/*SequentialTransition fadeOutAnims = new SequentialTransition();
		Duration animDuration = Duration.millis(OfflineGameEngine.CARDS_TRANSITION_DURATION / 2);
		
		for (int i = 0; i < _cardsImageViews.length; ++i) {
			FadeTransition fadeOutAnim = new FadeTransition(animDuration, _cardsImageViews[i]);
			fadeOutAnim.setFromValue(1.0);
			fadeOutAnim.setToValue(0.0);
			
			fadeOutAnims.getChildren().add(fadeOutAnim);
		}*/
		
		Duration animDuration = Duration.millis(ServerGameEngine.CARDS_TRANSITION_DURATION / 2);
		
		SequentialTransition fadeOutAnims = AnimUtil.sequentialFadeOutAnims(animDuration, _cardsImageViews);
		
		fadeOutAnims.setOnFinished(e -> {
			SequentialTransition displayAnims = new SequentialTransition();

			for (int i = 0, size = _cardsImageViews.length; i < size; ++i) {
				_cardsImageViews[i].setImage(cardsImages[i]);
				
				ParallelTransition displayAnim = new ParallelTransition(_cardsImageViews[i]);
				
				if (dealAnim) {
					TranslateTransition translateAnim = new TranslateTransition(animDuration);
					translateAnim.setFromX(i == 0 ? 25.0 : -25.0);
					translateAnim.setToX(i == 0 ? -5.0 : 5.0);
					
					displayAnim.getChildren().add(translateAnim);
				}
				
				displayAnim.getChildren().add(AnimUtil.fadeInAnim(animDuration));
				
				displayAnims.getChildren().add(displayAnim);
			}
			
			displayAnims.play();
		});
			
		fadeOutAnims.play();
	}
}
