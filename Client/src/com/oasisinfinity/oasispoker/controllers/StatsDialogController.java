package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.oasispoker.engines.PlayerInfosGetter;
import com.oasisinfinity.oasispoker.views.StatsControl;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class StatsDialogController {
	@FXML private Label _playerNameLabel;
	@FXML private StatsControl _statsControl;
	@FXML private Button _closeButton;
	
	private Stage _dialogStage;
	
	public StatsDialogController() {
		// Empty
	}
	
	@FXML
	private void initialize() {
		_closeButton.setOnMouseClicked(e -> _dialogStage.close());
	}
	
	public void setDialogStage(Stage dialogStage) {
		_dialogStage = dialogStage;
	}
	
	public void setPlayer(PlayerInfosGetter player) {
		_playerNameLabel.setText(player.getName());
		_statsControl.setStats(player.getStats().get());
	}
}
