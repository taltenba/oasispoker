package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.ClientApp;
import com.oasisinfinity.oasispoker.engines.ClientGameEngine;
import com.oasisinfinity.oasispoker.engines.ClientGameEngineInterface;
import com.oasisinfinity.oasispoker.listeners.ConnectionAdapter;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.ServerInfos;
import com.oasisinfinity.utils.javafx.FxThreadExecutor;

import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

public class ServerWaiterController extends ClientBaseController {
	@FXML private ScrollPane _configScrollPane;
	@FXML private GridPane _configPane;
	@FXML private Label _totalPlayersCountLabel;
	@FXML private Label _botsCountLabel;
    @FXML private Label _initialCashLabel;
    @FXML private Label _initialSmallBlindLabel;
    @FXML private Label _initialAntesLabel;
    @FXML private Label _blindsIncreaseFrequencyLabel;
    @FXML private Label _antesIncreaseFrequencyLabel;
    @FXML private Label _blindsIncreaseMultiplierLabel;
    @FXML private Label _antesIncreaseMultiplierLabel;
    @FXML private Label _antesDelayLabel;
    @FXML private Label _playingTimeLabel;
    @FXML private Label _maxAfkDurationLabel;
    @FXML private ImageView _loadingImageView;
    @FXML private Label _infosLabel;
    
    private int _maxPlayersCount;
    private ClientGameEngineInterface _engineInterface;
	
	private final EventHandler<WindowEvent> _closeEvent = new EventHandler<WindowEvent>() {
		@Override
		public void handle(WindowEvent e) {
			if (!showQuitConfirmationDialog())
				e.consume();
			else
				disconnect();
		}
	};
	
	public ServerWaiterController() {
		super();
	}
	
	@Override
	@FXML
	protected void initialize() {
		super.initialize();
		
		_configPane.minHeightProperty().bind(_configScrollPane.heightProperty().subtract(2.0));
		
		RotateTransition rotateAnim = new RotateTransition(Duration.millis(2000), _loadingImageView);
		rotateAnim.setFromAngle(0.0);
		rotateAnim.setToAngle(360.0);
		rotateAnim.setCycleCount(Animation.INDEFINITE);
		
		rotateAnim.play();
	}
	
	private void engineReady() {
		((GameController) getMainApp().getController(ClientApp.GAME_SCENE)).startGame(getGameEngine());
		
		goToNewScene(ClientApp.GAME_SCENE);
		
		ClientGameEngine engine = getGameEngine();
		
		engine.removeEngineInterface(_engineInterface);
		
		engine.setClientReady();
	}
	
	public void setClientGameEngine(ClientGameEngine engine, ServerInfos serverInfos) {	
		super.setClientGameEngine(engine);
		
		_maxPlayersCount = serverInfos.getMaxPlayersCount();
		
		updateGameConfig(serverInfos.getGameConfig());
		
		waitingForConnections(serverInfos.getPlayersCount());
		
		_engineInterface = engine.addEngineInterface(FxThreadExecutor.getInstance(), null, 
				new ConnectionAdapter() {
					@Override
					public void newPlayersCount(int playersCount) {
						waitingForConnections(playersCount);
					}
					
					@Override
					public void serverEngineReady() {
						ServerWaiterController.this.engineReady();
					}
				});
		
		if (engine.isServerEngineReady()) //If "Ready" message had been received before listener has been created
			engineReady();
	}
	
	private void waitingForConnections(int connectedPlayersCount) {
		if (connectedPlayersCount == _maxPlayersCount)
			_infosLabel.setText("Démarrage de la partie...");
		else
			_infosLabel.setText("En attente de connexion (" + connectedPlayersCount + "/" + _maxPlayersCount + ")...");
	}
	
	private void updateGameConfig(GameConfig config) {
		_totalPlayersCountLabel.setText(String.valueOf(config.getTotalPlayersCount()));
		_botsCountLabel.setText(String.valueOf(config.getBotsCount()));
		_initialCashLabel.setText(String.valueOf(config.getInitialCash()));
		_initialSmallBlindLabel.setText(String.valueOf(config.getInitialSmallBlind()));
		_initialAntesLabel.setText(String.valueOf(config.getInitialAntes()));
		_blindsIncreaseFrequencyLabel.setText(String.valueOf(config.getBlindsIncreaseFrequency()));
		_antesIncreaseFrequencyLabel.setText(String.valueOf(config.getAntesIncreaseFrequency()));
		_blindsIncreaseMultiplierLabel.setText(String.valueOf(config.getBlindsIncreaseMultiplier()));
		_antesIncreaseMultiplierLabel.setText(String.valueOf(config.getAntesIncreaseMultiplier()));
		_antesDelayLabel.setText(String.valueOf(config.getAntesDelay()));
		_playingTimeLabel.setText(String.valueOf(config.getPlayingTime()));
		_maxAfkDurationLabel.setText(String.valueOf(config.getMaxAfkDuration()));
	}
	
	@Override
	protected void onReturnToMainScene() {
		if (!showQuitConfirmationDialog())
			return;
		
		disconnect();
		
		super.onReturnToMainScene();
	}

	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		getMainApp().getStage().addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
	}
	
	@Override
	protected boolean onNavigateFrom() {
		getMainApp().getStage().removeEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
		
		return super.onNavigateFrom();
	}
	
	private boolean showQuitConfirmationDialog() {
		return BaseApp.ALERT_UTIL.showQuitConfirmationMsg("Voulez-vous réellement vous déconnecter du serveur ?");
	}
}
