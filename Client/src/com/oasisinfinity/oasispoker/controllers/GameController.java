package com.oasisinfinity.oasispoker.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.oasisinfinity.controls.IntegerTextField;
import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.ClientApp;
import com.oasisinfinity.oasispoker.engines.BaseGameEngine;
import com.oasisinfinity.oasispoker.engines.ClientGameEngine;
import com.oasisinfinity.oasispoker.engines.GameEngineInterface;
import com.oasisinfinity.oasispoker.engines.PlayerInterface;
import com.oasisinfinity.oasispoker.engines.ServerGameEngine;
import com.oasisinfinity.oasispoker.listeners.GameProgressListener;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.PlayerPosition;
import com.oasisinfinity.oasispoker.models.PlayerRank;
import com.oasisinfinity.oasispoker.models.User;
import com.oasisinfinity.oasispoker.views.CashLabel;
import com.oasisinfinity.oasispoker.views.ChatControl;
import com.oasisinfinity.utils.CircularList;
import com.oasisinfinity.utils.io.IOUtil;
import com.oasisinfinity.utils.javafx.AnimUtil;
import com.oasisinfinity.utils.javafx.DialogUtil;
import com.oasisinfinity.utils.javafx.DialogUtil.StageInfos;
import com.oasisinfinity.utils.javafx.FxThreadExecutor;

import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

public class GameController extends ClientBaseController implements GameProgressListener {
	private static final Duration CHAT_DISPLAY_ANIM_DUR = new Duration(600.0);
	private static final double CHAT_OPACITY = 0.7;
	
	@FXML private Button _foldButton;
	@FXML private Button _checkButton;
	@FXML private Button _callButton;
	@FXML private Button _raiseButton;
	@FXML private Button _minRaiseButton;
	@FXML private Button _potRaiseButton;
	@FXML private Button _maxRaiseButton;
	@FXML private IntegerTextField _stakeValueField;
	@FXML private ImageView _tableImageView;
	@FXML private GridPane _centerPane;
	@FXML private GridPane _playerActionPane;
	@FXML private FlowPane _boardPane;
	@FXML private Button _helpButton;
	@FXML private Button _chatButton;
	@FXML private CashLabel _potLabel;
	@FXML private ChatControl _chatControl;
	@FXML private Circle _newMsgIndicator;
	
	private final CircularList<PlayerInfosController> _playersInfosControllers;
	private final List<Region> _playersInfosControls;
	
	private BaseGameEngine _engine;
	private GameEngineInterface _engineInterface;
	private PlayerInterface _clientPlayer;
	private boolean _isSpectator;
	private Transition _currentBoardAnim;
	
	private final EventHandler<WindowEvent> _closeEvent = new EventHandler<WindowEvent>() {
		@Override
		public void handle(WindowEvent e) {
			if (!_engineInterface.isGameOver() && !showQuitConfirmationDialog())
				e.consume();
			else
				quit();
		}
	};
	
	public GameController() {
		super();
		
		_playersInfosControllers = CircularList.wrapArrayList();
		_playersInfosControls = new ArrayList<>();
	}
	
	@FXML
	@Override
	protected void initialize() {
		super.initialize();
		
		_stakeValueField.valueProperty().addListener((observable, oldValue, newValue) -> refreshRaiseButtonText());
		
		_stakeValueField.isValidProperty().addListener((observable, oldValue, newValue) -> {
			_raiseButton.setDisable(!newValue || !_engineInterface.isCurrentPlayer(_clientPlayer));
		});
		
		_tableImageView.fitHeightProperty().bind(_centerPane.heightProperty().divide(1.1392));
		_tableImageView.fitWidthProperty().bind(_centerPane.widthProperty().divide(1.0667));
		
		_foldButton.setOnMouseClicked(e -> _engine.fold());
		
		_checkButton.setOnMouseClicked(e ->_engine.check());	
		
		_raiseButton.setOnMouseClicked(e -> _engine.raise(_stakeValueField.getValue().orElseThrow(IllegalStateException::new)));
		
		_callButton.setOnMouseClicked(e -> {
			canCheck(true);
			
			_engine.call();
		});
		
		_minRaiseButton.setOnMouseClicked(e -> _stakeValueField.setValue(_engineInterface.getMinRaise(_clientPlayer)));	
		_potRaiseButton.setOnMouseClicked(e -> _stakeValueField.setValue(Math.min(_clientPlayer.getCash(), _engineInterface.getPot())));
		_maxRaiseButton.setOnMouseClicked(e -> _stakeValueField.setValue(_clientPlayer.getCash()));
		
		//_potLabel.textProperty().bind(_engine.getPotProperty().asString("Pot : $ %,d"));

		_helpButton.setOnMouseClicked(e -> showHelp());
		_chatButton.setOnMouseClicked(e -> changeChatVisibility());
	}
	
	private void changeChatVisibility() {
		if (_chatControl.isVisible())
			AnimUtil.hideNode(_chatControl, CHAT_DISPLAY_ANIM_DUR, false);
		else {
			AnimUtil.showNode(_chatControl, CHAT_DISPLAY_ANIM_DUR, CHAT_OPACITY);
			_newMsgIndicator.setVisible(false);
		}
	}
	
	private void disableActionButtons(boolean disable) {
		_foldButton.setDisable(disable);
		_callButton.setDisable(disable);
		_checkButton.setDisable(disable);
		_raiseButton.setDisable(disable || !_stakeValueField.isValid());
	}
	
	private void canCheck(boolean canCheck) {
		_checkButton.setManaged(canCheck);
		_callButton.setManaged(!canCheck);
		_callButton.setVisible(!canCheck);
		_checkButton.setVisible(canCheck);
	}
	
	private void showHelp() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(GameController.class.getResource("/fxml/HandsHelperDialog.fxml"));
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Les mains au poker");
			dialogStage.setResizable(false);
		    dialogStage.initOwner(getMainApp().getStage());
		    dialogStage.setScene(new Scene(loader.load()));
		     
		    dialogStage.show();
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.corruptedInstallation();
		}
	}
	
	private void startGame(BaseGameEngine engine) {
		_engine = engine;
		
		updatePlayers(_engine.createInterfacesForAllPlayers(FxThreadExecutor.getInstance()));
		
		_engineInterface = _engine.addEngineInterface(FxThreadExecutor.getInstance(), this);
		
		_engineInterface.getPotProperty().addListener((observable, oldValue, newValue) -> _potLabel.setValue(newValue.intValue()));
		
		_isSpectator = _clientPlayer == null;
		
		_playerActionPane.setVisible(!_isSpectator);
		
		if (_isSpectator)
			return;
		
		_engineInterface.getSmallBlindProperty().addListener((observable, oldValue, newValue) -> { 
			//final double value = newValue.doubleValue();
			
			/*_stakeValueSlider.setBlockIncrement(value);
			_stakeValueSlider.setMajorTickUnit(value);*/
			
			refreshMinRaise();
		});
		
		_engineInterface.getMaxRaiseProperty().addListener((observable, oldValue, newValue) -> {
			refreshMinRaise();
		});
		
		_engineInterface.getMaxStakeProperty().addListener((observable, oldValue, newValue) -> { 
			refreshMinRaise();
			
			refreshRaiseButtonText();
			
			//If call isn't equivalent to all in
			if (_engineInterface.getCallAmount(_clientPlayer) < _clientPlayer.getCash())
				canCheck(_engineInterface.canCheck(_clientPlayer));
			else {
				canCheck(false);
				_callButton.setVisible(false);
				_callButton.setManaged(false);
			}
		});
	}
	
	public void startGame(GameConfig gameConfig) {
		User u = getMainApp().getUser();
		
		ServerGameEngine engine = new ServerGameEngine.Builder(gameConfig)
									.addHumanPlayer(u.getPseudo(), u.getStats())
									.build();
		
		startGame(engine);
		
		/*PlayerInterface[] playersInterfaces = _engine.createInterfacesForAllPlayers(FxThreadExecutor.getInstance());
		
		if (playersInterfaces == null)
			goToNewScene(ClientApp.MAIN_MENU_SCENE);
		
		update(playersInterfaces);*/
		
		engine.startGame();
	}
	
	public void startGame(ClientGameEngine engine) {
		setClientGameEngine(engine);
		
		startGame((BaseGameEngine) engine);
		
		_chatControl.linkToEngine(getMainApp().getUser().getPseudo(), engine);
		_chatControl.setNewMsgIndicator(_newMsgIndicator);
		_chatButton.setVisible(true);
	}
	
	private void updatePlayers(PlayerInterface[] players) {
		try {
			int playersCount = players.length;
			
			if (playersCount < 2)
				throw new IllegalArgumentException("The players must be at least two");
			
			PlayerPosition[] positions = IOUtil.loadJsonFromFile(ClientApp.class.getResourceAsStream("/json/players_pos_" + playersCount + ".json"), PlayerPosition[].class);
			
			List<Node> centerPaneChildren = _centerPane.getChildren();
			int centerPaneChildrenCount = centerPaneChildren.size();
			
			for (int i = 0, max = _playersInfosControllers.size(); i < max; ++i)
				centerPaneChildren.remove(centerPaneChildrenCount - 1 - i);
			
			_playersInfosControllers.clear();
			
			User user = getMainApp().getUser();
			String userPseudo = user.getPseudo();
			
			for (int i = 0; i < playersCount; ++i) {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(ClientApp.class.getResource("/fxml/PlayerInfosControl.fxml"));
				
				PlayerPosition pos = positions[i];
				Region playerInfosControl = (Region) loader.load();
				
				_centerPane.add(playerInfosControl, pos.getX(), pos.getY(), pos.getXSpan(), pos.getYSpan());
				
				GridPane.setHalignment(playerInfosControl, pos.getHPos());
				GridPane.setValignment(playerInfosControl, pos.getVPos());
				
				_playersInfosControls.add(playerInfosControl);
				
				PlayerInfosController controller = loader.getController();
				
				PlayerInterface player = players[i];
				
				if (player.getName().equals(userPseudo)) {
					_clientPlayer = player;
					_clientPlayer.getStakeProperty().addListener((observable, oldValue, newValue) -> refreshMinRaise());
					_stakeValueField.maxProperty().bind(player.getCashProperty());
				}
				
				controller.setPlayer(player, _engine.getGameConfig().getPlayingTime());
				controller.setStakePosition(pos.getStakePos());
				
				_playersInfosControllers.add(controller);
			}
		} catch (IOException | JsonSyntaxException | JsonIOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.corruptedInstallation();
		}
	}
	
	private void refreshRaiseButtonText() {
		_raiseButton.setText(_stakeValueField.getValue().orElse(-1) >= _clientPlayer.getCash() ?
				"ALL-IN" : _engineInterface.getMaxStake() == 0 ?
						"Bet" : "Raise");
	}
	
	private void refreshMinRaise() {
		int min = Math.min(_clientPlayer.getCash(), _engineInterface.getMinRaise(_clientPlayer));
		
		_stakeValueField.setMin(min);
		_stakeValueField.setValue(min);
	}
	
	@Override
	public void gameStarted() { /* Empty */ }

	@Override
	public void roundStarted(boolean isFirstRound) {
		List<PlayerInfos> playersInfos = _engineInterface.getPlayersInfos();
		PlayerInfos clientPlayerInfos = _isSpectator ? null : _clientPlayer.getPlayerInfos();
		
		for (int i = 0; i < _playersInfosControllers.size(); ++i)
			_playersInfosControllers.get(i).setCardsVisible(playersInfos.get(i).equals(clientPlayerInfos));	
	}
	
	@Override
	public void turnStarted(int firstPlayerPos) { /* Empty */ }
	
	@Override
	public void nextPos(PlayerInfos p, int oldPos, int newPos) {
		_playersInfosControllers.get(oldPos).hasPlayed();
		_playersInfosControllers.get(newPos).isPlaying();
		
		if (!_isSpectator)
			disableActionButtons(!p.equals(_clientPlayer.getPlayerInfos()));
	}

	@Override
	public void turnCompleted(int lastPlayerPos) {
		if (!_isSpectator)
			disableActionButtons(true);
		
		_playersInfosControllers.get(_engineInterface.toRealPos(lastPlayerPos)).hasPlayed();
	}

	@Override
	public void roundCompleted(boolean onlyOneNonFoldedPlayer) {
		getMainApp().getUser().updateStats(_clientPlayer.getStats().get());
		
		if (onlyOneNonFoldedPlayer)
			return;
		
		for (PlayerInfosController controller : _playersInfosControllers)
			controller.setCardsVisible(true);
	}
	
	@Override
	public void gameOver(List<PlayerRank> rankings) {
		if (rankings == null) {
			goToNewScene(ClientApp.MAIN_MENU_SCENE);
			return;
		}				
		
		try {
			StageInfos<RankingsDialogController> dialogInfos = DialogUtil.createDialogStage(
					StageStyle.UNDECORATED,
					GameController.class.getResource("/fxml/RankingsDialog.fxml"),
					"",
					false,
					Modality.WINDOW_MODAL,
					getMainApp().getStage());

			Stage dialogStage = dialogInfos.getStage();
			
	        RankingsDialogController controller = dialogInfos.getController();
	        controller.setDialogStage(dialogStage);
	        controller.setRankings(new ArrayList<>(rankings));

	        DialogUtil.showAndWaitCentered(dialogStage);
	        
	        goToNewScene(ClientApp.MAIN_MENU_SCENE);
	    } catch (IOException e) {
	        BaseApp.logException(Level.SEVERE, e);
	        BaseApp.ALERT_UTIL.corruptedInstallation();
	    }
	}
	
	@Override
	public void newBoardCards(int addIndex, List<Card> newCards) {
		if (_currentBoardAnim != null)
			_currentBoardAnim.stop();
		
		_currentBoardAnim = new SequentialTransition();
		List<Node> boardPaneChildren = _boardPane.getChildren();
		
		for (int i = 0, size = newCards.size(); i < size; ++i) {
			ImageView iv = (ImageView) boardPaneChildren.get(addIndex + i);
			iv.setImage(newCards.get(i).getBigImage());
			
			((SequentialTransition) _currentBoardAnim).getChildren().add(AnimUtil.fadeInAnim(iv, Duration.millis(ServerGameEngine.BOARD_CARDS_APPEARANCE_DELAY)));
		}
		
		_currentBoardAnim.play();
	}
	
	@Override
	public void boardCleared() {
		if (_currentBoardAnim != null)
			_currentBoardAnim.stop();
		
		_currentBoardAnim = AnimUtil.parallelFadeOutAnims(Duration.millis(ServerGameEngine.BOARD_CARDS_APPEARANCE_DELAY), _boardPane.getChildren());
		
		_currentBoardAnim.setOnFinished(e -> clearBoardImages());
		
		_currentBoardAnim.play();
	}

	@Override
	public void playerBusted(int pos, String name) {
		_playersInfosControllers.remove(pos);
		
		final Region control = _playersInfosControls.get(pos);
		
		_playersInfosControls.remove(pos);
		
		FadeTransition fadeOut = AnimUtil.fadeOutAnim(control, Duration.millis(1000.0));
		fadeOut.setOnFinished(e -> _centerPane.getChildren().remove(control));
		
		fadeOut.play();
		
		if (name.equals(_clientPlayer.getName())) {
			_isSpectator = true;
			_playerActionPane.setVisible(false);
		}
	}
	
	private void clearBoardImages() {
		List<Node> boardPaneChildren = _boardPane.getChildren();
		
		for (Node iv : boardPaneChildren)
			((ImageView) iv).setImage(null);
	}
		
	private void quit() {
		disconnect();
		
		if (_engine != null) {
			_engine.removeEngineInterface(_engineInterface);
			
			_engine.shutdown();
			_engine = null;
		}

		_engineInterface = null;
		
		clearBoardImages();
	}
	
	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		getMainApp().getStage().addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
	}
	
	@Override
	protected boolean onNavigateFrom() {
		if (!connectionLost() && !_engineInterface.isGameOver() && !showQuitConfirmationDialog())
			return false;
		
		quit();
		
		getMainApp().getStage().removeEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
		
		return super.onNavigateFrom();
	}
	
	private boolean showQuitConfirmationDialog() {
		return BaseApp.ALERT_UTIL.showQuitConfirmationMsg("Voulez-vous réellement quitter la partie ?");
	}
}
