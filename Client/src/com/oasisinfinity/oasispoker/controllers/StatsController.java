package com.oasisinfinity.oasispoker.controllers;

import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.models.User;
import com.oasisinfinity.oasispoker.views.StatsControl;

import javafx.fxml.FXML;

public class StatsController extends ClientBaseController {
	@FXML private StatsControl _statsControl;
	
	public StatsController() {
		super();
	}
	
	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		User user = getMainApp().getUser();
		
		if (user == null) {
			BaseApp.logMsg(Level.WARNING, "StatsController : User is null");
			return;
		}
		
		_statsControl.setStats(user.getStats());
	}
}
