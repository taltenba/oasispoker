package com.oasisinfinity.oasispoker.controllers;

import java.util.List;

import com.oasisinfinity.oasispoker.models.PlayerRank;

import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class RankingsDialogController {
	@FXML private GridPane _rankingsPane;
	@FXML private Button _closeButton;
	
	private Stage _dialogStage;
	
	public RankingsDialogController() {
		// Empty
	}

	@FXML
	private void initialize() {
		_closeButton.setOnMouseClicked(e -> _dialogStage.close());
	}
	
	public void setDialogStage(Stage dialogStage) {
		_dialogStage = dialogStage;
	}
	
	public void setRankings(List<PlayerRank> rankings) {
		rankings.sort(null);
		
		for (int i = 0, size = rankings.size(); i < size; ++i) {
			PlayerRank playerRank = rankings.get(i);
			int rank = playerRank.getRank();
			
			Label ordinal = new Label(rank + " - ");
			Label playerName = new Label(playerRank.getPlayerName());
			
			String fontSizeStyle = "-fx-font-size: 14pt;";
			
			ordinal.setStyle(fontSizeStyle);
			playerName.setStyle(playerName.getStyle() + fontSizeStyle);
			
			if (rank == 1) {
				String textFillStyle = "-fx-text-fill: gold;";
				
				ordinal.setStyle(ordinal.getStyle() + textFillStyle);
				playerName.setStyle(playerName.getStyle() + textFillStyle);
			}
			
			_rankingsPane.addRow(i, ordinal, playerName);
			
			GridPane.setHalignment(ordinal, HPos.RIGHT);
			GridPane.setHalignment(playerName, HPos.LEFT);
			GridPane.setValignment(ordinal, VPos.CENTER);
			GridPane.setValignment(playerName, VPos.CENTER);
		}
	}
}
