package com.oasisinfinity.oasispoker.controllers;

import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.Level;

import com.oasisinfinity.controls.AutoCommitSpinner;
import com.oasisinfinity.controls.NoDelayTooltip;
import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.ClientApp;
import com.oasisinfinity.oasispoker.engines.ClientGameEngine;
import com.oasisinfinity.oasispoker.engines.GameEngineInterface;
import com.oasisinfinity.oasispoker.listeners.ConnectionAdapter;
import com.oasisinfinity.oasispoker.models.ServerInfos;
import com.oasisinfinity.oasispoker.models.User;
import com.oasisinfinity.oasispoker.network.Message.Reason;
import com.oasisinfinity.utils.StringUtil;
import com.oasisinfinity.utils.javafx.FxThreadExecutor;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.WindowEvent;

public class ServerConnectionController extends ClientBaseController {
	@FXML private TextField _ipField;
	@FXML private AutoCommitSpinner _portSpinner;
	@FXML private CheckBox _connectAsSpectatorCheckBox;
	@FXML private Label _portLabel;
	@FXML private Button _connectButton;
	
	private GameEngineInterface _engineInterface;
	
	private final EventHandler<WindowEvent> _closeEvent = new EventHandler<WindowEvent>() {
		@Override
		public void handle(WindowEvent e) {
			disconnect();
		}
	};
	
	public ServerConnectionController() {
		super();
	}

	@FXML
	@Override
	protected void initialize() {
		super.initialize();
		
		_portLabel.setTooltip(new NoDelayTooltip(_portLabel, "Veillez � ce que le port soit disponible et ouvert pour tous vos pare-feux dans l'optique d'une connection TCP"));
		_connectButton.setOnMouseClicked(e -> onConnectButtonClicked());
		
		_ipField.textProperty().addListener((observable, oldValue, newValue) -> {
			_connectButton.setDisable(!isValidIP());
		});
	}
	
	private void connectionFailed(String msg) {
		BaseApp.ALERT_UTIL.showErrorMsg("Connexion impossible", msg);
		setButtonsState(false);
	}
	
	private void onConnectButtonClicked() {
		setButtonsState(true);
		
		ClientGameEngine engine;
		
		try {
			engine = ClientGameEngine.newInstance(InetAddress.getByName(_ipField.getText()), _portSpinner.getValue());
		} catch (IOException e) {
			BaseApp.logException(Level.INFO, e);
			
			BaseApp.ALERT_UTIL.showErrorMsg("Connexion impossible", "Veuillez v�rifier les donn�es puis r�essayer");
			setButtonsState(false);
			
			return;
		}
		
		setClientGameEngine(engine);
		
		_engineInterface = engine.addEngineInterface(FxThreadExecutor.getInstance(), null, new ConnectionAdapter() {
			@Override
			public void connectionRejected(Reason reason) {
				ServerConnectionController.this.connectionFailed(reason.toString());
			}
			
			@Override
			public void connectionFailed(Exception e) {
				ServerConnectionController.this.connectionFailed("Veuillez v�rifier les donn�es puis r�essayer");
				getGameEngine().shutdown();
				//BaseApp.logException(Level.INFO, e);
			}
			
			@Override
			public void connected(ServerInfos serverInfos) {
				goToNewScene(ClientApp.SERVER_WAITER_SCENE);
				
				((ServerWaiterController) getMainApp().getController(ClientApp.SERVER_WAITER_SCENE)).setClientGameEngine(engine, serverInfos);
				
				getGameEngine().removeEngineInterface(_engineInterface);
			}
		});
		
		User u = getMainApp().getUser();
		engine.connect(u.getPseudo(), u.getStats(), _connectAsSpectatorCheckBox.isSelected());
	}
	
	private void setButtonsState(boolean connectingState) {
		_connectButton.setDisable(connectingState || !isValidIP());
		_connectButton.setText(connectingState ? "Tentative de connexion..." : "Connexion");
		
		getGoBackButton().setDisable(connectingState);
	}
	
	private boolean isValidIP() {
		return StringUtil.isValidIP(_ipField.getText());
	}
	
	@Override
	protected void onReturnToMainScene() {
		disconnect();
		
		super.onReturnToMainScene();
	}
	
	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		getMainApp().getStage().addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
		
		setButtonsState(false);
	}
	
	@Override
	protected boolean onNavigateFrom() {
		getMainApp().getStage().removeEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
		
		return super.onNavigateFrom();
	}
}
