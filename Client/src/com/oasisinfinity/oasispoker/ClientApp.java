package com.oasisinfinity.oasispoker;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.controllers.GameController;
import com.oasisinfinity.oasispoker.listeners.SettingsAdapter;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.User;
import com.oasisinfinity.utils.io.IOUtil;
import com.oasisinfinity.utils.javafx.MediaPlaylist;

import javafx.stage.Stage;

public class ClientApp extends BaseApp {
	public static final String
		MAIN_MENU_SCENE = "MainMenu",
		NEW_GAME_CONFIGURATION_SCENE = "NewGameConfiguration",
		SERVER_CONNECTION_SCENE = "ServerConnection",
		SERVER_WAITER_SCENE = "ServerWaiter",
		SETTINGS_SCENE = "Settings",
		STATS_SCENE = "Stats",
		GAME_SCENE = "Game";
	
	private static final int AUDIO_TRACKS_COUNT = 9;
	
	private final MediaPlaylist _playlist;
	private String _userAccountName;
	private User _user;

	public ClientApp() throws URISyntaxException {
		super(MAIN_MENU_SCENE);
		
		URI[] tracksUris = new URI[AUDIO_TRACKS_COUNT];
		
		for (int i = 0; i < AUDIO_TRACKS_COUNT; ++i)
			tracksUris[i] = ClientApp.class.getResource("/audio/track_" + i + ".mp3").toURI();
		
		_playlist = new MediaPlaylist(tracksUris);
		
		///
		ProcessBuilder ps = new ProcessBuilder("java.exe","-version");
		ps.redirectErrorStream(true);
		
		Process pr = null;
				
		try {
			pr = ps.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (pr == null)
			return;
	    
	    try (BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()))) {
	    	String line;
	    	
			while ((line = in.readLine()) != null)
			   logMsg(Level.INFO, line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    ///
	}

	@Override
	public void start(Stage primaryStage) {
		_userAccountName = getParameters().getNamed().get(USER_NAME_ARG_KEY);

		if (_userAccountName == null) {
			//_userAccountName = "DEBUG";
			BaseApp.ALERT_UTIL.showErrorMsg("Démarrage impossible", "Veuillez lancer l'application en utilisant le launcher");
			return;
		}

		File userSave = new File(BaseApp.createAccountPath(_userAccountName));
		
		if (!userSave.exists()) {
			BaseApp.ALERT_UTIL.showErrorMsg("Démarrage impossible", "Impossible de démarrer le client, le compte utilisateur n'existe pas.");
			_userAccountName = null;
			return;
		}

		try {
			_user = IOUtil.loadJsonFromFile(userSave, User.class);
		} catch (IOException e) {
			logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.showErrorMsg("Démarrage impossible", "Impossible de démarrer le client, une erreur est survenue pendant le chargement du compte utilisateur. Consultez les logs pour plus de détails.");
			_userAccountName = null;
			return;
		}

		super.start(primaryStage, "OasisPoker - " + _userAccountName, false);

		_user.addSettingsListener(new SettingsAdapter() {
			@Override
			public void newMusicVolume(double newVolume) {
				_playlist.setVolume(newVolume);
			}
		});

		_playlist.setVolume(_user.getMusicVolume());
		_playlist.play();
	}

	@Override
	protected void loadScenes() throws IOException {
		loadScene(MAIN_MENU_SCENE);
		loadScene(NEW_GAME_CONFIGURATION_SCENE);
		loadScene(SERVER_CONNECTION_SCENE);
		loadScene(SERVER_WAITER_SCENE);
		loadScene(SETTINGS_SCENE);
		loadScene(STATS_SCENE);
		loadScene(GAME_SCENE);
	}

	@Override
	public void stop() {
		saveUser();
		
		_playlist.dispose();

		try {
			super.stop();
		} catch (Exception e) {
			logException(Level.SEVERE, e);
		}
	}

	public void startGame(GameConfig gameConfig) {
		((GameController) getController(GAME_SCENE)).startGame(gameConfig);
		goToNewScene(ClientApp.GAME_SCENE);
	}
	
	public void saveUser() {
		if (_userAccountName == null)
			return;
		
		try {
			IOUtil.saveJson(BaseApp.createAccountPath(_userAccountName), _user); //reset
			_user.setSaved();
		} catch (IOException e) {
			logException(Level.SEVERE, e);
		}
	}

	public User getUser() {
		return _user;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
