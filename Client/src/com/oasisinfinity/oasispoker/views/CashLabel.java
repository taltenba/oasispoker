package com.oasisinfinity.oasispoker.views;

import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;

public class CashLabel extends Label {
	public final static int DEFAULT_VALUE_CHANGE_DURATION = 500;
	public final static int DEFAULT_INCREMENT_DURATION = 50;
	
	private final IntegerProperty _valueChangeDuration;
	private final IntegerProperty _incrementDuration;
	private final IntegerProperty _value;
	private final StringProperty _prefix;
	private Timer _timer;
	
	public CashLabel() {
		this(0, DEFAULT_VALUE_CHANGE_DURATION, DEFAULT_INCREMENT_DURATION, "");
	}
	
	public CashLabel(int initialValue) {
		this(initialValue, DEFAULT_VALUE_CHANGE_DURATION, DEFAULT_INCREMENT_DURATION, "");
	}
	
	public CashLabel(int initialValue, int valueChangeDuration, int incrementDuration, String prefix) {
		super();
		
		if (incrementDuration > valueChangeDuration)
			throw new IllegalArgumentException("Increment duration cannot be longer than value change duration");
		
		_valueChangeDuration = new SimpleIntegerProperty(this, "valueChangeDuration", valueChangeDuration);
		_incrementDuration = new SimpleIntegerProperty(this, "incrementDuration", incrementDuration);
		_value = new SimpleIntegerProperty(this, "value", initialValue);
		_prefix = new SimpleStringProperty(this, "prefix", prefix);
		
		textProperty().bind(_prefix.concat(_value.asString(" $ %,d")));
	}
	
	public IntegerProperty getValueChangeDurationProperty() {
		return _valueChangeDuration;
	}
	
	public IntegerProperty getIncrementDurationProperty() {
		return _incrementDuration;
	}
	
	public StringProperty getPrefixProperty() {
		return _prefix;
	}
	
	public int getValueChangeDuration() {
		return _valueChangeDuration.get();
	}
	
	public int getIncrementDuration() {
		return _incrementDuration.get();
	}
	
	public String getPrefix() {
		return _prefix.get();
	}
	
	public int getValue() {
		return _value.get();
	}
	
	public void setValueChangeDuration(int millis) {
		if (millis < _incrementDuration.get())
			throw new IllegalArgumentException("Value change duration cannot be shorter than increment duration");
		
		_valueChangeDuration.set(millis);
	}
	
	public void setIncrementDuration(int millis) {
		if (millis > _valueChangeDuration.get())
			throw new IllegalArgumentException("Increment duration cannot be longer than value change duration");
		
		_valueChangeDuration.set(millis);
	}
	
	public void setValue(int value) {
		changeValue(value);
	}
	
	public void setPrefix(String prefix) {
		_prefix.set(prefix);
	}
	
	public void bindValueProperty(ReadOnlyIntegerProperty property) {
		property.addListener((observable, oldValue, newValue) -> changeValue(newValue.intValue()));
		
		changeValue(property.get());
	}
	
	private void changeValue(final int toValue) {
		final double difference = toValue - _value.get();
		
		if (difference == 0)
			return;
		
		if (_timer != null) {
			_timer.cancel();
			_timer.purge();
		}

		_timer = new Timer();
		
		_timer.scheduleAtFixedRate(new ChangeValueTask(
					_value.get(), 
					toValue, 
					(difference / _valueChangeDuration.get()) * _incrementDuration.get()), 
				0, _incrementDuration.get());
	}
	
	private class ChangeValueTask extends TimerTask {
		private volatile double _realCurrentValue;
		private final double _increment;
		private final int _toValue;
		
		public ChangeValueTask(int fromValue, int toValue, double increment) {
			_realCurrentValue = (double) fromValue;
			_increment = increment;
			_toValue = toValue;
		}
		
		@Override
		public void run() {
			_realCurrentValue += _increment;	
			
			Platform.runLater(() -> _value.set((int) Math.round(_realCurrentValue)));
			
			if (Math.abs(_realCurrentValue - _toValue) < 0.0000001) {
				_timer.cancel();
				_timer.purge();
			}
		}
	}
}
