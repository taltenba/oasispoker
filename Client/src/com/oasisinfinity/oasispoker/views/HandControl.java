package com.oasisinfinity.oasispoker.views;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.models.Hand.HandType;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class HandControl extends GridPane {
	@FXML private Label _handNameLabel;
	
	private final ObjectProperty<Hand> _hand;
	private final IntegerProperty _fontSize;
	private final BooleanProperty _showName;
	
	public HandControl() {
		this(null);
	}
	
	public HandControl(Hand hand) {
		this(hand, 11);
	}
	
	public HandControl(Hand hand, int fontSize) {
		this(hand, fontSize, true);
	}
	
	public HandControl(Hand hand, int fontSize, boolean showName) {
		super();
		
		_hand = new SimpleObjectProperty<>(this, "hand", hand);
		_fontSize = new SimpleIntegerProperty(this, "fontSize", fontSize);
		_showName = new SimpleBooleanProperty(this, "showName", showName);
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(HandControl.class.getResource("/fxml/HandControl.fxml"));
		loader.setRoot(this);
		loader.setController(this);
		
		try {
			loader.load();
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.corruptedInstallation();
			return;
		}	
		
		_hand.addListener((observable, oldValue, newValue) -> refreshHand());
		_fontSize.addListener((observable, oldValue, newValue) -> refreshFontSize());
		
		refreshFontSize();
		refreshHand();
	}
	
	@FXML
	private void initialize() {
		_handNameLabel.visibleProperty().bind(_showName);
	}

	public ObjectProperty<Hand> handProperty() {
		return _hand;
	}
	
	public IntegerProperty fontSizeProperty() {
		return _fontSize;
	}
	
	public BooleanProperty showNameProperty() {
		return _showName;
	}
	
	public void setHand(Hand hand) {
		_hand.set(hand);
	}
	
	public void setFontSize(int pt) {
		_fontSize.set(pt);
	}
	
	public void setShowName(boolean showName) {
		_showName.set(showName);
	}
	
	public Hand getHand() {
		return _hand.get();
	}
	
	public int getFontSize() {
		return _fontSize.get();
	}
	
	public boolean getShowName() {
		return _showName.get();
	}
	
	public String getHandName() {
		Hand hand = _hand.get();
		
		return hand == null ? null : hand.getType().toString();
	}
	
	private void refreshFontSize() {
		_handNameLabel.setStyle("-fx-font-size:" + _fontSize.get() + "pt");
	}
	
	private void refreshHand() {
		Hand hand = _hand.get();
		List<Node> children = getChildren();
		
		for (int i = 0; i < children.size(); ++i) {
			if (children.get(i) instanceof ImageView)
				children.remove(i);
		}
		
		if (hand == null)
			return;
		
		HandType handType = hand.getType();
		Card[] handCards = hand.getCards();
		
		for (int i = 0; i < 5 && i < handCards.length; ++i) {
			ImageView iv = new ImageView(handCards[i].getImage());
			iv.setFitHeight(72.0);
			iv.setFitWidth(50.0);
			
			children.add(iv);
			
			GridPane.setColumnIndex(iv, i);
		}
		
		_handNameLabel.setText(handType.toString());
	}
}
