package com.oasisinfinity.oasispoker.views;

import java.io.IOException;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.models.Hand.HandType;
import com.oasisinfinity.utils.io.IOUtil;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class ExampleHandControl extends HandControl {
	private final ObjectProperty<HandType> _handType;
	
	public ExampleHandControl() {
		this(null);
	}
	
	public ExampleHandControl(HandType handType) {
		super();
		
		setFontSize(27);
		
		_handType = new SimpleObjectProperty<>(this, "handType", handType);
		_handType.addListener((observable, oldValue, newValue) -> refreshHandType());
		
		refreshHandType();
	}
	
	public ObjectProperty<HandType> handTypeProperty() {
		return _handType;
	}
	
	public void setHandType(HandType handType) {
		_handType.set(handType);
	}
	
	public HandType getHandType() {
		return _handType.get();
	}
	
	private void refreshHandType() {
		HandType handType = _handType.get();
		
		if (handType == null)
			return;
		
		Card[] example;
		
		try {
			example = IOUtil.loadJsonFromFile(HandControl.class.getResourceAsStream("/json/hand_example_" + handType.name().toLowerCase() + ".json"), Card[].class);
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.corruptedInstallation();
			return;
		}
		
		setHand(Hand.newHand(example));
	}
}
