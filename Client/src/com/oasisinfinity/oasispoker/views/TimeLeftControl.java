package com.oasisinfinity.oasispoker.views;

import java.io.IOException;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.controllers.PlayerInfosController;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.util.Duration;

public class TimeLeftControl extends GridPane {
	private final IntegerProperty _startTime;
	private final IntegerProperty _timeLeft;
	private final Timeline _timeline;
	
	@FXML private Label _timeLeftLabel;
	
	public TimeLeftControl() {
		this(30);
	}
	
	/**
	 * 
	 * @param startTime in seconds
	 */
	public TimeLeftControl(int startTime) {
		super();
		
		_startTime = new SimpleIntegerProperty(this, "startTime", startTime);
		_timeLeft = new SimpleIntegerProperty(startTime);
		
		_timeline = new Timeline();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(PlayerInfosController.class.getResource("/fxml/TimeLeftControl.fxml"));
		loader.setRoot(this);
		loader.setController(this);
		
		try {
			loader.load();
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);	
			BaseApp.ALERT_UTIL.corruptedInstallation();
			return;
		}	
	}
	
	@FXML
	private void initialize() {
		_timeLeftLabel.textProperty().bind(_timeLeft.asString("%02d"));
		_timeline.cycleCountProperty().bind(_startTime);
		
		_timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1), e -> {
			_timeLeft.set(_timeLeft.get() - 1);
		}));
	}
	
	public void start() {
		_timeline.playFromStart();
	}
	
	public void stopAndReset() {
		_timeline.stop();
		_timeLeft.set(_startTime.get());
	}
	
	public void setStartTime(int startTime) {
		_startTime.set(startTime);
		_timeLeft.set(startTime);
	}
}
