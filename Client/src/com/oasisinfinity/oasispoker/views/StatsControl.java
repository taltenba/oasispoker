package com.oasisinfinity.oasispoker.views;

import java.io.IOException;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.engines.ServerGameEngine.Phase;
import com.oasisinfinity.oasispoker.models.StatsInfos;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;

public class StatsControl extends ScrollPane {
	@FXML private GridPane _statsPane;
	@FXML private Label _gainsLossesBalanceLabel;
	@FXML private Label _gainsLossesRatioLabel;
	@FXML private Label _biggestWonPotLabel;
	@FXML private Label _playedHandsLabel;
	@FXML private Label _averageCashPerHandLabel;
	@FXML private Label _raiseFrequencyLabel;
	@FXML private Label _totalFoldedHandsLabel;
	@FXML private Label _preFlopFoldedHandsLabel;
	@FXML private Label _flopFoldedHandsLabel;
	@FXML private Label _turnFoldedHandsLabel;
	@FXML private Label _riverFoldedHandsLabel;
	@FXML private Label _bestHandNameLabel;
	@FXML private HandControl _bestHandHandControl;
	
	public StatsControl() {
		super();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(StatsControl.class.getResource("/fxml/StatsControl.fxml"));
		loader.setRoot(this);
		loader.setController(this);
		
		try {
			loader.load();
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.corruptedInstallation();
			return;
		}	
	}
	
	@FXML
	private void initialize() {
		_statsPane.minHeightProperty().bind(heightProperty().subtract(2.0));
	}
	
	public void setStats(StatsInfos stats) {
		int wonCash = stats.getWonCash();
		int lostCash = stats.getLostCash();
		int cashBalance = wonCash - lostCash;
		int playedHands = stats.getTotalPlayedHands();
		
		_gainsLossesBalanceLabel.setText(formatCash(cashBalance));
		_gainsLossesRatioLabel.setText(formatCashRatio(wonCash, lostCash));
		_biggestWonPotLabel.setText(formatCash(stats.getBiggestWonPot()));
		_playedHandsLabel.setText(String.format("%,d", playedHands));
		_averageCashPerHandLabel.setText(formatCashRatio(cashBalance, playedHands));
		_raiseFrequencyLabel.setText(formatPercentage(stats.getRaisesCount(), stats.getTotalPlayedActions()));
		_totalFoldedHandsLabel.setText(formatPercentage(stats.getTotalFoldedHands(), playedHands));
		_preFlopFoldedHandsLabel.setText(formatPercentage(stats.getFoldedHands(Phase.PreFlop), playedHands));
		_flopFoldedHandsLabel.setText(formatPercentage(stats.getFoldedHands(Phase.Flop), playedHands));
		_turnFoldedHandsLabel.setText(formatPercentage(stats.getFoldedHands(Phase.Turn), playedHands));
		_riverFoldedHandsLabel.setText(formatPercentage(stats.getFoldedHands(Phase.River), playedHands));
		
		_bestHandHandControl.setHand(stats.getBestHand());
		
		String bestHandName = _bestHandHandControl.getHandName();
		
		_bestHandNameLabel.setText(bestHandName == null ? "-" : bestHandName);
	}
	
	private static String formatCash(int cash) {
		return String.format("$ %,d", cash);
	}

	private static String formatPercentage(int dividend, int divisor) {
		return String.format("%,.2f %%", divisor == 0 ? 0.0f : ((float) dividend / divisor) * 100.0f);
	}
	
	private static String formatCashRatio(int cash, int divisor) {
		return divisor == 0 ? "-" : String.format("$ %,.2f", (float) cash / divisor);
	}
}
