package com.oasisinfinity.oasispoker.views;

import java.io.IOException;
import java.util.logging.Level;

import org.fxmisc.richtext.InlineCssTextArea;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.engines.ClientGameEngine;
import com.oasisinfinity.oasispoker.listeners.ChatListener;
import com.oasisinfinity.oasispoker.models.ChatMessage;
import com.oasisinfinity.utils.javafx.FxThreadExecutor;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class ChatControl extends VBox implements ChatListener {
	@FXML private InlineCssTextArea _chatArea;
	@FXML private TextField _newMsgField;
	
	private String _userName;
	private ClientGameEngine _engine;
	private Node _newMsgIndicator;
	
	public ChatControl() {
		super();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(ChatControl.class.getResource("/fxml/ChatControl.fxml"));
		loader.setRoot(this);
		loader.setController(this);
		
		try {
			loader.load();
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);	
			BaseApp.ALERT_UTIL.corruptedInstallation();
			return;
		}	
	}
	
	@FXML
	private void initialize() {
		_newMsgField.setOnAction(e -> {
			String msg = _newMsgField.getText();
			
			if (msg == "")
				return;
			
			_engine.sendChatMessage(new ChatMessage(_userName, _newMsgField.getText()));
			_newMsgField.clear();
		});
	}
	
	public void linkToEngine(String userName, ClientGameEngine engine) {
		_userName = userName;
		_engine = engine;
		
		engine.addEngineInterface(FxThreadExecutor.getInstance(), null, null, this);
	}
	
	public void setNewMsgIndicator(Node indicator) {
		_newMsgIndicator = indicator;
	}
	
	@Override
	public void newChatMessage(ChatMessage msg) {
		int lastCharInd = _chatArea.getText().length() - 1;
		
		if (lastCharInd < 0)
			lastCharInd = 0;
		
		String senderTxt = msg.getSenderName() + " : ";
		
		_chatArea.appendText(senderTxt + msg.getMsg() + "\n");	
		_chatArea.setStyle(lastCharInd, lastCharInd + senderTxt.length(), "-fx-font-weight: bold;");
		
		if (!isVisible() && _newMsgIndicator != null)
			_newMsgIndicator.setVisible(true);
	}
}
