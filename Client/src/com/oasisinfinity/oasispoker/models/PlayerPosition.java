package com.oasisinfinity.oasispoker.models;

import com.oasisinfinity.oasispoker.controllers.PlayerInfosController.Position;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import net.jcip.annotations.Immutable;

@Immutable
public final class PlayerPosition {
	private final int _x;
	private final int _y;
	private final int _xSpan;
	private final int _ySpan;
	private final HPos _hPos;
	private final VPos _vPos;
	private final Position _stakePos;
	
	public PlayerPosition(int x, int y, int xSpan, int ySpan, HPos hPos, VPos vPos, Position stakePos) {
		_x = x;
		_y = y;
		_xSpan = xSpan;
		_ySpan = ySpan;
		_hPos = hPos;
		_vPos = vPos;
		_stakePos = stakePos;
	}
	
	public int getX() {
		return _x;
	}
	
	public int getY() {
		return _y;
	}

	public int getXSpan() {
		return _xSpan;
	}
	
	public int getYSpan() {
		return _ySpan;
	}
	
	public HPos getHPos() {
		return _hPos;
	}
	
	public VPos getVPos() {
		return _vPos;
	}
	
	public Position getStakePos() {
		return _stakePos;
	}
}
