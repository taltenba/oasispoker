package com.oasisinfinity.oasispoker.network;

import java.io.IOException;
import java.util.logging.Level;

import javax.swing.event.EventListenerList;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.listeners.DisconnectedListener;
import com.oasisinfinity.oasispoker.listeners.NewMessageListener;

import net.jcip.annotations.Immutable;

@Immutable
public class ClientTask implements Runnable {
	private final Client _client;
	private final EventListenerList _listeners;
	
	public ClientTask(Client client) {
		_client = client;
		_listeners = new EventListenerList();
	}

	@Override
	public void run() {
		try {
			String msg;
			
			while ((msg = _client.read()) != null) {
				try {
					onNewMessage(new Message(msg));
				} catch (IllegalArgumentException e1) {
					BaseApp.logException(Level.WARNING, e1);
				}
			}
		} catch (IOException e) {
			//Socket closed
			//BaseApp.logException(Level.INFO, e);
		} finally {
			onDisconnect();
		}
	}
	
	public void addNewMessageListener(NewMessageListener listener) {
		_listeners.add(NewMessageListener.class, listener);
	}
	
	public void removeNewMessageListener(NewMessageListener listener) {
		_listeners.remove(NewMessageListener.class, listener);
	}
	
	public void addDisconnectedListener(DisconnectedListener listener) {
		_listeners.add(DisconnectedListener.class, listener);
	}
	
	public void removeDisconnectedListener(DisconnectedListener listener) {
		_listeners.remove(DisconnectedListener.class, listener);
	}
	
	private void onNewMessage(Message msg) {
		for (NewMessageListener l : _listeners.getListeners(NewMessageListener.class))
			l.newMessage(_client, msg);
	}
	
	private void onDisconnect() {
		for (DisconnectedListener l : _listeners.getListeners(DisconnectedListener.class))
			l.disconnected(_client);
	}
}
