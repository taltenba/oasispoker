package com.oasisinfinity.oasispoker.network;

import com.oasisinfinity.utils.io.IOUtil;

import net.jcip.annotations.Immutable;

@Immutable
public class Message {
	private static final String ARG_SEPARATOR = "_=_";
	
	public static final Message MSG_READY = new Message(Instruction.Ready);
	public static final Message MSG_GAME_STARTED = new Message(Instruction.GameStarted);
	public static final Message MSG_BOARD_CLEARED = new Message(Instruction.BoardCleared);
	
	public enum Instruction {
		NewAction,
		ClientInfos,
		ConnectionRejected,
		ConnectionAccepted,
		Ready,
		GameStarted,
		GameOver,
		RoundStarted,
		RoundCompleted,
		TurnStarted,
		TurnCompleted,
		NextPos,
		PlayerBusted,
		NewBoardCards,
		BoardCleared,
		NewGameInfos,
		NewPlayersCount,
		NewChatMessage
	}
	
	public enum Reason {
		IdenticalNames("Le serveur contient d�j� un client poss�dant votre pseudo, veuillez en changer pour vous connecter."),
		ServerFull("Le serveur est complet."),
		Incompatible("Le serveur est incompatible avec votre version de OasisPoker."),
		GameAlreadyStarted("La partie a d�j� d�marr�e.");
		
		private String _msg;
		
		private Reason(String msg) {
			_msg = msg;
		}
		
		@Override
		public String toString() {
			return _msg;
		}
	}
	
	private final Instruction _instruction;
	private final String _arg;
	
	public Message(String msg) throws IllegalArgumentException {
		String[] decodedMsg = msg.split(ARG_SEPARATOR);
		
		_instruction = Instruction.valueOf(decodedMsg[0]);
		_arg = decodedMsg.length > 1 ? decodedMsg[1] : null;	
	}
	
	public Message(Instruction instruction) {
		this(instruction, null);
	}
	
	public Message(Instruction instruction, String arg) {
		_instruction = instruction;
		_arg = arg;
	}
	
	public Message(Instruction instruction, int arg) {
		_instruction = instruction;
		_arg = String.valueOf(arg);
	}
	
	public Message(Instruction instruction, boolean arg) {
		_instruction = instruction;
		_arg = String.valueOf(arg);
	}
	
	public Message(Instruction instuction, Object arg) {
		this(instuction, IOUtil.toJson(arg));
	}
	
	public Instruction getInstruction() {
		return _instruction;
	}
	
	public String getArg() {
		return _arg;
	}
	
	@Override
	public String toString() {
		return _instruction + (_arg != null ? ARG_SEPARATOR + _arg : "");
	}
}
