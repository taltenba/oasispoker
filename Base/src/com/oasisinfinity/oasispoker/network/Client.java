package com.oasisinfinity.oasispoker.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.oasisinfinity.oasispoker.models.ClientInfos;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.oasispoker.network.Message.Instruction;
import com.oasisinfinity.utils.io.IOUtil;

import net.jcip.annotations.NotThreadSafe;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class Client {
	private final Socket _socket;
	private final ClientInfos _infos;
	private final PrintWriter _out;
	private final BufferedReader _in;
	
	private volatile boolean _isReady;
	private volatile boolean _isConnected;
	
	@NotThreadSafe
	public static class Builder {
		private final Socket _socket;
		
		private ClientInfos _infos;
		
		public Builder(Socket s) {
			this(s, null);
		}
		
		public Builder(Socket s, ClientInfos infos) {
			_socket = s;
			_infos = infos;
		}
		
		public void setInfos(ClientInfos infos) {
			_infos = infos;
		}
		
		public Client build() throws IOException {
			if (_infos == null) {
				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(_socket.getInputStream()));
					Message infosMsg = new Message(in.readLine());
					
					if (infosMsg.getInstruction() != Instruction.ClientInfos)
						throw new IllegalStateException("ClientInfos are null");
					
					_infos = IOUtil.loadJson(infosMsg.getArg(), ClientInfos.class);
				} catch (Exception e) {
					_socket.close();
					throw e;
				}
			}
			
			return new Client(_socket, _infos);
		}
	}
	
	private Client(Socket s, ClientInfos infos) throws IOException {
		if (infos == null)
			throw new IllegalStateException("ClientInfos are null");
		
		_socket = s;
		
		try {
			_out = new PrintWriter(s.getOutputStream(), true);
			_in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		} catch (IOException e) {
			disconnect();
			throw e;
		}

		_infos = infos;
		_isConnected = true;
	}

	public void write(String msg) {
		_out.println(msg);
	}
	
	public String read() throws IOException {
		return _in.readLine();
	}
	
	public ClientInfos getInfos() {
		return _infos;
	}
	
	public String getName() {
		return _infos.getName();
	}
	
	public StatsInfos getStats() {
		return _infos.getStats();
	}
	
	public boolean isSpectator() {
		return _infos.isSpectator();
	}
	
	public int getCompatibilityVersion() {
		return _infos.getCompatibilityVersion();
	}
	
	public boolean isConnected() {
		return _isConnected;
	}
	
	public boolean isReady() {
		return _isReady;
	}
	
	public void setReady() {
		_isReady = true;
	}
	
	public void disconnect() throws IOException {
		_isConnected = false;
		
		if (_out != null)
			_out.close();
		
		try {
			if (_in != null)
				_in.close();
		} finally {
			_socket.close();
		}		
	}

	@Override
	public int hashCode() {
		return 31 * 17 + _infos.getName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (!(obj instanceof Client))
			return false;
		
		return _infos.getName().equals(((Client) obj)._infos.getName());
	}
}
