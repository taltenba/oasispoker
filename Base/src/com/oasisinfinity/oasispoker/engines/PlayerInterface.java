package com.oasisinfinity.oasispoker.engines;

import java.util.Optional;
import java.util.concurrent.Executor;

import com.oasisinfinity.oasispoker.engines.Player.Action;
import com.oasisinfinity.oasispoker.engines.Player.Role;
import com.oasisinfinity.oasispoker.engines.Player.Status;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.utils.ImmutablePair;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import net.jcip.annotations.NotThreadSafe;

/**
 * All public methods must be called in the interface executor's thread
 * @author OasisInfinity
 */
@NotThreadSafe
public final class PlayerInterface implements PlayerInfosGetter {
	private final Executor _interfaceExec;
	
	/* The values of properties must be confined in the _interfaceExec thread */
	private final ReadOnlyObjectWrapper<PlayerInfos> _playerInfos;
	private final ReadOnlyIntegerWrapper _cash;
	private final ReadOnlyObjectWrapper<Action> _action;
	private final ReadOnlyObjectWrapper<Role> _role;
	private final ReadOnlyIntegerWrapper _stake;
	private final ReadOnlyObjectWrapper<ImmutablePair<Card>> _cards;
	private final ReadOnlyObjectWrapper<Hand> _hand;
	private final ReadOnlyObjectWrapper<Status> _status;

	PlayerInterface(Executor interfaceExec) {
		_interfaceExec = interfaceExec;
		
		_playerInfos = new ReadOnlyObjectWrapper<>();
		_cash = new ReadOnlyIntegerWrapper();
		_action = new ReadOnlyObjectWrapper<Action>();
		_role = new ReadOnlyObjectWrapper<Role>();
		_stake = new ReadOnlyIntegerWrapper();
		_cards = new ReadOnlyObjectWrapper<ImmutablePair<Card>>(new ImmutablePair<Card>(null, null));
		_hand = new ReadOnlyObjectWrapper<Hand>();
		_status = new ReadOnlyObjectWrapper<Status>();
	}
	
	void updateInfos(final PlayerInfos playerInfos) {
		_interfaceExec.execute(() -> {
			_playerInfos.set(playerInfos);
			_cash.set(playerInfos.getCash());
			_action.set(playerInfos.getAction());
			_role.set(playerInfos.getRole());
			_stake.set(playerInfos.getStake());
			_hand.set(playerInfos.getHand());
			_status.set(playerInfos.getStatus());
			
			ImmutablePair<Card> newCards = playerInfos.getCards();
			
			if (!_cards.get().equals(newCards))
				_cards.set(newCards);
		});
	}
	
	public PlayerInfos getPlayerInfos() {
		return _playerInfos.get();
	}
	
	@Override
	public String getName() {
		return _playerInfos.get().getName();
	}
	
	@Override
	public Optional<StatsInfos> getStats() {
		return _playerInfos.get().getStats();
	}

	@Override
	public int getCash() {
		return _cash.get();
	}

	@Override
	public int getStake() {
		return _stake.get();
	}

	@Override
	public Action getAction() {
		return _action.get();
	}

	@Override
	public Role getRole() {
		return _role.get();
	}

	@Override
	public ImmutablePair<Card> getCards() {
		return _cards.get();
	}

	@Override
	public Hand getHand() {
		return _hand.get();
	}

	@Override
	public Status getStatus() {
		return _status.get();
	}
	
	@Override
	public boolean isFolded() {
		return getPlayerInfos().isFolded();
	}
	
	@Override
	public boolean hasBet() {
		return getPlayerInfos().hasBet();
	}

	@Override
	public boolean canPlay() {
		return getPlayerInfos().canPlay();
	}
	
	public ReadOnlyObjectProperty<PlayerInfos> getPlayerInfosProperty() {
		return _playerInfos.getReadOnlyProperty();
	}
	
	public ReadOnlyIntegerProperty getCashProperty() {
		return _cash.getReadOnlyProperty();
	}
	
	public ReadOnlyObjectProperty<Action> getActionProperty() {
		return _action.getReadOnlyProperty();
	}
	
	public ReadOnlyObjectProperty<Role> getRoleProperty() {
		return _role.getReadOnlyProperty();
	}
	
	public ReadOnlyIntegerProperty getStakeProperty() {
		return _stake.getReadOnlyProperty();
	}
	
	public ReadOnlyObjectProperty<ImmutablePair<Card>> getCardsProperty() {
		return _cards.getReadOnlyProperty();
	}
	
	public ReadOnlyObjectProperty<Hand> getHandProperty() {
		return _hand.getReadOnlyProperty();
	}
	
	public ReadOnlyObjectProperty<Status> getStatusProperty() {
		return _status.getReadOnlyProperty();
	}
}
