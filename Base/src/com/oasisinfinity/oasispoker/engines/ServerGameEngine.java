package com.oasisinfinity.oasispoker.engines;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.engines.Player.Action;
import com.oasisinfinity.oasispoker.engines.Player.Role;
import com.oasisinfinity.oasispoker.engines.Player.Status;
import com.oasisinfinity.oasispoker.models.ActionInfos;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.GameInfos;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.SecondaryPot;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.oasispoker.utils.PokerDeck;
import com.oasisinfinity.utils.CircularList;
import com.oasisinfinity.utils.Deck;
import com.oasisinfinity.utils.RandomUtil;
import com.oasisinfinity.utils.concurrent.ConcurrentUtil;
import com.oasisinfinity.utils.io.IOUtil;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class ServerGameEngine extends BaseGameEngine {
	public static final int CARDS_TRANSITION_DURATION = 1000;
	public static final int BOARD_CARDS_APPEARANCE_DELAY = 1000;
	
	private static final String[] BOTS_NAMES;
	
	static {
		String[] tmp = null;
		
		try {
			tmp = IOUtil.loadJsonFromFile(ServerGameEngine.class.getResourceAsStream("/json/bots_names.json"), String[].class);
		} catch (IOException e) {
			BaseApp.corruptedInstallation(e);
		}
		
		BOTS_NAMES = tmp;
	}
	
	public enum Phase {
		PreFlop (0),
		Flop (3),
		Turn (1),
		River (1) {
			@Override
			public Phase next() {throw new NoSuchElementException("There is no phase after the river");}
		};
		
		private final int _newCardsCount;
		
		private Phase(int newCardsCount) {
			_newCardsCount = newCardsCount;
		}
		
		public int getNewCardsCount() {
			return _newCardsCount;
		}
		
		public Phase next() {
			return values()[ordinal() + 1];
		}
	}
	
	//LOGGING
	private final GameLogger _logger;
	
	private final CircularList<Player> _players;
	private final List<Card> _board;
	private final PlayersRankings _rankings;
	private final Map<Player, SecondaryPot> _secondaryPots;
	private final GameConfig _config;
	private final MonteCarloEvaluator _winEvaluator;

	private int _smallBlind;
	private int _antes;
	private int _maxRaise;
	private int _maxStake;
	private int _currentPos;
	private int _pot;
	private int _foldedPlayersCount;
	private int _round;
	private int _lastRaiserPos;
	private int _dealerPos;
	private Phase _phase;
	private Deck<Card> _deck;
	private boolean _isGameOver;
	private Future<?> _playingTimeElapsedFuture;
	
	private final AtomicBoolean _isWaitingForInput;
	
	private ServerGameEngine(ScheduledExecutorService engineExec, GameConfig config, Map<String, StatsInfos> humanPlayers) {
		super(engineExec);
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> shutdown(), "EngineClearHookThread"));
		
		_logger = createLogger();
		
		_players = CircularList.wrapArrayList();
		
		int initialCash = config.getInitialCash();
		
		for (Map.Entry<String, StatsInfos> p : humanPlayers.entrySet())
			_players.add(new HumanPlayer(p.getKey(), p.getValue(), initialCash));
		
		int botsCount = config.getBotsCount();
		int[] rndIndexes = RandomUtil.generateDistinct(0, BOTS_NAMES.length, botsCount);
		
		for (int i = 0; i < botsCount; ++i)
			_players.add(new Bot("Bot " + BOTS_NAMES[rndIndexes[i]], initialCash));
		
		_board = new ArrayList<>();
		_rankings = new PlayersRankings();
		_winEvaluator = new MonteCarloEvaluator();
		_secondaryPots = new HashMap<>();
		_round = -1;
		_deck = PokerDeck.newDeck();
		_isGameOver = true;
		_config = config;
		_smallBlind = config.getInitialSmallBlind();
		_isWaitingForInput = new AtomicBoolean();
		
		updateGameInfos();
	}
	
	public void startGame() {
		if (isShutdown())
			throw new IllegalStateException("This engine has been shut down");
		
		if (!getGameInfos().isGameOver())
			throw new IllegalStateException("The game has already started");
		
		execute(() -> {
			log("------ STARTING GAME ------");
			
			_isGameOver = false;
			
			initRoles();
			
			newRound(true);
			
			onGameStarted();
		});
	}
	
	private void initRoles() {
		initRoles(RandomUtil.generateRandom(_players.size()));
	}
	
	private void initRoles(int dealerPos) {
		Role[] roles = Role.values();

		for (int i = 0, j = dealerPos; i < roles.length; ++i, ++j)
			_players.get(j).setRole(roles[i]);
	}
	
	private void redistributeCash(int cash) {
		int playersCount = _players.size();
		int cashByPlayer = Math.floorDiv(cash, playersCount);
		
		for (Player p : _players)
			p.win(cashByPlayer, false);
	}

	private void newRound(boolean isFirstRound) {
		log("### STARTING ROUND ###");
		
		++_round;

		if (_round != 0 && _round % _config.getBlindsIncreaseFrequency() == 0)
			_smallBlind = _smallBlind * _config.getBlindsIncreaseMultiplier();

		if (_round == _config.getAntesDelay())
			_antes = _config.getInitialAntes();
		else if (_round != 0 && _round % _config.getAntesIncreaseFrequency() == 0)
			_antes = _antes * _config.getAntesIncreaseMultiplier();

		_foldedPlayersCount = 0;
		_phase = Phase.PreFlop;
		_secondaryPots.clear();
		
		_dealerPos = toRealPos(_dealerPos);
		
		boolean playersBusted = false;
		int cashToRedistribute = 0;
		List<String> potentialDisconnectedWinners = new ArrayList<>();
		
		int pos = -1;
		
		for (Iterator<Player> it = _players.iterator(); it.hasNext(); ) {
			++pos;
			
			Player p = it.next();
			Status s = p.getStatus();
			
			if (s == Status.Busted || s == Status.Disconnected) {
				playersBusted = true;
				
				String pName = p.getName();
				int pCash = p.getCash();
				
				if (pCash > 0) { //Here, pCash > 0 iff p is disconnected and not busted
					potentialDisconnectedWinners.add(pName);
					cashToRedistribute += pCash;
				} else //p is busted
					_rankings.addPlayer(pName);
				
				it.remove();
				
				onPlayerBusted(pos, pName);
				
				if (pos <= _dealerPos)
					--_dealerPos;
				
				--pos;
				
				continue;
			}
			
			p.clearHand();
			p.clearStatus();
			p.clearAction();
		}
			
		if (playersBusted) {
			int remainingPlayers = _players.size();
			
			if (remainingPlayers > 0)
				_rankings.addAllPlayers(potentialDisconnectedWinners);
			
			_rankings.nextRank();
			
			redistributeCash(cashToRedistribute);
			
			if (remainingPlayers == 0) {
				endGame(potentialDisconnectedWinners);
				return;
			} else if (remainingPlayers == 1) {
				endGame(_players.get(0).getName());
				return;
			}
			
			initRoles(_dealerPos);
		}
		
		_board.clear();
		
		onBoardCleared();
		
		updateGameInfos();
		
		schedule(() -> newRoundTask(isFirstRound), isFirstRound ? 0 : CARDS_TRANSITION_DURATION, TimeUnit.MILLISECONDS);	
	}
	
	private void newRoundTask(boolean isFirstRound) {
		moveRoles();
		initStakes();
		dealCards();
		
		onRoundStarted(isFirstRound);
		onTurnStarted(_lastRaiserPos);
		
		log("### STARTING TURN ###");

		if (canPlayCount() <= 1)
			nextTurn();
		else
			nextPos(_lastRaiserPos);
	}

	private void moveRoles() {
		Role r = _players.get(-1).getRole();
		
		for (int i = 0, playersCount = _players.size(); i < playersCount; ++i) {
			Player p = _players.get(i);
			Role oldRole = p.getRole();

			p.setRole(r);

			if (r == Role.BigBlind)
				_lastRaiserPos = toRealPos(i + 1);
			else if (r == Role.Dealer || (playersCount == 2 && r == Role.SmallBlind))
				_dealerPos = i;

			r = oldRole;
		}
	}

	private void initStakes() {
		int bigBlind = _smallBlind * 2;

		for (Player p : _players) {
			pay(p, _antes);

			Role r = p.getRole();

			if (r == null)
				continue;

			switch (r) {
				case SmallBlind:
					pay(p, _smallBlind);
					break;
				case BigBlind:
					pay(p, bigBlind);
					break;
				default:
					break;
			}
		}

		_maxStake = bigBlind + _antes;
	}
	
	private void pay(Player p, int amount) {
		pay(p, amount, null);
	}
	
	private void pay(Player p, int amount, Action a) {
		int previousStake = p.getStake();
		int realAmount = p.pay(amount, a);
		
		_pot += realAmount;
		
		int newStake = previousStake + realAmount;
		
		_maxStake = Math.max(_maxStake, newStake);
		
		refreshSecondaryPots(previousStake, newStake);
		
		if (amount > realAmount) {
			int secondaryPotStartValue = _pot;
			
			for (Player other : _players) {
				int stake = other.getStake();
				
				if (stake > newStake)
					secondaryPotStartValue -= stake - newStake;
			}
			
			_secondaryPots.put(p, new SecondaryPot(secondaryPotStartValue, newStake));
		}
	}
	
	private int canPlayCount() {
		int count = 0;
		
		for (Player p : _players) {
			if (p.canPlay())
				++count;
		}
		
		return count;
	}

	private void dealCards() {
		_deck.refill();
		
		for (Player p : _players)
			p.setCards(_deck.draw(), _deck.draw());
	}

	private void nextPos() {
		nextPos(_currentPos + 1);
	}
	
	private void nextPos(int startPos) {
		int currentPos = getNextPos(startPos);
		
		// If it's the last player who has raise/bet (or a player after him, if this one is folded/disconnected)
		if (currentPos == -1 || currentPos >= (_lastRaiserPos + _players.size())) {		
			//And nobody raised
			if (currentPos == -1 || _maxStake == _players.get(currentPos).getStake()) {
				nextTurn();
				return;
			}
			
			currentPos = _lastRaiserPos;
		}
		
		onNextPos(_players.get(currentPos).getInfos(), toRealPos(_currentPos), toRealPos(currentPos));

		_currentPos = currentPos;
		
		updateGameInfos();
		
		final Player current = _players.get(currentPos);
		
		log("### POT : $" + _pot);
		log("### NEW POS : " + current);
		
		if (current.getStatus() == Status.Disconnected) {
			log("`-- DISCONNECTED => Fold");
			foldSafe();
			return;
		}
		
		_isWaitingForInput.set(true);
		
		_playingTimeElapsedFuture = schedule(() -> playingTimeElapsed(), _config.getPlayingTime(), TimeUnit.SECONDS);
		
		if (current instanceof HumanPlayer)
			((HumanPlayer) current).updateGameInfos(getGameInfos());
		else
			schedule(() -> nextPosTask((Bot) current), 1500, TimeUnit.MILLISECONDS);
	}
	
	private void nextPosTask(Bot current) {
		ActionInfos a = current.play(getGameInfos(), _winEvaluator, _logger);
		
		switch (a.getAction()) {
			case Fold:
				fold();
				break;
			case Check:
				check();
				break;
			case Call:
				call();
				break;
			case Raise:
				raise(a.getAmount());
				break;
			default:
				throw new AssertionError();
		}
	}
	
	private void playingTimeElapsed() {
		if (_isWaitingForInput.compareAndSet(true, false)) {
			Player current = getCurrentPlayer();
			int afkDur = current.incrementAfkDuration();
			
			if (afkDur > _config.getMaxAfkDuration())
				setPlayerDisconnected(current);
			else
				foldSafe();
			
			log("`-- AFK => Fold");
		}
	}

	private void nextTurn() {
		onTurnCompleted(_currentPos);
		
		for (SecondaryPot pot : _secondaryPots.values())
			pot.stopAdd();
		
		if (_phase == Phase.River) {
			endRound();
			return;
		}
		
		log("### NEXT TURN ###");
		
		_phase = _phase.next();

		clearStakes();

		List<Card> newCards = new ArrayList<>();
		int newCardsCount = _phase.getNewCardsCount();

		for (int i = 0; i < newCardsCount; ++i) {
			Card newCard = _deck.draw();
			
			newCards.add(newCard);
			_board.add(newCard);
		}
		
		updateGameInfos();
		
		onNewBoardCards(_board.size() - newCards.size(), Collections.unmodifiableList(newCards));
		
		log("`-- NEW BOARD : " + Arrays.toString(_board.toArray()));
		
		schedule(() -> nextTurnTask(), BOARD_CARDS_APPEARANCE_DELAY * newCardsCount, TimeUnit.MILLISECONDS);
	}
	
	private void clearStakes() {
		for (Player p : _players)
			p.clearStake();	
		
		_maxStake = 0;
		_maxRaise = 0;
	}
	
	private void nextTurnTask() {
		int canPlayCount = 0;
		
		for (Player p : _players) {
			if (p.canPlay()) {
				++canPlayCount;
				p.clearAction();
			}
		}
		
		if (canPlayCount <= 1) {
			nextTurn();
			return;
		}
		
		if (_players.size() == 2)
			_lastRaiserPos = _dealerPos + 1;
		else if (!_players.get(_dealerPos + 1).canPlay())
			_lastRaiserPos = getNextPos(_dealerPos + 2);
		else
			_lastRaiserPos = _dealerPos + 1;
		
		onTurnStarted(_lastRaiserPos);
		
		nextPos(_lastRaiserPos); 
	}
	
	private int getNextPos() {
		return getNextPos(_currentPos + 1);
	}
	
	private int getNextPos(int startPos) {
		for (int i = startPos, max = startPos + _players.size(); i < max; ++i) {
			if (_players.get(i).canPlay())
				return i;
		}
		
		return -1;
	}
	
	@Override
	protected void foldSafe() {
		_playingTimeElapsedFuture.cancel(false);
		
		getCurrentPlayer().fold();
		
		++_foldedPlayersCount;

		log("`-- ACTION : Fold");
		
		if (_foldedPlayersCount == _players.size() - 1) {
			onTurnCompleted(_currentPos);
			endRound();
		} else {
			if (_currentPos == _lastRaiserPos)
				_lastRaiserPos = getNextPos();
			
			nextPos();
		}
	}
	
	@Override
	protected void checkSafe() {
		_playingTimeElapsedFuture.cancel(false);
		
		getCurrentPlayer().check();

		log("`-- ACTION : Check");
		
		nextPos();
	}
	
	@Override
	protected void callSafe() {
		_playingTimeElapsedFuture.cancel(false);
		
		int callAmount = getGameInfos().getCurrentCallAmount();
		
		pay(getCurrentPlayer(), callAmount, Action.Call);
		
		log("`-- ACTION : Call ($" + callAmount + ")");
		
		nextPos();
	}
	
	@Override
	protected void raiseSafe(int amount) {
		_playingTimeElapsedFuture.cancel(false);
		
		Player current = getCurrentPlayer();
		
		_lastRaiserPos = _currentPos;
		_maxRaise = (current.getStake() + amount) - _maxStake; //newStake - _maxStake
		
		log("`-- ACTION : Bet ($" + amount + ")");
		
		if (amount == current.getCash()) {
			pay(current, amount, Action.AllIn);
			
			if (_currentPos == _lastRaiserPos)
				_lastRaiserPos = getNextPos();
		} else if (_maxStake == 0) {
			pay(current, amount, Action.Bet);
		} else
			pay(current, amount, Action.Raise);

		nextPos();
	}
	
	private void resetAfkDuration() {
		execute(() -> getCurrentPlayer().resetAfkDuration());
	}
	
	/**
	 * If the engine is waiting for an input, the _gameInfos field is up to date
	 * and will not be modified before an input of a player. 
	 * Therefore _gameInfos represents the current state of the engine.
	 */
	private void ensureWaitingState() {
		if (!_isWaitingForInput.compareAndSet(true, false))
			throw new IllegalStateException("This engine is not waiting for an input");
	}
	
	@Override
	public void fold() {
		ensureWaitingState();
		
		super.fold();
		
		resetAfkDuration();
	}
	
	@Override
	public void check() {
		ensureWaitingState();
		
		super.check();

		resetAfkDuration();
	}
	
	@Override
	public boolean call() {
		ensureWaitingState();
		
		resetAfkDuration();
		
		return super.call();
	}

	@Override
	public boolean raise(final int amount) {
		ensureWaitingState();
		
		resetAfkDuration();
		
		return super.raise(amount);
	}
	
	@Override
	public GameConfig getGameConfig() {
		return _config;
	}
	
	private void refreshSecondaryPots(int previousStake, int newStake) {
		for (SecondaryPot pot : _secondaryPots.values())
			pot.add(previousStake, newStake);
	}

	private void endRound() {
		log("### ROUND OVER ###");
		
		clearStakes();
		
		final List<Player> nonFoldedPlayers = new ArrayList<>();

		for (Player p : _players) {
			if (!p.isFolded())
				nonFoldedPlayers.add(p);
		}
		
		int nonFoldedPlayersCount = nonFoldedPlayers.size();
		boolean onlyOneNonFoldedPlayer = nonFoldedPlayersCount == 1;
		
		if (onlyOneNonFoldedPlayer)
			nonFoldedPlayers.get(0).win(_pot);
		else {
			for (Player p : nonFoldedPlayers)
				p.refreshHand(new ArrayList<>(_board));
			
			Player.PlayerHandComparator comparator = Player.getHandComparator();
			
			nonFoldedPlayers.sort(comparator);
			
			int winnerCount = 1;
			
			for (int i = 0; i < nonFoldedPlayersCount - 1; ++i) {
				if (!comparator.equal(nonFoldedPlayers.get(i), nonFoldedPlayers.get(i + 1)))
					break;
				
				++winnerCount;
			}
			
			int distributedCash = 0;
			
			if (winnerCount > 1) {
				Map<Player, Integer> maxWonCashByPlayer = new HashMap<>();
				
				for (int i = 0; i < winnerCount; ++i) {
					Player winner = nonFoldedPlayers.get(i);
					int maxWonCash = -1;
					
					if (_secondaryPots.containsKey(winner))
						maxWonCash = _secondaryPots.get(winner).getValue();
					
					maxWonCashByPlayer.put(winner, maxWonCash);
				}
				
				Map<Player, Integer> wonCashByPlayer = computeWonCashByPlayer(maxWonCashByPlayer);
				
				for (Map.Entry<Player, Integer> entry : wonCashByPlayer.entrySet()) {
					int wonCash = entry.getValue();
					distributedCash += wonCash;
					
					entry.getKey().win(wonCash);
					nonFoldedPlayers.remove(0);
				}
			} else {
				Player winner = nonFoldedPlayers.get(0);
				int wonCash = _secondaryPots.containsKey(winner) ? _secondaryPots.get(winner).getValue() : _pot;
				distributedCash += wonCash;
				
				winner.win(wonCash);
				nonFoldedPlayers.remove(0);
			}
			
			for (int i = 0; i < nonFoldedPlayers.size(); ++i) {
				Player loser = nonFoldedPlayers.get(i);
				
				if (distributedCash != _pot) {
					int wonCash = _pot - distributedCash;
					
					if (_secondaryPots.containsKey(loser)) {
						int maxWonCash = _secondaryPots.get(loser).getValue();
						
						if (maxWonCash < wonCash)
							wonCash = maxWonCash;
					}
					
					distributedCash += wonCash;
					loser.win(wonCash, false);
				}
				
				if (loser.getCash() <= 0)
					loser.bust();
				else
					nonFoldedPlayers.get(i).lose();
			}
		}

		_pot = 0;
		
		updateGameInfos();
		
		onRoundCompleted(onlyOneNonFoldedPlayer);
		
		schedule(() -> newRound(false), onlyOneNonFoldedPlayer ? 5000 : 15000, TimeUnit.MILLISECONDS);
	}
	
	private Map<Player, Integer> computeWonCashByPlayer(Map<Player, Integer> maxWonCashByPlayer) {
		int winnerCount = maxWonCashByPlayer.size();
		double wonCash = (double) _pot / winnerCount;
		boolean terminated = false;
		Map<Player, Integer> wonCashByPlayer = new HashMap<>();
		
		while (!terminated) {
			int i = 0;
			
			for (Map.Entry<Player, Integer> entry : maxWonCashByPlayer.entrySet()) {
				Player p = entry.getKey();
				int maxWonCash = entry.getValue();
				
				if (maxWonCash != -1 && wonCashByPlayer.get(p) == null && wonCash > maxWonCash) {
					wonCashByPlayer.put(p, maxWonCash);
					wonCash += (double) (wonCash - maxWonCash) / (double) (winnerCount - 1);
					break;
				}
				
				++i;
				
				if (i == winnerCount)
					terminated = true;
			}
		}
		
		int flooredWonCash = (int) Math.floor(wonCash);
		
		for (Player p : maxWonCashByPlayer.keySet()) {
			if (wonCashByPlayer.get(p) == null)
				wonCashByPlayer.put(p, flooredWonCash);
		}
		
		return wonCashByPlayer;
	}
	
	private void endGame(String winnerName) {
		endGame(Arrays.asList(winnerName));
	}
	
	private void endGame(List<String> winnersNames) {
		log("----- GAME OVER -----");
		
		shutdown();
		
		_isGameOver = true;
		
		_rankings.addAllPlayers(winnersNames);
		
		_rankings.finalizeRankings();
		
		updateGameInfos();
		
		onGameOver(_rankings.getRankings());
	}
	
	@Override
	public void shutdown() {
		if (_playingTimeElapsedFuture != null)
			_playingTimeElapsedFuture.cancel(false);
		
		_winEvaluator.shutdown();
		
		if (_logger != null)
			_logger.close();
		
		super.shutdown();
	}

	private Player getCurrentPlayer() {
		return _players.get(_currentPos);
	}
	
	private int toRealPos(int pos) {
		return _players.toRealIndex(pos);
	}
	
	/*private Player getPlayerByName(String name) {
		for (Player p : _players) {
			if (p.getName().equals(name))
				return p;
		}
		
		return null;
	}*/
	
	private PlayerInfos[] getPlayersInfos() {
		int playersCount = _players.size();
		PlayerInfos[] infos = new PlayerInfos[playersCount];
		
		for (int i = 0; i < playersCount; ++i)
			infos[i] = _players.get(i).updateInfos();
		
		return infos;
	}
	
	private void setPlayerDisconnected(Player p) {
		p.setDisconnected();
		
		if (p.equals(getCurrentPlayer()))
			foldSafe();
	}
	
	public void setPlayerDisconnected(String playerName) {
		execute(() -> {
			for (Player p : _players) {
				if (p.getName().equals(playerName)) {
					setPlayerDisconnected(p);
					break;
				}
			}
		});
	}
	
	private void updateGameInfos() {
		updateGameInfos(new GameInfos(
				getPlayersInfos(), _board, _phase, _currentPos, _dealerPos, _players.size(), _smallBlind, _antes, _maxRaise, _maxStake, _pot, _isGameOver));
	}
	
	private void log(String msg) {
		if (_logger != null)
			_logger.log(msg);
	}
	
	private static GameLogger createLogger() {
		GameLogger logger = null;
		
		try {
			logger = new GameLogger(BaseApp.LOGS_OUTPUT_PATH);
		} catch (SecurityException | IOException e) {
			BaseApp.logException(Level.WARNING, e);
		}
		
		return logger;
	}
	
	@ThreadSafe
	public static class Builder {
		@GuardedBy("this") private final Map<String, StatsInfos> _humanPlayers;
		
		private final GameConfig _config;
		
		public Builder(GameConfig config) {
			_config = config;
			
			synchronized (this) {
				_humanPlayers = new HashMap<>();
			}
		}
		
		public synchronized Builder addHumanPlayer(String name, StatsInfos stats) {
			if (_humanPlayers.size() == _config.getHumanPlayersCount())
				throw new IllegalStateException("All human players names have already been set");
			
			_humanPlayers.put(name, stats);
			
			return this;
		}
		
		public synchronized Builder removeHumanPlayer(String name) {
			_humanPlayers.remove(name);
			
			return this;
		}
		
		public synchronized ServerGameEngine build() {
			if (_humanPlayers.size() < _config.getHumanPlayersCount())
				throw new IllegalStateException("humanPlayersCount < config.getHumanPlayersCount() (Actually : humanPlayersCount == " + _humanPlayers.size() + ")");
			
			final ScheduledExecutorService engineExec = createEngineExecutor();
			
			return ConcurrentUtil.getSubmitResult(engineExec, () -> {
				return new ServerGameEngine(engineExec, _config, _humanPlayers);
			}, BaseApp.LOGGER);
		}
	}
}
