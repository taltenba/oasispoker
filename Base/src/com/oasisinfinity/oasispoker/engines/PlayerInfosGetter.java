package com.oasisinfinity.oasispoker.engines;

import java.util.Optional;

import com.oasisinfinity.oasispoker.engines.Player.Action;
import com.oasisinfinity.oasispoker.engines.Player.Role;
import com.oasisinfinity.oasispoker.engines.Player.Status;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.utils.ImmutablePair;

public interface PlayerInfosGetter extends NamedPlayer {
	public String getName();
	public Optional<StatsInfos> getStats();
	public int getCash();	
	public int getStake();
	public Action getAction();
	public Role getRole();
	public ImmutablePair<Card> getCards();
	public Hand getHand();
	public Status getStatus();
	public boolean isFolded();
	public boolean hasBet();
	public boolean canPlay();
}
