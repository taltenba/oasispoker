package com.oasisinfinity.oasispoker.engines;

import java.util.List;

import com.oasisinfinity.oasispoker.engines.ServerGameEngine.Phase;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.PlayerInfos;

public interface GameInfosGetter {
	public List<PlayerInfos> getPlayersInfos();
	public PlayerInfos getCurrentPlayerInfos();
	public PlayerInfos getPlayerInfosByName(String playerName);
	public List<PlayerInfos> getNonFoldedPlayers();
	public Phase getPhase();
	public List<Card> getBoard();
	public int getCurrentPos();
	public int getDealerPos();
	public int getPlayersCount();
	public int getSmallBlind();
	public int getBigBlind();
	public int getAntes();
	public int getMaxRaise();
	public int getMaxStake();
	public int getPot();
	public boolean isGameOver();
	public boolean canCurrentCheck();
	public boolean canCheck(PlayerInfosGetter playerInfos);
	public int getCurrentMinRaise();
	public int getMinRaise(PlayerInfosGetter playerInfos);
	public int getCurrentCallAmount();
	public int getCallAmount(PlayerInfosGetter playerInfos);
	public int getRealCurrentPos();
	public int toRealPos(int pos);
	public boolean isCurrentPlayer(NamedPlayer p);
	public boolean isCurrentPlayer(String playerName);
}
