package com.oasisinfinity.oasispoker.engines;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.PocketHands.PocketHand;
import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.ImmutablePair;
import com.oasisinfinity.utils.concurrent.ConcurrentUtil;
import com.oasisinfinity.utils.logging.LogPolicy;

import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class MonteCarloEvaluator {
	private static final int TOTAL_MONTE_CARLO_EVALS = 60000; //60,000 evaluations => precision = (+/-) 0.5 % on average
	
	private final ThreadPoolExecutor _exec;
	private final CompletionService<int[]> _evalsCompService;
	private final int _threadsCount;
	private final int _evalsPerThread;
	
	public MonteCarloEvaluator() {
		_threadsCount = Math.max(1, Runtime.getRuntime().availableProcessors() - 1);
		_evalsPerThread = Math.floorDiv(TOTAL_MONTE_CARLO_EVALS, _threadsCount);
		
		if (_threadsCount > 1) {
			_exec = ConcurrentUtil.newFixedCrashHandlerThreadPoolExecutor(_threadsCount - 1, new BasicThreadFactory("Bot - Monte Carlo Evaluation Thread"), BaseApp.FX_CRASH_HANDLER);
			_exec.setRejectedExecutionHandler(new LogPolicy(BaseApp.LOGGER));
			
			_evalsCompService = new ExecutorCompletionService<>(_exec);
		} else {
			_exec = null;
			_evalsCompService = null;
		}
	}
	
	private static List<Set<ImmutablePair<Card>>> makePocketCardsDists(List<List<PocketHand>> pocketHandsDists) {
		List<Set<ImmutablePair<Card>>> cardsDists = new ArrayList<>();
		
		for (List<PocketHand> handDist : pocketHandsDists) {
			if (handDist == null) {
				cardsDists.add(null);
				continue;
			}
			
			Set<ImmutablePair<Card>> cardsDist = new HashSet<>();
			
			for (PocketHand hand : handDist)
				cardsDist.addAll(hand.toPocketCards());
			
			cardsDists.add(cardsDist);
		}
		
		return cardsDists;
	}
	
	/**
	 * 
	 * @param boardCards
	 * @param playerCards
	 * @param playersCount
	 * @return An array containing an approximation of the probability of winning indexed by the number of players to beat
	 */
	public double[] evaluate(List<Card> boardCards, List<Card> playerCards, List<List<PocketHand>> pocketHandsDists, int playersCount) {
		if (_threadsCount > 1)
			return evaluateAsync(boardCards, playerCards, makePocketCardsDists(pocketHandsDists), playersCount);
		else
			return evaluateSync(boardCards, playerCards, makePocketCardsDists(pocketHandsDists), playersCount);
	}
	
	private double[] evaluateSync(List<Card> boardCards, List<Card> playerCards, List<Set<ImmutablePair<Card>>> pocketCardsDists, int playersCount) {
		return toProbabilities(
				new MonteCarloEvaluation(boardCards, playerCards, pocketCardsDists, playersCount, TOTAL_MONTE_CARLO_EVALS)
										.call(), 
				TOTAL_MONTE_CARLO_EVALS);
	}
	
	private double[] evaluateAsync(List<Card> boardCards, List<Card> playerCards, List<Set<ImmutablePair<Card>>> pocketCardsDists, int playersCount) {
		int newThreadsCount = _threadsCount - 1;
		
		for (int i = 0; i < newThreadsCount; ++i)
			_evalsCompService.submit(new MonteCarloEvaluation(boardCards, playerCards, pocketCardsDists, playersCount, _evalsPerThread));
		
		int remainingEvals = TOTAL_MONTE_CARLO_EVALS - _evalsPerThread * newThreadsCount;
		int[] winsCount = new MonteCarloEvaluation(boardCards, playerCards, pocketCardsDists, playersCount, remainingEvals).call();
		int success = newThreadsCount;
		
		for (int i = 0; i < newThreadsCount; ++i) {
			try {
				combineResults(winsCount, _evalsCompService.take().get());
			} catch (InterruptedException | ExecutionException e) {
				BaseApp.logException(Level.SEVERE, e);
				--success;
			}
		}
		
		return toProbabilities(winsCount, success * _evalsPerThread + remainingEvals);
	}
	
	public void shutdown() {
		if (_exec != null)
			_exec.shutdown();
	}
	
	private static double[] toProbabilities(int[] winsCount, int evals) {
		int playersCount = winsCount.length;
		double[] probs = new double[playersCount];
		
		for (int i = 0; i < playersCount; ++i)
			probs[i] = (double) winsCount[i] / evals;
		
		return probs;
	}
	
	private static void combineResults(int[] finalResults, int[] tmpResults) {
		for (int i = 0; i < finalResults.length; ++i)
			finalResults[i] += tmpResults[i];
	}
}
