package com.oasisinfinity.oasispoker.engines;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.oasisinfinity.oasispoker.models.PlayerRank;

import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
class PlayersRankings {
	private final List<PlayerRank> _rankings;
	
	private int _currentBustedRank;
	
	public PlayersRankings() {
		_rankings = new ArrayList<>();
	}

	public void nextRank() {
		++_currentBustedRank;
	}
	
	public void addPlayer(String playerName) {
		_rankings.add(new PlayerRank(playerName, _currentBustedRank));
	}
	
	public void addAllPlayers(List<String> playersNames) {
		for (String name : playersNames)
			addPlayer(name);
	}
	
	public void finalizeRankings() {
		int maxBustedRank = _currentBustedRank + 1;
		
		for (int i = 0, size = _rankings.size(); i < size; ++i) {
			PlayerRank bustedRank = _rankings.get(i);
			
			_rankings.set(i, new PlayerRank(bustedRank.getPlayerName(), maxBustedRank - bustedRank.getRank()));
		}
	}
	
	public List<PlayerRank> getRankings() {
		return Collections.unmodifiableList(_rankings);
	}
}
