package com.oasisinfinity.oasispoker.engines;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.utils.ImmutablePair;

import javafx.scene.paint.Color;

public abstract class Player implements NamedPlayer {
	private static final ImmutablePair<Card> NO_CARDS = new ImmutablePair<Card>(null, null);
	private static final PlayerHandComparator HAND_COMPARATOR = new PlayerHandComparator();

	public static class PlayerHandComparator implements Comparator<Player>, Serializable {
		private static final long serialVersionUID = 791874183697425454L;

		@Override
		public int compare(Player p1, Player p2) {
			return p1._hand.compareTo(p2._hand);
		}

		public boolean equal(Player p1, Player p2) {
			if (p1 == p2)
				return true;

			return p1._hand.equals(p2._hand);
		}
	}

	/*@Immutable
	public static class Builder {
		private final String _name;
		private final int _initialCash;
		private final boolean _isBot;

		private Builder(String name, int initialCash, boolean isBot) {
			_name = name;
			_initialCash = initialCash;
			_isBot = isBot;
		}

		public String getName() {
			return _name;
		}

		Player build(GameEngine engine) {
			if (_isBot)
				return new Bot(_name, _initialCash, engine);
			else
				return new Player(_name, _initialCash);
		}

		public static Builder newPlayerBuilder(String name, int initialCash) {
			return new Builder(name, initialCash, false);
		}

		public static Builder newBotBuilder(String name, int initialCash) {
			return new Builder(name, initialCash, true);
		}
	}*/

	public enum Action {
		Fold ("Fold", Color.RED),
		Check ("Check", Color.GREEN),
		Call ("Call", Color.ROYALBLUE),
		Bet ("Bet", Color.YELLOW),
		Raise ("Raise", Color.YELLOW),
		AllIn ("ALL-IN", Color.ORANGE);

		private final String _name;
		private final Color _color;

		private Action(String name, Color color) {
			_name = name;
			_color = color;
		}

		public Color getColor() {return _color;}

		@Override
		public String toString() {return _name;}
	}

	public enum Role {
		Dealer ("D", Color.WHITE),
		SmallBlind ("SB", Color.ROYALBLUE),
		BigBlind ("BB", Color.web("#FFB700"));

		private final String _abbreviation;
		private final Color _color;

		private Role(String abbreviation, Color color) {
			_abbreviation = abbreviation;
			_color = color;
		}

		public String getAbbreviation() {return _abbreviation;}
		public Color getColor () {return _color;}
	}

	public enum Status {
		Winner ("Gagnant", Color.GREEN),
		Loser ("Perdant", Color.ORANGE),
		Disconnected ("D�connect�", Color.RED),
		Busted ("�limin�", Color.RED);

		private final String _name;
		private final Color _color;

		private Status(String name, Color color) {
			_name = name;
			_color = color;
		}

		public Color getColor() {return _color;}

		@Override
		public String toString() {return _name;}
	}

	protected final String _name;
	
	private ImmutablePair<Card> _cards;
	private int _cash;
	private int _stake;
	private Action _action;
	private Role _role;
	private Hand _hand;
	private Status _status;
	private int _afkDuration;
	private boolean _hasBet;

	private PlayerInfos _infos;

	Player(String name, int cash) {
		_name = name;
		_cash = cash;
		_cards = NO_CARDS;
	}

	/*public Player(PlayerInfos infos) {
		this(infos.getName(), infos.getCash());

		_role = infos.getRole();
		_action = infos.getAction();
		_stake = infos.getStake();
		_cards = infos.getCards();
		_hand = infos.getHand();
		_status = infos.getStatus();
	}*/
	
	abstract StatsInfos getStatsInfos();
	
	void win(int amount) {
		win(amount, true);
	}

	void win(int amount, boolean isWinner) {
		_cash += amount;

		if (isWinner)
			setStatus(Status.Winner);
	}

	void lose() {
		setStatus(Status.Loser);
	}

	int pay(int amount, Action a) {
		if (a == Action.Bet || a == Action.Raise || a == Action.AllIn)
			_hasBet = true;
		
		if (amount >= _cash) {
			setAction(Action.AllIn);
			amount = _cash;
		} else
			setAction(a);
		
		_stake += amount;
		_cash -= amount;

		return amount;
	}

	void fold() {
		setAction(Action.Fold);
		clearCards();
	}

	void check() {
		setAction(Action.Check);
	}

	int call(int amount) {
		return pay(amount, Action.Call);
	}

	int bet(int amount) {
		return pay(amount, Action.Bet);
	}

	int raise(int amount) {
		return pay(amount, Action.Raise);
	}

	@Override
	public String getName() {
		return _name;
	}

	int getCash() {
		return _cash;
	}

	Action getAction() {
		return _action;
	}
	
	Role getRole() {
		return _role;
	}

	int getStake() {
		return _stake;
	}

	ImmutablePair<Card> getCards() {
		return _cards;
	}

	Hand getHand() {
		return _hand;
	}

	Status getStatus() {
		return _status;
	}
	
	boolean hasBet() {
		return _hasBet;
	}

	void setStatus(Status status) {
		if (_status != Status.Disconnected)
			_status = status;
	}
	
	void setAction(Action a) {
		_action = a;
	}

	void setDisconnected() {
		setStatus(Status.Disconnected);
	}

	void bust() {
		setStatus(Status.Busted);
	}

	void setRole(Role role) {
		_role = role;
	}

	void refreshHand(List<Card> board) {
		board.addAll(_cards.toList());
		
		_hand = Hand.newHand(board);
	}

	void clearAction() {
		_hasBet = false;
		setAction(null);
	}

	void clearHand() {
		_hand = null;
	}

	void clearStatus() {
		setStatus(null);
	}

	void clearStake() {
		_stake = 0;
	}

	void setCards(Card... cards) {
		if (cards.length != 2)
			throw new IllegalArgumentException("cards.length != 2");

		 setCards(cards[0], cards[1]);
	}

	void setCards(Card first, Card second) {
		_cards = new ImmutablePair<Card>(first, second);
	}

	void clearCards() {
		_cards = NO_CARDS;

		clearHand();
	}

	boolean isFolded() {
		return _action == Action.Fold;
	}

	boolean canPlay() {
		return _action != Action.Fold && _action != Action.AllIn;
	}

	int incrementAfkDuration() {
		return ++_afkDuration;
	}

	void resetAfkDuration() {
		_afkDuration = 0;
	}

	PlayerInfos updateInfos() {
		_infos = new PlayerInfos(_name, getStatsInfos(), _cash, _stake, _action, _role, _cards, _hand, _status, _hasBet); //cards are copied in PlayerInfos' constructor (no reference is kept)
		return _infos;
	}

	PlayerInfos getInfos() {
		return _infos;
	}

	@Override
	public int hashCode() {
		return 17 * 31 + _name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null || !(obj instanceof Player))
			return false;

		return _name.equals(((Player) obj)._name);
	}
	
	@Override
	public String toString() {
		return _name + " ($" + _cash + (_status != null ? " / " + _status : "") + ") : " + _cards;
	}

	static PlayerHandComparator getHandComparator() {
		return HAND_COMPARATOR;
	}
}
