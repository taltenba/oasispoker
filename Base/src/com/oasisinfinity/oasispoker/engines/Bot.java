package com.oasisinfinity.oasispoker.engines;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.engines.ServerGameEngine.Phase;
import com.oasisinfinity.oasispoker.models.ActionInfos;
import com.oasisinfinity.oasispoker.models.GameInfos;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.PocketHands;
import com.oasisinfinity.oasispoker.models.PocketHands.PocketHand;
import com.oasisinfinity.oasispoker.models.StatsInfos;
import com.oasisinfinity.utils.ArrayUtil;
import com.oasisinfinity.utils.MathUtil;
import com.oasisinfinity.utils.RandomUtil;
import com.oasisinfinity.utils.io.IOUtil;

import net.jcip.annotations.NotThreadSafe;

public class Bot extends Player {
	private static final StatsInfos STATS;
	private static final List<PocketHand> DEFAULT_POCKET_HANDS_DIST;
	private static final List<PocketHand> DEFAULT_BLINDS_POCKET_HANDS_DIST;
	
	static {
		List<PocketHand> defaultPocketHandsDist = null;
		List<PocketHand> defaultBlindsPocketHandsDist = null;
		StatsInfos botsStatsInfos = null;
		
		try {
			Gson gson = PocketHands.getGson();
			Type type = new TypeToken<ArrayList<PocketHand>>(){}.getType();
			
			defaultPocketHandsDist = IOUtil.loadJsonFromFile(Bot.class.getResourceAsStream("/json/default_pocket_hands_dist.json"), 
										  type,
									      gson);
			
			defaultBlindsPocketHandsDist = IOUtil.loadJsonFromFile(Bot.class.getResourceAsStream("/json/default_blinds_pocket_hands_dist.json"), 
					  type,
				      gson);
			
			botsStatsInfos = IOUtil.loadJsonFromFile(Bot.class.getResourceAsStream("/json/bots_stats_infos.json"), StatsInfos.class);
		} catch (IOException e) {
			BaseApp.corruptedInstallation(e);
		}
		
		DEFAULT_POCKET_HANDS_DIST = defaultPocketHandsDist;
		DEFAULT_BLINDS_POCKET_HANDS_DIST = defaultBlindsPocketHandsDist;
		STATS = botsStatsInfos;
	}
	
	private double[] _turnWinProbs;
	
	Bot(String name, int cash) {
		super(name, cash);
	}

	/**
	 * Must only be called when {@code this} is the current player
	 * @param gameInfos
	 * @param evaluator
	 * @return
	 */
	ActionInfos play(GameInfos gameInfos, MonteCarloEvaluator evaluator, GameLogger engineLogger) {
		List<PlayerInfos> nonFoldedOpponents = new ArrayList<>();
		List<List<PocketHand>> opponentsPocketHandsDists = new ArrayList<>();
		int betsCount = 0;
		
		for (PlayerInfos p : gameInfos.getPlayersInfos()) {
			if (!p.isFolded() && !p.getName().equals(_name)) {
				nonFoldedOpponents.add(p);
				
				if (gameInfos.getPhase() == Phase.PreFlop && p.getAction() == null) //if the player hasn't already played this turn
					opponentsPocketHandsDists.add(null); //null <=> full distribution
				else {
					Role role = p.getRole();
					
					if (role == Role.SmallBlind || role == Role.BigBlind)
						opponentsPocketHandsDists.add(DEFAULT_BLINDS_POCKET_HANDS_DIST);
					else
						opponentsPocketHandsDists.add(DEFAULT_POCKET_HANDS_DIST);	
				}
				
				if (p.hasBet())
					++betsCount;
			}
		}
		
		int nonFoldedOpponentsCount = nonFoldedOpponents.size();
		Phase phase = gameInfos.getPhase();
		
		/*if (phase == Phase.PreFlop && nonFoldedOpponentsCount == 1 
				&& Math.min(getCash(), nonFoldedOpponents.get(0).getCash()) <= gameInfos.getBigBlind() * 10)
			return getJamOrFoldAction(getCash(), gameInfos.canCurrentCheck());*/
		
		double[] pWin = evaluator.evaluate(gameInfos.getBoard(), getCards().toList(), opponentsPocketHandsDists, nonFoldedOpponentsCount + 1);
		
		if (phase != Phase.PreFlop) {
			if (_turnWinProbs != null)
				pWin = ArrayUtil.average(_turnWinProbs, pWin);
			
			_turnWinProbs = pWin;
		}
		
		Map<ActionInfos, Double> expectedValues = new HashMap<>();
		
		int pot = gameInfos.getPot();
		
		ExpectedValueCalculator evCalculator = 
				new ExpectedValueCalculator(nonFoldedOpponents, pWin, phase, pot, getCash(), betsCount);
		
		if (gameInfos.canCurrentCheck())
			expectedValues.put(ActionInfos.newCheckActionInfos(_name), pWin[nonFoldedOpponentsCount] * pot);
		else {
			expectedValues.put(ActionInfos.newCallActionInfos(_name), evCalculator.calculate(getStake(), gameInfos.getMaxStake()));
			expectedValues.put(ActionInfos.newFoldActionInfos(_name), 0.0);
		}
		
		if (!hasBet() && gameInfos.getCurrentCallAmount() < getCash()) {
			int raise;
			
			if (phase == Phase.PreFlop && gameInfos.getMaxRaise() == gameInfos.getBigBlind() + gameInfos.getAntes()) { //if it's preflop and no one has raised
				raise = ((getRole() != null ? 4 : 3) + getLimpersCount(nonFoldedOpponents)) * gameInfos.getBigBlind()
					  + (int) (0.4f * gameInfos.getAntes() * gameInfos.getPlayersCount());
			} else
				raise = (int) (pot * 0.6f);
			
			raise = Math.min(getCash(),
							 Math.max(MathUtil.floor(raise, 5),
									  gameInfos.getCurrentMinRaise()));
			
			expectedValues.put(ActionInfos.newRaiseActionInfos(_name, raise), evCalculator.calculate(getStake(), raise));
		}
		
		ActionInfos bestAction = null;
		ActionInfos alternativeAction = null;
		double bestEV = Float.NEGATIVE_INFINITY;
		double alternativeEV = Float.NEGATIVE_INFINITY;
		
		for (Map.Entry<ActionInfos, Double> entry : expectedValues.entrySet()) {
			double ev = entry.getValue();
			
			if (ev > bestEV) {
				alternativeEV = bestEV;
				alternativeAction = bestAction;
				bestEV = ev;
				bestAction = entry.getKey();
			} else if (ev > alternativeEV) {
				alternativeEV = ev;
				alternativeAction = entry.getKey();
			}
		}
		
		if (engineLogger != null) {
			engineLogger.log("`-- pWin : " + Arrays.toString(pWin));
			engineLogger.log("`-- Best action : [" + bestAction + "] : " + bestEV);
			engineLogger.log("`-- Alt. action : [" + alternativeAction + "] : " + alternativeEV);
		}
		
		/*System.out.println("--------------------------");
		System.out.println("Board : " + Arrays.toString(gameInfos.getBoard().toArray()));
		System.out.println("Pot : $" + pot);
		System.out.println(this);
		System.out.println(Arrays.toString(pWin));
		System.out.println("[" + bestAction +  "] : "  + bestEV);
		System.out.println("[" + alternativeAction + "] : " + alternativeEV);
		System.out.println("--------------------------");*/
		
		if (alternativeAction != null && bestEV != 0.0 && alternativeEV >= 0.0) {
			double alternativeDelta = 100.0 * (bestEV - alternativeEV) / bestEV;
			
			if (RandomUtil.generateRandom(100) < 50.0 - 1.5 * alternativeDelta) //Reduce the predictability of the bots
				bestAction = alternativeAction;
		}
		
		return bestAction;
		
		//return ActionInfos.newCallActionInfos(getName());
		
		/*int rnd = RandomUtil.generateRandom(99);
		
		if (rnd < 20)
			return ActionInfos.newFoldActionInfos(getName());
			
		if (rnd < 70 || gameInfos.getCallAmount(getInfos()) >= getCash())
			return ActionInfos.newCallActionInfos(getName());
		
		if (rnd < 80)
			return ActionInfos.newRaiseActionInfos(getName(), getCash());
		
		return ActionInfos.newRaiseActionInfos(getName(), gameInfos.getMinRaise(getInfos()));*/
	}

	@Override
	StatsInfos getStatsInfos() {
		return null;
	}
	
	@Override
	void clearAction() {
		super.clearAction();
		_turnWinProbs = null;
	}
	
	private static int getLimpersCount(List<PlayerInfos> players) {
		int count = 0;
		
		for (PlayerInfos p : players) {
			if (p.getAction() == Action.Call)
				++count;
		}
		
		return count;
	}
	
	/*private static ActionInfos getJamOrFoldAction(int playerCash, boolean canPlayerCheck) {
	
	}*/
	
	@NotThreadSafe
	private static class ExpectedValueCalculator {
		private final List<PlayerInfos> _players;
		private final double[] _pWin;
		private final double[] _playersCallProbs;
		private final Phase _phase;
		private final int _pot;
		private final int _playerStack;
		private final int _betsCount;
		
		private int _oldStake;
		private int _newStake;
		
		public ExpectedValueCalculator(List<PlayerInfos> players, double[] pWin, Phase phase, int pot, int playerStack, int betsCount) {
			_pWin = pWin;
			_players = players;
			_phase = phase;
			_playerStack = playerStack;
			_betsCount = betsCount;
			
			for (PlayerInfos p : players)
				pot -= p.getStake();
			
			_pot = pot;
			
			int playersCount = players.size();
			_playersCallProbs = new double[playersCount];
			
			for (int i = 0; i < playersCount; ++i)
				_playersCallProbs[i] = getCallProbability(players.get(i));
		}
		
		private double getMaxExpectedValue(int playerIndex, double pCall, int callersCount, int totalWinCash) {
			if (playerIndex == _players.size()) {
				int stakeDiff = Math.min(totalWinCash - _pot, _newStake) - _oldStake;
				return pCall * (_pWin[callersCount] * (totalWinCash + stakeDiff) - stakeDiff * getRisk(stakeDiff));
			}
			
			PlayerInfos player = _players.get(playerIndex);
			double playerCallProb = _playersCallProbs[playerIndex];
			int playerOldStake = player.getStake();
			int playerNewStake = playerOldStake + Math.min(Math.abs(_newStake - playerOldStake), player.getCash());
			
			if (playerOldStake == playerNewStake) //Can check, therefore never fold
				playerCallProb = 1.0;
			
			/*if (player.hasBet())
				playerCallProb = Math.min(1.0, playerCallProb * 1.8);*/
			
			return getMaxExpectedValue(playerIndex + 1, pCall * playerCallProb, callersCount + 1, totalWinCash + playerNewStake)
		         + getMaxExpectedValue(playerIndex + 1, pCall * (1.0 - playerCallProb), callersCount, totalWinCash + playerOldStake);
		}
		
		private double getCallProbability(PlayerInfos player) {
			if (player.hasBet())
				return 0.9;
			
			StatsInfos stats = player.getStats().orElse(STATS);
			int playedHands = stats.getTotalPlayedHands();
			
			if (playedHands < 50) {
				switch (_phase) {
					case PreFlop:
						return 0.6;
					case Flop:
						return 0.8;
					case Turn:
						return 0.9;
					case River:
						return 0.95;
				}
			}
			
			return 1.0 - ((double) stats.getFoldedHands(_phase) / playedHands);
		}
		
		private double getRisk(int stakeDiff) {
			return (0.2 * (_phase.ordinal() + 1) * _players.size() * (_betsCount + 1) * Math.pow((double) stakeDiff / _playerStack, 0.6)) + 1.0;
		}
		
		public double calculate(int oldStake, int newStake) {
			_oldStake = oldStake;
			_newStake = newStake;
			
			return getMaxExpectedValue(0, 1.0, 0, _pot);
		}
	}
}
