package com.oasisinfinity.oasispoker.engines;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.utils.PokerDeck;
import com.oasisinfinity.utils.Deck;
import com.oasisinfinity.utils.ImmutablePair;

public class MonteCarloEvaluation implements Callable<int[]> {
	private final List<Card> _boardCards;
	private final List<Card> _playerCards;
	private final List<Set<ImmutablePair<Card>>> _opponentsDistributions;
	private final int _playersCount;
	private final int _evalsCount;
	
	private Deck<Card> _mainDeck;
	private Set<Card> _deadCards;
	private List<Card> _board;
	
	public MonteCarloEvaluation(List<Card> boardCards, List<Card> playerCards, List<Set<ImmutablePair<Card>>> opponentsDistributions, int playersCount, int evalsCount) {
		_boardCards = boardCards;
		_playerCards = playerCards;
		_opponentsDistributions = opponentsDistributions;
		_playersCount = playersCount;
		_evalsCount = evalsCount;
	}
	
	/**
	 * WARNING : Never stop if an impossible state (like : distributions = {AA, AA, AA}) is given
	 */
	@Override
	public int[] call() {
		int[] winsCount = new int[_playersCount];
		int knownBoardCards = _boardCards.size();
		
		Set<Card> knownCards = new HashSet<>();
		knownCards.addAll(_boardCards);
		knownCards.addAll(_playerCards);
		
		_mainDeck = PokerDeck.newDeck(knownCards);
		_board = prepareBoard();
		
		List<Deck<ImmutablePair<Card>>> opponentsPocketDecks = preparePocketDecks(knownCards);
		List<ImmutablePair<Card>> opponentsPocketCards = prepareOpponentsPocketCards();
		
		winsCount[0] = _evalsCount;
		
		mainLoop:
		for (int i = 0; i < _evalsCount; ++i) {
			_deadCards = new HashSet<>();
			_mainDeck.refill();
			
			for (int j = 0; j < _playersCount - 1; ++j) { //Need to be before to set board to avoid problems with probabilities
				Deck<ImmutablePair<Card>> deck = opponentsPocketDecks.get(j);
				
				if (deck != null)
					deck.refill();
				
				ImmutablePair<Card> pocketCards = drawPocketCards(deck);
				
				if (pocketCards == null) { //Collision: impossible state reached
					--i;
					continue mainLoop;
				}
				
				opponentsPocketCards.set(j, pocketCards);
			}
			
			for (int j = knownBoardCards; j < 5; ++j)
				_board.set(j, drawCard());
			
			Hand playerHand = Hand.newHand(makeHandCards(_playerCards));
			
			for (int j = 1; j < _playersCount; ++j) {
				if (Hand.newHand(makeHandCards(opponentsPocketCards.get(j - 1))).isBetterThan(playerHand))
					continue mainLoop;
				
				++winsCount[j];
			}
		}
		
		return winsCount;
	}
	
	private List<Card> prepareBoard() {
		List<Card> board = new ArrayList<>(5);
		board.addAll(_boardCards);
		
		for (int i = board.size(); i < 5; ++i)
			board.add(null);
		
		return board;
	}
	
	private List<Deck<ImmutablePair<Card>>> preparePocketDecks(Set<Card> knownCards) {
		List<Deck<ImmutablePair<Card>>> decks = new ArrayList<>(_opponentsDistributions.size());
		
		for (Set<ImmutablePair<Card>> dist : _opponentsDistributions) {
			if (dist != null && !dist.isEmpty())
				decks.add(newPocketCardsDeck(dist, knownCards));
		}
		
		for (int i = 1; i < _playersCount; ++i) //Add empty distributions at the end in order to avoid problems with
			decks.add(null);                    //probabilities and to reduce collisions count
		
		return decks;
	}
	
	private List<ImmutablePair<Card>> prepareOpponentsPocketCards() {
		List<ImmutablePair<Card>> opponentsPocketCards = new ArrayList<>(_playersCount - 1);
		
		for (int i = 1; i < _playersCount; ++i)
			opponentsPocketCards.add(null);
		
		return opponentsPocketCards;
	}
	
	private List<Card> makeHandCards(List<Card> playerCards) {
		List<Card> handCards = new ArrayList<>(7);
		handCards.addAll(_board);
		handCards.addAll(playerCards);
		
		return handCards;
	}
	
	private List<Card> makeHandCards(/*PocketCardsDeck deck,*/ ImmutablePair<Card> pocketCards) {
		List<Card> handCards = new ArrayList<>(7);
		handCards.addAll(_board);
		handCards.add(pocketCards.getFirst());
		handCards.add(pocketCards.getSecond());
		
		return handCards;
	}
	
	private Card drawCard() {
		Card c;
		
		do {
			c = _mainDeck.draw();
		} while(_deadCards.contains(c));
		
		return c;
	}
	
	private ImmutablePair<Card> drawPocketCards(Deck<ImmutablePair<Card>> deck) {
		ImmutablePair<Card> pocketCards;
		
		if (deck == null)
			return new ImmutablePair<>(drawCard(), drawCard());
		else {
			do {
				if (deck.isEmpty()) //Collision
					return null;
				
				pocketCards = deck.draw();
			} while (!killAll(pocketCards));
		}

		return pocketCards;
	}
	
	private boolean killAll(ImmutablePair<Card> pocketCards) {
		if (_deadCards.contains(pocketCards.getFirst()) || _deadCards.contains(pocketCards.getSecond()))
			return false;
		
		_deadCards.add(pocketCards.getFirst());
		_deadCards.add(pocketCards.getSecond());
		
		return true;
	}
	
	private static Deck<ImmutablePair<Card>> newPocketCardsDeck(Collection<ImmutablePair<Card>> elements, Set<Card> excludedCards) {
		List<ImmutablePair<Card>> deck = new ArrayList<>(elements.size());
		
		for (ImmutablePair<Card> e : elements) {
			if (!excludedCards.contains(e.getFirst()) && !excludedCards.contains(e.getSecond()))
				deck.add(e);
		}
		
		return new Deck<>(deck);
	}
}
