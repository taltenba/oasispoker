package com.oasisinfinity.oasispoker.engines;

import java.util.List;
import java.util.Map;

import com.oasisinfinity.oasispoker.engines.ServerGameEngine.Phase;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.GameInfos;
import com.oasisinfinity.oasispoker.models.Hand;
import com.oasisinfinity.oasispoker.models.StatsInfos;

public class HumanPlayer extends Player {
	private final Map<Phase, Integer> _foldedHands;
	
	private int _wonCash;
	private int _lostCash;
	private int _biggestWonPot;
	private int _wonHands;
	private int _lostHands;
	private int _totalPlayedActions;
	private int _raisesCount;
	private Hand _bestHand;
	
	private StatsInfos _stats;
	private GameInfos _currentGameInfos;
	
	HumanPlayer(String name, StatsInfos stats, int cash) {
		super(name, cash);
		
		_stats = stats;
		_foldedHands = stats.getFoldedHandsMap();
		_wonCash = stats.getWonCash();
		_lostCash = stats.getLostCash();
		_biggestWonPot = stats.getBiggestWonPot();
		_wonHands = stats.getWonHands();
		_lostHands = stats.getLostHands();
		_totalPlayedActions = stats.getTotalPlayedActions();
		_raisesCount = stats.getRaisesCount();
		_bestHand = stats.getBestHand();
	}
	
	private void updateStatsInfos() {
		_stats = new StatsInfos(_wonCash, _lostCash, _biggestWonPot, _wonHands, _lostHands, _totalPlayedActions, _raisesCount, _foldedHands, _bestHand);
	}
	
	private void roundOver(boolean isWinner) {
		if (isWinner)
			++_wonHands;
		else
			++_lostHands;
		
		updateStatsInfos();
	}

	@Override
	void setAction(Action a) {
		super.setAction(a);
		
		if (a == null)
			return;
		
		++_totalPlayedActions;
		
		if (a == Action.Fold) {
			Phase currentPhase = _currentGameInfos.getPhase();
			_foldedHands.put(currentPhase, _foldedHands.get(currentPhase) + 1);
			
			roundOver(false);
		}
	}
	
	@Override
	int pay(int amount, Action a) {
		if (a == Action.Bet || a == Action.Raise || a == Action.AllIn)
			++_raisesCount;
		
		amount = super.pay(amount, a);
		
		_lostCash += amount;
		
		return amount;
	}
	
	@Override
	void win(int amount, boolean isWinner) {
		super.win(amount, isWinner);
		
		_wonCash += amount;
		
		if (amount > _biggestWonPot)
			_biggestWonPot = amount;
		
		roundOver(isWinner);
	}
	
	@Override
	void lose() {
		super.lose();
		
		roundOver(false);
	}
	
	@Override
	void refreshHand(List<Card> board) {
		super.refreshHand(board);
		
		Hand newHand = getHand();
		
		if (_bestHand == null || newHand.isBetterThan(_bestHand))
			_bestHand = newHand;
	}
	
	void updateGameInfos(GameInfos infos) {
		_currentGameInfos = infos;
	}
	
	@Override
	StatsInfos getStatsInfos() {
		return _stats;
	}
}
