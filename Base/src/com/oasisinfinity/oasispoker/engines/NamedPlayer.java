package com.oasisinfinity.oasispoker.engines;

public interface NamedPlayer {
	public String getName();
}
