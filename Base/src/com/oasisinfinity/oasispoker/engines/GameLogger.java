package com.oasisinfinity.oasispoker.engines;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ThreadPoolExecutor;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.concurrent.ConcurrentUtil;
import com.oasisinfinity.utils.logging.LogPolicy;

public class GameLogger {
	private final ThreadPoolExecutor _exec;
	private final PrintWriter _writer;
	
	public GameLogger(String logDirPath) throws IOException {
		_exec = ConcurrentUtil.newFixedCrashHandlerThreadPoolExecutor(1, new BasicThreadFactory("GameLoggerThread"), BaseApp.FX_CRASH_HANDLER);
		_exec.setRejectedExecutionHandler(new LogPolicy(BaseApp.LOGGER));
		
		File logFile = new File(logDirPath + "/LastGame.log");
		
		if (logFile.exists())
			logFile.delete();
		
		_writer = new PrintWriter(new BufferedWriter(new FileWriter(logFile)));
	}
	
	public void log(String msg) {
		_exec.execute(() -> _writer.println(msg));
	}
	
	public void close() {
		_writer.close();
		_exec.shutdown();
	}
}
