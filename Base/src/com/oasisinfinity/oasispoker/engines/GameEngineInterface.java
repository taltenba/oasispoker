package com.oasisinfinity.oasispoker.engines;

import java.util.List;
import java.util.concurrent.Executor;

import com.oasisinfinity.oasispoker.engines.ServerGameEngine.Phase;
import com.oasisinfinity.oasispoker.listeners.GameProgressListener;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.GameInfos;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.PlayerRank;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import net.jcip.annotations.NotThreadSafe;

/**
 * All public methods must be called in the interface executor's thread
 * @author OasisInfinity
 */
@NotThreadSafe
public class GameEngineInterface implements GameInfosGetter {
	private final Executor _interfaceExec;
	private final GameProgressListener _gameProgressListener;
	
	/* The values of properties must be confined in the _interfaceExec thread */
	private final ReadOnlyObjectWrapper<GameInfos> _gameInfos;
	private final ReadOnlyIntegerWrapper _currentPos;
	private final ReadOnlyIntegerWrapper _smallBlind;
	private final ReadOnlyIntegerWrapper _maxRaise;
	private final ReadOnlyIntegerWrapper _maxStake;
	private final ReadOnlyIntegerWrapper _pot;
	
	GameEngineInterface(Executor interfaceExec, GameProgressListener gameProgressListener) {
		_interfaceExec = interfaceExec;
		_gameProgressListener = gameProgressListener;
		
		_gameInfos = new ReadOnlyObjectWrapper<>();
		_currentPos = new ReadOnlyIntegerWrapper();
		_smallBlind = new ReadOnlyIntegerWrapper();
		_maxRaise = new ReadOnlyIntegerWrapper();
		_maxStake = new ReadOnlyIntegerWrapper();
		_pot = new ReadOnlyIntegerWrapper();
	}
	
	public GameInfos getGameInfos() {
		return _gameInfos.get();
	}
	
	public ReadOnlyObjectProperty<GameInfos> getGameInfosProperty() {
		return _gameInfos.getReadOnlyProperty();
	}
	
	public ReadOnlyIntegerProperty getCurrentPosProperty() {
		return _currentPos.getReadOnlyProperty();
	}
	
	public ReadOnlyIntegerProperty getSmallBlindProperty() {
		return _smallBlind.getReadOnlyProperty();
	}
	
	public ReadOnlyIntegerProperty getMaxRaiseProperty() {
		return _maxRaise.getReadOnlyProperty();
	}
	
	public ReadOnlyIntegerProperty getMaxStakeProperty() {
		return _maxStake.getReadOnlyProperty();
	}
	
	public ReadOnlyIntegerProperty getPotProperty() {
		return _pot.getReadOnlyProperty();
	}
	
	@Override
	public List<PlayerInfos> getPlayersInfos() {
		return getGameInfos().getPlayersInfos();
	}
	
	@Override
	public PlayerInfos getCurrentPlayerInfos() {
		return getGameInfos().getCurrentPlayerInfos();
	}
	
	@Override
	public PlayerInfos getPlayerInfosByName(String playerName) {
		return getGameInfos().getPlayerInfosByName(playerName);
	}
	
	@Override
	public List<PlayerInfos> getNonFoldedPlayers() {
		return getGameInfos().getNonFoldedPlayers();
	}

	@Override
	public List<Card> getBoard() {
		return getGameInfos().getBoard();
	}

	@Override
	public Phase getPhase() {
		return getGameInfos().getPhase();
	}
	
	@Override
	public int getCurrentPos() {
		return getGameInfos().getCurrentPos();
	}
	
	@Override
	public int getDealerPos() {
		return getGameInfos().getDealerPos();
	}

	@Override
	public int getPlayersCount() {
		return getGameInfos().getPlayersCount();
	}

	@Override
	public int getSmallBlind() {
		return getGameInfos().getSmallBlind();
	}

	@Override
	public int getBigBlind() {
		return getGameInfos().getBigBlind();
	}
	
	@Override
	public int getAntes() {
		return getGameInfos().getAntes();
	}
	
	@Override
	public int getMaxRaise() {
		return getGameInfos().getMaxRaise();
	}

	@Override
	public int getMaxStake() {
		return getGameInfos().getMaxStake();
	}

	@Override
	public int getPot() {
		return getGameInfos().getPot();
	}

	@Override
	public boolean isGameOver() {
		return getGameInfos().isGameOver();
	}

	@Override
	public boolean canCurrentCheck() {
		return getGameInfos().canCurrentCheck();
	}

	@Override
	public boolean canCheck(PlayerInfosGetter playerInfos) {
		return getGameInfos().canCheck(playerInfos);
	}

	@Override
	public int getCurrentMinRaise() {
		return getGameInfos().getCurrentMinRaise();
	}

	@Override
	public int getMinRaise(PlayerInfosGetter playerInfos) {
		return getGameInfos().getMinRaise(playerInfos);
	}

	@Override
	public int getCurrentCallAmount() {
		return getGameInfos().getCurrentCallAmount();
	}

	@Override
	public int getCallAmount(PlayerInfosGetter playerInfos) {
		return getGameInfos().getCallAmount(playerInfos);
	}

	@Override
	public int getRealCurrentPos() {
		return getGameInfos().getRealCurrentPos();
	}
	
	@Override
	public int toRealPos(int pos) {
		return getGameInfos().toRealPos(pos);
	}

	@Override
	public boolean isCurrentPlayer(NamedPlayer p) {
		return getGameInfos().isCurrentPlayer(p);
	}
	
	@Override
	public boolean isCurrentPlayer(String playerName) {
		return getGameInfos().isCurrentPlayer(playerName);
	}
	
	protected void execute(Runnable r) {
		_interfaceExec.execute(r);
	}
	
	void updateGameInfos(final GameInfos gameInfos) {
		if (gameInfos == null)
			return;
		
		_interfaceExec.execute(() -> {
			_gameInfos.set(gameInfos);
			_currentPos.set(gameInfos.getCurrentPos());
			_smallBlind.set(gameInfos.getSmallBlind());
			_maxStake.set(gameInfos.getMaxStake());
			_pot.set(gameInfos.getPot());
		});
	}
	
	void onPlayerBusted(final int pos, final String name) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.playerBusted(pos, name));
	}
	
	void onNextPos(final PlayerInfos p, final int oldPos, final int newPos) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.nextPos(p, oldPos, newPos));
	}

	void onRoundCompleted(final boolean onlyOneNonFoldedPlayer) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.roundCompleted(onlyOneNonFoldedPlayer));
	}

	void onRoundStarted(final boolean isFirstRound) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.roundStarted(isFirstRound));
	}
	
	void onTurnStarted(final int firstPlayerPos) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.turnStarted(firstPlayerPos));
	}
	
	void onTurnCompleted(final int lastPlayerPos) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.turnCompleted(lastPlayerPos));
	}
	
	void onGameStarted() {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.gameStarted());
	}
	
	void onGameOver(final List<PlayerRank> rankings) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.gameOver(rankings));
	}
	
	void onNewBoardCards(final int addIndex, final List<Card> newCards) {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.newBoardCards(addIndex, newCards));
	}
	
	void onBoardCleared() {
		if (_gameProgressListener != null)
			execute(() -> _gameProgressListener.boardCleared());
	}
}
