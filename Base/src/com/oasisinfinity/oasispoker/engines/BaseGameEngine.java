package com.oasisinfinity.oasispoker.engines;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.listeners.GameProgressListener;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.GameInfos;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.PlayerRank;
import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.concurrent.ConcurrentUtil;
import com.oasisinfinity.utils.logging.LogPolicy;

/**
 * @author OasisInfinity
 * @date 2016-06-30
 */
public abstract class BaseGameEngine {
	private final ScheduledExecutorService _engineExec;
	private final List<GameEngineInterface> _engineInterfaces;
	private final Map<String, List<PlayerInterface>> _playersInterfaces;
	
	private volatile GameInfos _gameInfos;
	
	protected BaseGameEngine(ScheduledExecutorService engineExec) {
		_engineExec = engineExec;
		_engineInterfaces = new CopyOnWriteArrayList<>();
		_playersInterfaces = new HashMap<>();
	}
	
	protected ScheduledExecutorService getExecutor() {
		return _engineExec;
	}
	
	protected void execute(Runnable r) {
		_engineExec.execute(r);
	}
	
	protected <T> Future<T> submit(Callable<T> c) {
		return _engineExec.submit(c);
	}
	
	protected Future<?> submit(Runnable r) {
		return _engineExec.submit(r);
	}
	
	protected ScheduledFuture<?> schedule(Runnable r, long delay, TimeUnit unit) {
		return _engineExec.schedule(r, delay, unit);
	}
	
	protected boolean isShutdown() {
		return _engineExec.isShutdown();
	}
	
	protected GameInfos getGameInfos() {
		return _gameInfos;
	}
	
	protected List<GameEngineInterface> getEngineInterfaces() {
		return Collections.unmodifiableList(_engineInterfaces);
	}

	private void initPlayersInterfacesMap(List<PlayerInfos> players) {
		for (PlayerInfos p : players)
			_playersInterfaces.put(p.getName(), new CopyOnWriteArrayList<>());
	}
	
	/*protected void initPlayersInterfacesMap(List<Player> players) {
		for (Player p : players)
			_playersInterfaces.put(p.getName(), new CopyOnWriteArrayList<>());
	}*/
	
	/*protected void awaitIdle() {
		try {
			_engineExec.submit(() -> {return null;}).get();
		} catch (RejectedExecutionException | InterruptedException | ExecutionException e) {
			BaseApp.logException(Level.INFO, e);
			
			if (_engineExec.isShutdown())
				throw new IllegalStateException("This engine has been shut down");
		}
	}*/
	
	protected void ensureCanCheck() {
		if (!getGameInfos().canCurrentCheck())
			throw new IllegalStateException("Current player cannot check");
	}
	
	protected boolean validateCall() {
		if (getGameInfos().canCurrentCheck()) {
			execute(() -> checkSafe());
			return false;
		}
		
		return true;
	}
	
	protected boolean validateRaise(int amount) {
		GameInfos infos = getGameInfos();
		
		if (amount == infos.getCurrentCallAmount()) {
			execute(() -> callSafe());
			return false;
		} else if (amount < infos.getCurrentMinRaise())
			throw new IllegalArgumentException("Amount must be higher or equals than min raise amount");
		
		/*if (amount < infos.getCurrentMinRaise()) {
			int callAmount = infos.getCurrentCallAmount();
			
			if (amount == callAmount || amount == infos.getCurrentPlayerInfos().getCash()) {
				execute(() -> callSafe());
				return false;
			}
			
			throw new IllegalArgumentException("Amount must be higher or equals than min raise amount");
		}*/
		
		return true;
	}
	
	protected abstract void foldSafe();
	
	protected abstract void checkSafe();
	
	protected abstract void callSafe();
	
	protected abstract void raiseSafe(int amount);
	
	public abstract GameConfig getGameConfig();
	
	public void fold() {
		execute(() -> foldSafe());
	}
	
	public void check() {
		ensureCanCheck();
		
		execute(() -> checkSafe());
	}
	
	public boolean call() {
		if (!validateCall())
			return false;
		
		execute(() -> callSafe());
		
		return true;
	}
	
	public boolean raise(final int amount) {
		if (!validateRaise(amount))
			return false;
		
		execute(() -> raiseSafe(amount));
		
		return true;
	}
	
	public void shutdown() {
		_engineExec.shutdown();
	}
	
	public GameEngineInterface addEngineInterface(Executor interfaceExec, GameProgressListener gameProgressListener) {
		GameEngineInterface newInterface = new GameEngineInterface(interfaceExec, gameProgressListener);
		
		addEngineInterface(newInterface);
		
		return newInterface;
	}
	
	protected void addEngineInterface(GameEngineInterface engineInterface) {
		_engineInterfaces.add(engineInterface);
		
		engineInterface.updateGameInfos(_gameInfos);
	}
	
	public void removeEngineInterface(GameEngineInterface engineInterface) {
		_engineInterfaces.remove(engineInterface);
	}
	
	public PlayerInterface addPlayerInterface(String playerName, Executor interfaceExec) {
		PlayerInterface newInterface = new PlayerInterface(interfaceExec);
			
		addPlayerInterface(playerName, newInterface);
		
		return newInterface;
	}
	
	protected void addPlayerInterface(String playerName, PlayerInterface playerInterface) {
		_playersInterfaces.get(playerName).add(playerInterface);
		
		playerInterface.updateInfos(_gameInfos.getPlayerInfosByName(playerName));
	}
	
	public PlayerInterface[] createInterfacesForAllPlayers(final Executor interfaceExecutor) {
		List<PlayerInfos> playersInfos = _gameInfos.getPlayersInfos();
		int playersCount = playersInfos.size();
		
		PlayerInterface[] playersInterfaces = new PlayerInterface[playersCount];
		
		for (int i = 0; i < playersCount; ++i) {
			PlayerInterface newInterface = new PlayerInterface(interfaceExecutor);
			
			addPlayerInterface(playersInfos.get(i).getName(), newInterface);
			
			playersInterfaces[i] = newInterface;
		}
		
		return playersInterfaces;
	}
	
	public void removePlayerInterface(String playerName, PlayerInterface playerInterface) {
		_playersInterfaces.get(playerName).remove(playerInterface);
	}
	
	protected void onPlayerBusted(int pos, String name) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onPlayerBusted(pos, name);
	}
	
	protected void onNextPos(PlayerInfos p, int oldPos, int newPos) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onNextPos(p, oldPos, newPos);
	}

	protected void onRoundCompleted(boolean onlyOneNonFoldedPlayer) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onRoundCompleted(onlyOneNonFoldedPlayer);
	}

	protected void onRoundStarted(boolean isFirstRound) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onRoundStarted(isFirstRound);
	}
	
	protected void onTurnStarted(int firstPlayerPos) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onTurnStarted(firstPlayerPos);
	}
	
	protected void onTurnCompleted(int lastPlayerPos) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onTurnCompleted(lastPlayerPos);
	}
	
	protected void onGameStarted() {
		for (GameEngineInterface i : _engineInterfaces)
			i.onGameStarted();
	}
	
	protected void onGameOver(List<PlayerRank> rankings) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onGameOver(rankings);
	}
	
	protected void onNewBoardCards(int addIndex, List<Card> newCards) {
		for (GameEngineInterface i : _engineInterfaces)
			i.onNewBoardCards(addIndex, newCards);
	}
	
	protected void onBoardCleared() {
		for (GameEngineInterface i : _engineInterfaces)
			i.onBoardCleared();
	}
	
	protected void updateGameInfos(GameInfos gameInfos) {
		if (_gameInfos == null)
			initPlayersInterfacesMap(gameInfos.getPlayersInfos());
		
		_gameInfos = gameInfos;
		
		List<PlayerInfos> newPlayersInfos = _gameInfos.getPlayersInfos();
		
		for (PlayerInfos newInfos : newPlayersInfos) {
			List<PlayerInterface> playerInterfaces = _playersInterfaces.get(newInfos.getName());
			
			for (PlayerInterface i : playerInterfaces)
				i.updateInfos(newInfos);
		}
		
		for (GameEngineInterface i : _engineInterfaces)
			i.updateGameInfos(_gameInfos);
	}
	
	protected static ScheduledExecutorService createEngineExecutor() {
		ScheduledThreadPoolExecutor exec = 
				ConcurrentUtil.newCrashHandlerScheduledThreadPool(1, new BasicThreadFactory("GameEngineThread"), BaseApp.FX_CRASH_HANDLER);
		
		exec.setRemoveOnCancelPolicy(true);
		exec.setRejectedExecutionHandler(new LogPolicy(BaseApp.LOGGER));
		
		return Executors.unconfigurableScheduledExecutorService(exec);
	}
}
