package com.oasisinfinity.oasispoker;

import com.oasisinfinity.oasispoker.controllers.BaseController;

import javafx.scene.Scene;
import net.jcip.annotations.Immutable;

@Immutable
public class AppScene {
	private final Scene _scene;
	private final BaseController _controller;
	
	public AppScene(Scene scene, BaseController controller) {
		_scene = scene;
		_controller = controller;
	}

	public Scene getScene() {
		return _scene;
	}
	
	public BaseController getController() {
		return _controller;
	}
}
