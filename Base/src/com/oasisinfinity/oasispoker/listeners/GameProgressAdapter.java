package com.oasisinfinity.oasispoker.listeners;

import java.util.List;

import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.PlayerRank;

public abstract class GameProgressAdapter implements GameProgressListener {
	@Override
	public void roundStarted(boolean isFirstRound) { /* Empty */ }

	@Override
	public void roundCompleted(boolean onlyOneNonFoldedPlayer) { /* Empty */ }	
	
	@Override
	public void turnStarted(int firstPlayerPos) { /* Empty */ }
	
	@Override
	public void turnCompleted(int lastPlayerPos) { /* Empty */ }
	
	@Override
	public void gameStarted() { /* Empty */ }
	
	@Override
	public void gameOver(List<PlayerRank> rankings) { /* Empty */ }
	
	@Override
	public void nextPos(PlayerInfos p, int oldPos, int newPos) { /* Empty */ }
	
	@Override
	public void newBoardCards(int addIndex, List<Card> newCards) { /* Empty */ }
	
	@Override
	public void boardCleared() { /* Empty */ }
	
	@Override
	public void playerBusted(int pos, String name) { /* Empty */ }
}
