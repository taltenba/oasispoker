package com.oasisinfinity.oasispoker.listeners;

public abstract class SettingsAdapter implements SettingsListener {
	public void newPseudo(String newPseudo) { /* Empty */ }
	
	public void newMusicVolume(double newMusicVolume) { /* Empty */ }
	
	public void newSoundEffectsVolume(double newSoundsEffectVolume) { /* Empty */ }
}
