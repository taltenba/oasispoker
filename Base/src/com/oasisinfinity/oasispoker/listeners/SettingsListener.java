package com.oasisinfinity.oasispoker.listeners;

public interface SettingsListener {
	void newPseudo(String newPseudo);
	void newMusicVolume(double newMusicVolume);
	void newSoundEffectsVolume(double newSoundsEffectVolume);
}
