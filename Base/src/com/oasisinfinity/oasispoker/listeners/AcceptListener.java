package com.oasisinfinity.oasispoker.listeners;

import java.util.EventListener;

import com.oasisinfinity.oasispoker.network.Client;
import com.oasisinfinity.oasispoker.network.ClientTask;

public interface AcceptListener extends EventListener {
	public void clientAccepted(Client c, ClientTask task);
}
