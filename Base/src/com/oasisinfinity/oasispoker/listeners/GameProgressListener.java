package com.oasisinfinity.oasispoker.listeners;

import java.util.EventListener;
import java.util.List;

import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.PlayerRank;

public interface GameProgressListener extends EventListener {
	public void gameStarted();
	public void roundStarted(boolean isFirstRound);
	public void turnStarted(int firstPlayerPos);
	public void nextPos(PlayerInfos p, int oldPos, int newPos);
	public void turnCompleted(int lastPlayerPos);
	public void roundCompleted(boolean onlyOneNonFoldedPlayer);
	public void gameOver(List<PlayerRank> rankings);
	public void newBoardCards(int addIndex, List<Card> newCards);
	public void boardCleared();
	public void playerBusted(int pos, String name);
}
