package com.oasisinfinity.oasispoker.listeners;

import java.util.EventListener;

import com.oasisinfinity.oasispoker.network.Client;
import com.oasisinfinity.oasispoker.network.Message;

public interface NewMessageListener extends EventListener {
	public void newMessage(Client c, Message msg);
}
