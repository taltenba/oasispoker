package com.oasisinfinity.oasispoker.listeners;

import java.util.EventListener;

import com.oasisinfinity.oasispoker.network.Message;

public interface ServerResponseListener extends EventListener {
	public void serverAnswered(Message response);
}
