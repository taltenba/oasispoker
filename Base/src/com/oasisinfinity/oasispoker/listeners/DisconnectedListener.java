package com.oasisinfinity.oasispoker.listeners;

import java.util.EventListener;

import com.oasisinfinity.oasispoker.network.Client;

public interface DisconnectedListener extends EventListener {
	public void disconnected(Client c);
}
