package com.oasisinfinity.oasispoker;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.oasisinfinity.oasispoker.controllers.BaseController;
import com.oasisinfinity.oasispoker.models.AppInfos;
import com.oasisinfinity.oasispoker.models.AppInfos.AppName;
import com.oasisinfinity.utils.ArrayUtil;
import com.oasisinfinity.utils.GoogleMail.GoogleAccount;
import com.oasisinfinity.utils.concurrent.SingleAppInstanceLock;
import com.oasisinfinity.utils.io.IOUtil;
import com.oasisinfinity.utils.javafx.AlertUtil;
import com.oasisinfinity.utils.javafx.AnimUtil;
import com.oasisinfinity.utils.javafx.FxCrashHandler;
import com.oasisinfinity.utils.logging.AsyncFileLogger;
import com.oasisinfinity.utils.logging.CrashHandler;

import javafx.animation.ParallelTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public abstract class BaseApp extends Application {
	public static final String USER_NAME_ARG_KEY = "userName";
	public static final GoogleAccount REPORT_MAIL_ACCOUNT = new GoogleAccount("oasisinf.feedback@gmail.com", "jtecrachedessus");
	public static final AlertUtil ALERT_UTIL = new AlertUtil(BaseApp.class.getResource("/css/Application.css").toExternalForm(), StageStyle.UNDECORATED);
	public static final String APP_INFOS_FILE_NAME = "app_infos.json";
	public static final String ACCOUNTS_PATH = IOUtil.getAppDataDir("OasisPoker/Accounts");
	public static final String LOGS_OUTPUT_PATH = IOUtil.getAppDataDir("OasisPoker/Logs");
	public static final AsyncFileLogger LOGGER;
	public static final CrashHandler FX_CRASH_HANDLER;

	static {
		AsyncFileLogger tmp = null;
		
		try {
			tmp = new AsyncFileLogger(LOGS_OUTPUT_PATH + "/OasisPoker", REPORT_MAIL_ACCOUNT);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
		
		LOGGER = tmp;
		FX_CRASH_HANDLER = LOGGER == null ? null : new FxCrashHandler(LOGGER);
	}
	
	protected static final Duration HALF_SCENE_TRANSITION_DUR = Duration.millis(500);
	
	protected final String _mainSceneName;
	protected Stage _primaryStage;
	
	private final Map<String, AppScene> _scenes;
	private final Map<AppName, AppInfos> _appInfos;
	
	private String _currentSceneName;
	
	protected BaseApp(String mainSceneName) {
		Thread.currentThread().setUncaughtExceptionHandler(FX_CRASH_HANDLER);
		
		_scenes = new HashMap<>();
		_mainSceneName = mainSceneName;
		
		List<AppInfos> appInfos = loadAppInfos();
		
		if (appInfos != null)
			_appInfos = AppInfos.appInfosListToMap(appInfos);
		else
			_appInfos = null;
	}
	
	@Override
	public void start(Stage primaryStage) {
		start(primaryStage, "OasisPoker", true);
	}
	
	public void start(Stage primaryStage, String title, boolean allowMultipleInstances) {
		try {
			if (!allowMultipleInstances && !(new SingleAppInstanceLock(title).lock())) {
				BaseApp.ALERT_UTIL.showErrorMsg("D�j� en cours d'execution", "Impossible de d�marrer une nouvelle instance du programme");
				return;
			}
		} catch (IOException e) {
			logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.showErrorMsg("Initialisation impossible", "Impossible de cr�er le fichier d'instance unique.");
			return;
		}
		
		_primaryStage = primaryStage;
		_primaryStage.setTitle(title);
		_primaryStage.getIcons().add(new Image(BaseApp.class.getResourceAsStream("/images/icon.png")));
		
		try {
			loadScenes();
			
			_currentSceneName = _mainSceneName;
			
			_primaryStage.setScene(_scenes.get(_mainSceneName).getScene());
			_primaryStage.show();
		} catch (IOException e) {
			logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.corruptedInstallation();
			return;
		}
		
		ALERT_UTIL.setParentWindow(_primaryStage.getScene().getWindow());
		
		double stageHeight = _primaryStage.getHeight();
		double stageWidth = _primaryStage.getWidth();
		
		_primaryStage.setMinHeight(stageHeight);
		_primaryStage.setMinWidth(stageWidth);
		
		_primaryStage.heightProperty().addListener((observable, oldValue, newValue) -> {
			resize(false);
		});
		
		_primaryStage.widthProperty().addListener((observable, oldValue, newValue) -> {
			resize(true);
		});
	}
	
	@Override
	public void stop() {
		LOGGER.close();
		
		try {
			super.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.exit(0); // Execute shutdown hooks
	}
	
	protected abstract void loadScenes() throws IOException;
	
	private void resize(boolean isWidth) {
		Region rootPane = (Region) _primaryStage.getScene().getRoot().getChildrenUnmodifiable().get(0);
		
		double ratio = rootPane.getMinHeight() / rootPane.getMinWidth();
		double sceneHeight = _primaryStage.getHeight() - (_primaryStage.getMinHeight() - rootPane.getMinHeight());
		double sceneWidth = _primaryStage.getWidth() - (_primaryStage.getMinWidth() - rootPane.getMinWidth());
		
		double neededSize, maxSize;
		
		if (isWidth) {
			neededSize = sceneWidth * ratio;
			maxSize = sceneHeight;
		} else {
			neededSize = sceneHeight / ratio;
			maxSize = sceneWidth;
		}
		
		double widthAdj = 0.0, heightAdj = 0.0;
		
		boolean adjustHeight;
		
		if (neededSize <= maxSize) {
			adjustHeight = isWidth;	
		} else {
			if (isWidth) {
				neededSize = sceneHeight / ratio;
				adjustHeight = false;
			} else {
				neededSize = sceneWidth * ratio;
				adjustHeight = true;
			}
		}
		
		if (adjustHeight)
			heightAdj = (sceneHeight - neededSize) / 2;
		else
			widthAdj = (sceneWidth - neededSize) / 2;
		
		AnchorPane.setLeftAnchor(rootPane, widthAdj);
		AnchorPane.setRightAnchor(rootPane, widthAdj);
		AnchorPane.setTopAnchor(rootPane, heightAdj);
		AnchorPane.setBottomAnchor(rootPane, heightAdj);
	}
	
	protected Scene loadScene(String fxmlName) throws IOException {
		return loadScene(fxmlName, true);
	}
	
	protected Scene loadScene(String fxmlName, boolean hasController) throws IOException {
		FXMLLoader loader = new FXMLLoader();	
		loader.setLocation(BaseApp.class.getResource("/fxml/" + fxmlName + ".fxml"));
		
		Scene scene = new Scene(loader.load(), -1, -1, false, SceneAntialiasing.BALANCED);
		
		BaseController controller = null;
		
		if (hasController) {
			controller = (BaseController) loader.getController();
			controller.setMainApp(this);
		}
		
		_scenes.put(fxmlName, new AppScene(scene, controller));
		
		return scene;
	}
	
	public Stage getStage() {
		return _primaryStage;
	}
	
	public Scene getScene(String sceneName) {
		return _scenes.get(sceneName).getScene();
	}
	
	public BaseController getController(String controllerName) {
		return _scenes.get(controllerName).getController();
	}
	
	public String getMainSceneName() {
		return _mainSceneName;
	}
	
	public AppInfos getAppInfosByName(AppName app) {
		return _appInfos == null ? null : _appInfos.get(app);
	}
	
	public Map<AppName, AppInfos> getAppInfos() {
		return _appInfos == null ? null : new HashMap<AppName, AppInfos>(_appInfos);
	}
	
	public String getVersion(AppName app) {
		AppInfos infos = getAppInfosByName(app);
		
		if (infos != null)
			return infos.getVersion();
		
		return null;
	}
	
	public void goToNewScene(String sceneName) {
		if (_currentSceneName.equals(sceneName))
			return;
		
		_currentSceneName = sceneName;
		
		Parent oldSubRoot = (Parent) _primaryStage.getScene().getRoot().getChildrenUnmodifiable().get(0);
		
		/*ParallelTransition fadeOutAnims = new ParallelTransition();
				
		for (Node n : oldSubRoot.getChildrenUnmodifiable()) {
			FadeTransition fadeOutAnim = new FadeTransition(Duration.millis(500), n);
			fadeOutAnim.setFromValue(1.0);
			fadeOutAnim.setToValue(0.0);
			
			fadeOutAnims.getChildren().add(fadeOutAnim);
		}*/
		
		ParallelTransition fadeOutAnims = AnimUtil.parallelFadeOutAnims(HALF_SCENE_TRANSITION_DUR, oldSubRoot.getChildrenUnmodifiable());
		
		fadeOutAnims.setOnFinished(e -> {
			Scene newScene = getScene(sceneName);
			Parent newSubRoot = (Parent) newScene.getRoot().getChildrenUnmodifiable().get(0);
			
			_primaryStage.setScene(newScene);
			
			AnchorPane.setLeftAnchor(newSubRoot, AnchorPane.getLeftAnchor(oldSubRoot));
			AnchorPane.setRightAnchor(newSubRoot, AnchorPane.getRightAnchor(oldSubRoot));
			AnchorPane.setTopAnchor(newSubRoot, AnchorPane.getTopAnchor(oldSubRoot));
			AnchorPane.setBottomAnchor(newSubRoot, AnchorPane.getBottomAnchor(oldSubRoot));
			
			AnimUtil.parallelFadeInAnims(HALF_SCENE_TRANSITION_DUR, newSubRoot.getChildrenUnmodifiable()).play();
			
			/*ParallelTransition fadeInAnims = new ParallelTransition();
			
			for (Node n : newSubRoot.getChildrenUnmodifiable()) {
				FadeTransition fadeInAnim = new FadeTransition(Duration.millis(500), n);
				fadeInAnim.setFromValue(0.0);
				fadeInAnim.setToValue(1.0);
				
				fadeInAnims.getChildren().add(fadeInAnim);
			}
			
			fadeInAnims.play();*/
		});
		
		fadeOutAnims.play();
		
		getController(sceneName).onNavigateTo();
	}
	
	public void reloadAppInfos() {
		List<AppInfos> appInfos = loadAppInfos();
		
		if (appInfos != null) {
			for (AppInfos infos : appInfos)
				_appInfos.put(infos.getName(), infos);
		}
	}
	
	public static List<AppInfos> loadAppInfos() {
		List<AppInfos> appInfos = null;
		
		try {
			appInfos = ArrayUtil.toArrayList(IOUtil.loadJsonFromFile(APP_INFOS_FILE_NAME, AppInfos[].class));
		} catch (IOException | JsonSyntaxException | JsonIOException e) {
			logException(Level.SEVERE, e);
		}
		
		return appInfos;
	}
	
	public static String createAccountPath(String accountName) {
		return BaseApp.ACCOUNTS_PATH + "/" + accountName + ".json";
	}
	
	public static void logException(Level lvl, Throwable t) {
		if (LOGGER != null)
			LOGGER.logException(lvl, t);
	}
	
	public static void logMsg(Level lvl, String msg) {
		if (LOGGER != null)
			LOGGER.log(lvl, msg);
	}
	
	public static void corruptedInstallation(Throwable t) {
		ALERT_UTIL.corruptedInstallation();
		logException(Level.SEVERE, t);
	}
}
