package com.oasisinfinity.oasispoker.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.Card.FaceCard;
import com.oasisinfinity.oasispoker.models.Card.Suit;
import com.oasisinfinity.utils.Deck;

public final class PokerDeck {
	private PokerDeck() { /* Empty */ }
	
	public static Deck<Card> newDeck() {
		List<Card> cards = new ArrayList<>(52);
		Suit[] suits = Suit.values();
		
		for (FaceCard fc : FaceCard.values()) {
			for (Suit s : suits)
				cards.add(new Card(fc, s));
		}
		
		return new Deck<>(cards);
	}
	
	public static Deck<Card> newDeck(Set<Card> excludedCards) {
		List<Card> cards = new ArrayList<>(52 - excludedCards.size());
		Suit[] suits = Suit.values();
		
		for (FaceCard fc : FaceCard.values()) {
			for (Suit s : suits) {
				Card c = new Card(fc, s);
				
				if (!excludedCards.contains(c))
					cards.add(new Card(fc, s));
			}
		}
		
		return new Deck<>(cards);
	}
}
