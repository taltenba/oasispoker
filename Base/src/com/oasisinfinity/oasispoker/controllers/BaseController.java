package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.oasispoker.BaseApp;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public abstract class BaseController {
	@FXML private Button _goBackButton;
	
	private BaseApp _mainApp;
	
	protected BaseController() {
		// Empty
	}

	public void setMainApp(BaseApp mainApp) {
		_mainApp = mainApp;
	}
	
	@FXML
	protected void initialize() {
		_goBackButton.setOnMouseClicked(e -> onReturnToMainScene());
	}
	
	protected void onReturnToMainScene() {
		goToNewScene(_mainApp.getMainSceneName());
	}
	
	protected BaseApp getMainApp() {
		return _mainApp;
	}
	
	protected void goToNewScene(String sceneName) {
		if (onNavigateFrom())
			_mainApp.goToNewScene(sceneName);
	}
	
	protected Button getGoBackButton() {
		return _goBackButton;
	}
	
	public void onNavigateTo() {
		// Empty
	}
	
	/**
	 * Callback when the we are going to new scene
	 * @return Continue navigation ?
	 */
	protected boolean onNavigateFrom() {
		return true;
	}
}
