package com.oasisinfinity.oasispoker.models;

import com.oasisinfinity.oasispoker.engines.Player.Action;

import net.jcip.annotations.Immutable;

@Immutable
public final class ActionInfos {
	private final Action _action;
	private final String _fromName;
	private final int _amount;
	
	private ActionInfos(Action a, String fromName) {
		this(a, fromName, 0);
	}
	
	private ActionInfos(Action a, String fromName, int amount) {
		_action = a;
		_fromName = fromName;
		_amount = amount;
	}

	public Action getAction() {
		return _action;
	}

	public String getFromName() {
		return _fromName;
	}

	public int getAmount() {
		return _amount;
	}

	public static ActionInfos newFoldActionInfos(String fromName) {
		return new ActionInfos(Action.Fold, fromName);
	}
	
	public static ActionInfos newCheckActionInfos(String fromName) {
		return new ActionInfos(Action.Check, fromName);
	}
	
	public static ActionInfos newCallActionInfos(String fromName) {
		return new ActionInfos(Action.Call, fromName);
	}
	
	public static ActionInfos newRaiseActionInfos(String fromName, int amount) {
		if (amount <= 0)
			throw new IllegalArgumentException("amount <= 0");
		
		return new ActionInfos(Action.Raise, fromName, amount);
	}
	
	@Override
	public String toString() {
		return _fromName + " : " + _action + " (" + _amount + ")";
	}
}
