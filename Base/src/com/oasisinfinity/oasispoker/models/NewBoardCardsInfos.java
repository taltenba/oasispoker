package com.oasisinfinity.oasispoker.models;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.jcip.annotations.Immutable;

@Immutable
public final class NewBoardCardsInfos {
	private final int _addIndex;
	private final Card[] _newCards;
	
	public NewBoardCardsInfos(int addIndex, List<Card> newCards) {
		_addIndex = addIndex;
		_newCards = newCards.toArray(new Card[newCards.size()]);
	}
	
	public int getAddIndex() {
		return _addIndex;
	}
	
	public List<Card> getNewCards() {
		return Collections.unmodifiableList(Arrays.asList(_newCards));
	}
}
