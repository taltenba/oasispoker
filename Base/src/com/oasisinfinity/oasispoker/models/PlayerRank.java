package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.Immutable;

@Immutable
public final class PlayerRank implements Comparable<PlayerRank> {
	private final String _playerName;
	private final int _rank;

	public PlayerRank(String playerName, int rank) {
		if (playerName == null)
			throw new NullPointerException("playerName is null");
		
		_playerName = playerName;
		_rank = rank;
	}
	
	public String getPlayerName() {
		return _playerName;
	}
	
	public int getRank() {
		return _rank;
	}

	@Override
	public int compareTo(PlayerRank other) {
		int rankCompareResult = Integer.compare(_rank, other._rank);
		
		if (rankCompareResult == 0)
			return _playerName.compareTo(other._playerName);
		
		return rankCompareResult;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + _playerName.hashCode();
		result = 31 * result + _rank;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (!(obj instanceof PlayerRank))
			return false;
		
		PlayerRank other = (PlayerRank) obj;

		return _rank == other._rank && _playerName.equals(other._playerName);
	}
	
	
}
