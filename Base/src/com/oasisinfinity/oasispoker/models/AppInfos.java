package com.oasisinfinity.oasispoker.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.jcip.annotations.Immutable;

@Immutable
public final class AppInfos {
	public enum AppName {
		Updater,
		Client,
		Server,
		Launcher,
	}
	
	private final AppName _name;
	private final String _version;
	private final String _changelog;
	
	public AppInfos(AppName name, String version, String changelog) {
		_name = name;
		_version = version;
		_changelog = changelog;
	}

	public AppName getName() {
		return _name;
	}

	public String getVersion() {
		return _version;
	}

	public String getChangelog() {
		return _changelog;
	}
	
	public static Map<AppName, AppInfos> appInfosListToMap(List<AppInfos> appInfos) {
		Map<AppName, AppInfos> map = new HashMap<>();
		
		for (AppInfos infos : appInfos)
			map.put(infos.getName(), infos);
		
		return map;
	}
}
