package com.oasisinfinity.oasispoker.models;

import java.util.HashSet;
import java.util.Set;

import com.oasisinfinity.oasispoker.listeners.SettingsListener;

import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class User {
	/* SETTINGS */
	private String _pseudo;
	private float _musicVolume;
	private float _soundEffectsVolume;
	
	/* STATS */
	private StatsInfos _stats;
	
	/* TRANSIENT */
	private transient final Set<SettingsListener> _settingsListeners;
	private transient boolean _hasBeenModified;
	
	public User() {
		this("Player");
	}
	
	public User(String pseudo) {
		_pseudo = pseudo;
		_musicVolume = 0.8f;
		_soundEffectsVolume = 1.0f;
		_settingsListeners = new HashSet<>();
		_stats = new StatsInfos();
	}
	
	public void addSettingsListener(SettingsListener l) {
		_settingsListeners.add(l);
	}
	
	public void removeSettingsListener(SettingsListener l) {
		_settingsListeners.remove(l);
	}
	
	public String getPseudo() {
		return _pseudo;
	}
	
	public float getMusicVolume() {
		return _musicVolume;
	}
	
	public float getSoundEffectsVolume() {
		return _soundEffectsVolume;
	}
	
	public StatsInfos getStats() {
		return _stats;
	}
	
	public boolean hasBeenModified() {
		return _hasBeenModified;
	}
	
	public void setPseudo(String pseudo) {
		_pseudo = pseudo;
		
		for (SettingsListener l : _settingsListeners)
			l.newPseudo(pseudo);
		
		_hasBeenModified = true;
	}
	
	public void setMusicVolume(float volume) {
		_musicVolume = volume;
		
		for (SettingsListener l : _settingsListeners)
			l.newMusicVolume(volume);
		
		_hasBeenModified = true;
	}
	
	public void setSoundEffectsVolume(float volume) {
		_soundEffectsVolume = volume;
		
		for (SettingsListener l : _settingsListeners)
			l.newSoundEffectsVolume(volume);
		
		_hasBeenModified = true;
	}
	
	public void updateStats(StatsInfos stats) {
		_stats = stats;
		_hasBeenModified = true;
	}
	
	public void setSaved() {
		_hasBeenModified = false;
	}
}
