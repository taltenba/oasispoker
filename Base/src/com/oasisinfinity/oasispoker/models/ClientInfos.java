package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.Immutable;

@Immutable
public final class ClientInfos {
	private final String _name;
	private final StatsInfos _stats;
	private final int _compatibilityVersion;
	private final boolean _isSpectator;
	
	public ClientInfos(String name, StatsInfos stats, int compatibilityVersion, boolean isSpectator) {
		_name = name;
		_stats = stats;
		_compatibilityVersion = compatibilityVersion;
		_isSpectator = isSpectator;
	}

	public String getName() {
		return _name;
	}
	
	public StatsInfos getStats() {
		return _stats;
	}

	public int getCompatibilityVersion() {
		return _compatibilityVersion;
	}
	
	public boolean isSpectator() {
		return _isSpectator;
	}
}
