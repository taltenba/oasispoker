package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.Immutable;

@Immutable
public final class ChatMessage {
	private final String _senderName;
	private final String _msg;
	
	public ChatMessage(String senderName, String msg) {
		_senderName = senderName;
		_msg = msg;
	}

	public String getSenderName() {
		return _senderName;
	}

	public String getMsg() {
		return _msg;
	}
}
