package com.oasisinfinity.oasispoker.models;

import java.util.Optional;

import com.oasisinfinity.oasispoker.engines.Player.Action;
import com.oasisinfinity.oasispoker.engines.Player.Role;
import com.oasisinfinity.oasispoker.engines.Player.Status;
import com.oasisinfinity.oasispoker.engines.PlayerInfosGetter;
import com.oasisinfinity.utils.ImmutablePair;

import net.jcip.annotations.Immutable;

@Immutable
public final class PlayerInfos implements PlayerInfosGetter {
	private final String _name;
	private final Optional<StatsInfos> _stats;
	private final int _cash;
	private final int _stake;
	private final Action _action;
	private final Role _role;
	private final ImmutablePair<Card> _cards;
	private final Hand _hand;
	private final Status _status;
	private final boolean _hasBet;

	public PlayerInfos(String name, StatsInfos stats, int cash, int stake, Action action, Role role, ImmutablePair<Card> cards, Hand hand, Status status, boolean hasBet) {
		_name = name;
		_stats = Optional.ofNullable(stats);
		_cash = cash;
		_stake = stake;
		_action = action;
		_role = role;
		_cards = cards;
		_hand = hand;
		_status = status;
		_hasBet = hasBet;
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public Optional<StatsInfos> getStats() {
		return _stats;
	}

	@Override
	public int getCash() {
		return _cash;
	}

	@Override
	public int getStake() {
		return _stake;
	}

	@Override
	public Action getAction() {
		return _action;
	}

	@Override
	public Role getRole() {
		return _role;
	}

	@Override
	public ImmutablePair<Card> getCards() {
		return _cards;
	}

	@Override
	public Hand getHand() {
		return _hand;
	}

	@Override
	public Status getStatus() {
		return _status;
	}

	@Override
	public boolean isFolded() {
		return _action == Action.Fold;
	}
	
	@Override
	public boolean hasBet() {
		return _hasBet;
	}

	@Override
	public boolean canPlay() {
		return _action != Action.Fold && _action != Action.AllIn;
	}

	@Override
	public int hashCode() {
		return 31 + _name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null || !(obj instanceof PlayerInfos))
			return false;

		return _name.equals(((PlayerInfos) obj)._name);
	}
	
	@Override
	public String toString() {
		return _name + " ($" + _cash + ") : " + _cards;
	}
}
