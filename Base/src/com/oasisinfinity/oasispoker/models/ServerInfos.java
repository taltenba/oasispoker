package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.Immutable;

@Immutable
public final class ServerInfos {
	private final GameConfig _gameConfig;
	private final int _playersCount;
	private final int _maxPlayersCount;
	
	public ServerInfos(GameConfig gameConfig, int playersCount, int maxPlayersCount) {
		_gameConfig = gameConfig;
		_playersCount = playersCount;
		_maxPlayersCount = maxPlayersCount;
	}

	public GameConfig getGameConfig() {
		return _gameConfig;
	}

	public int getPlayersCount() {
		return _playersCount;
	}

	public int getMaxPlayersCount() {
		return _maxPlayersCount;
	}
}
