package com.oasisinfinity.oasispoker.models;

import java.io.Serializable;
import java.util.Comparator;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javafx.scene.image.Image;
import net.jcip.annotations.Immutable;

@Immutable
@XmlRootElement
public final class Card implements Comparable<Card> {
	private static final CardStrengthComparator STRENGTH_COMPARATOR = new CardStrengthComparator();
		
	public static final class CardStrengthComparator implements Comparator<Card>, Serializable {
		private static final long serialVersionUID = -1224330804152048277L;

		/**
		 * Order is descendant
		 */
		@Override
		public int compare(Card card1, Card card2) {
			return card1._faceCard.compareTo(card2._faceCard);
		}
		
		public boolean equal(Card card1, Card card2) {
			if (card1 == card2)
				return true;
			
			return card1._faceCard == card2._faceCard;
		}
		
		public int hash(Card card) {
			return 31 * 17 + card._faceCard.hashCode();
		}
	}
	
	public enum Suit {
		Clubs,
		Diamonds,
		Spades,
		Hearts;
	}

	public enum FaceCard {
		Ace,
		King,
		Queen,
		Jack,
		Ten,
		Nine,
		Eight,
		Seven,
		Six,
		Five,
		Four,
		Three,
		Two;
		
		public boolean isBetterThan(FaceCard other) {
			return compareTo(other) <= 0;
		}
		
		public boolean isStriclyBetterThan(FaceCard other) {
			return compareTo(other) < 0;
		}
	}

	@XmlElement private final FaceCard _faceCard;
	@XmlElement private final Suit _suit;
	
	// For XmlSerializer
	@SuppressWarnings("unused")
	private Card() {
		_faceCard = null;
		_suit = null;
	}
	
	public Card(FaceCard faceCard, Suit suit) {
		_faceCard = faceCard;
		_suit = suit;
	}
	
	public FaceCard getFaceCard() {
		return _faceCard;
	}
	
	public Suit getSuit() {
		return _suit;
	}

	public Image getImage() {
		return new Image("/images/" + _faceCard.toString().toLowerCase() + "_of_" + _suit.toString().toLowerCase() + ".png");
	}
	
	public Image getBigImage() {
		return new Image("/images/" + _faceCard.toString().toLowerCase() + "_of_" + _suit.toString().toLowerCase() + "_big.png");
	}

	@Override
	public int compareTo(Card other) {
		int ret = _faceCard.compareTo(other._faceCard);
		
		if (ret == 0)
			ret = _suit.compareTo(other._suit);
		
		return ret;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (!(obj instanceof Card))
			return false;
		
		Card other = (Card) obj;
		
		return _faceCard == other._faceCard && _suit == other._suit;
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + _faceCard.hashCode();
		result = 31 * result + _suit.hashCode();
		return result;
	}
	
	@Override
	public String toString() {
		return _faceCard.toString() + " of " + _suit.toString();
	}

	public static CardStrengthComparator getStrengthComparator() {
		return STRENGTH_COMPARATOR;
	}
}
