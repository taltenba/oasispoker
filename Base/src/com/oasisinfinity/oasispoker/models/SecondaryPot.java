package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class SecondaryPot {
	private final int _maxAdd;
	
	private int _value;
	private boolean _isStopped;

	public SecondaryPot(int value, int maxAdd) {
		_value = value;
		_isStopped = false;
		_maxAdd = maxAdd;
	}

	public void add(int previousStake, int newStake) {
		if (_isStopped)
			return;
		
		_value += newStake > _maxAdd ? _maxAdd - previousStake : newStake - previousStake;
	}
	
	public int getValue() {
		return _value;
	}
	
	public void stopAdd() {
		if (_isStopped)
			return;
		
		_isStopped = true;
	}
}
