package com.oasisinfinity.oasispoker.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.oasisinfinity.oasispoker.engines.GameInfosGetter;
import com.oasisinfinity.oasispoker.engines.NamedPlayer;
import com.oasisinfinity.oasispoker.engines.PlayerInfosGetter;
import com.oasisinfinity.oasispoker.engines.ServerGameEngine.Phase;
import com.oasisinfinity.utils.CircularList;

import net.jcip.annotations.Immutable;

@Immutable
public final class GameInfos implements GameInfosGetter {
	private final List<PlayerInfos> _playersInfos;
	private final List<Card> _board;
	private final Phase _phase;
	private final int _currentPos;
	private final int _dealerPos;
	private final int _playersCount;
	private final int _smallBlind;
	private final int _antes;
	private final int _maxRaise;
	private final int _maxStake;
	private final int _pot;
	private final boolean _isGameOver;
	
	public GameInfos(PlayerInfos[] playersInfos, List<Card> board, Phase phase, int currentPos, int dealerPos, int playersCount,
			 int smallBlind, int antes, int maxRaise, int maxStake, int pot, boolean isGameOver) {
		_playersInfos = Collections.unmodifiableList(Arrays.asList(playersInfos.clone()));
		_board = Collections.unmodifiableList(new ArrayList<>(board));
		_phase = phase;
		_currentPos = currentPos;
		_dealerPos = dealerPos;
		_playersCount = playersCount;
		_smallBlind = smallBlind;
		_antes = antes;
		_maxRaise = maxRaise;
		_maxStake = maxStake;
		_pot = pot;
		_isGameOver = isGameOver;
	}
	
	@Override
	public List<PlayerInfos> getPlayersInfos() {
		return _playersInfos;
	}
	
	@Override
	public PlayerInfos getCurrentPlayerInfos() {
		return _playersInfos.get(getRealCurrentPos());
	}
	
	@Override
	public PlayerInfos getPlayerInfosByName(String playerName) {
		for (PlayerInfos infos : _playersInfos) {
			if (infos.getName().equals(playerName))
				return infos;
		}
		
		return null;
	}
	
	@Override
	public List<PlayerInfos> getNonFoldedPlayers() {
		List<PlayerInfos> nonFoldedPlayers = new ArrayList<>();
		
		for (PlayerInfos p : _playersInfos) {
			if (!p.isFolded())
				nonFoldedPlayers.add(p);
		}
		
		return nonFoldedPlayers;
	}
	
	@Override
	public Phase getPhase() {
		return _phase;
	}
	
	@Override
	public List<Card> getBoard() {
		return _board;
	}
	
	@Override
	public int getCurrentPos() {
		return _currentPos;
	}
	
	@Override
	public int getDealerPos() {
		return _dealerPos;
	}

	@Override
	public int getPlayersCount() {
		return _playersCount;
	}

	@Override
	public int getSmallBlind() {
		return _smallBlind;
	}

	@Override
	public int getBigBlind() {
		return _smallBlind * 2;
	}
	
	@Override
	public int getAntes() {
		return _antes;
	}
	
	@Override
	public int getMaxRaise() {
		return _maxRaise;
	}

	@Override
	public int getMaxStake() {
		return _maxStake;
	}

	@Override
	public int getPot() {
		return _pot;
	}

	@Override
	public boolean isGameOver() {
		return _isGameOver;
	}

	@Override
	public boolean canCurrentCheck() {
		return canCheck(getCurrentPlayerInfos());
	}

	@Override
	public boolean canCheck(PlayerInfosGetter playerInfos) {
		return playerInfos.getStake() == _maxStake;
	}

	@Override
	public int getCurrentMinRaise() {
		return getMinRaise(getCurrentPlayerInfos());
	}

	@Override
	public int getMinRaise(PlayerInfosGetter playerInfos) {
		return Math.min(playerInfos.getCash(), (_maxStake + Math.max(getBigBlind(), _maxRaise)) - playerInfos.getStake());
	}

	@Override
	public int getCurrentCallAmount() {
		return getCallAmount(getCurrentPlayerInfos());
	}

	@Override
	public int getCallAmount(PlayerInfosGetter playerInfos) {
		return Math.min(playerInfos.getCash(), _maxStake - playerInfos.getStake());
	}

	@Override
	public int getRealCurrentPos() {
		return toRealPos(_currentPos);
	}
	
	@Override
	public int toRealPos(int pos) {
		return CircularList.toRealIndex(pos, _playersCount);
	}

	@Override
	public boolean isCurrentPlayer(NamedPlayer p) {
		return isCurrentPlayer(p.getName());
	}
	
	@Override
	public boolean isCurrentPlayer(String playerName) {
		return playerName.equals(getCurrentPlayerInfos().getName());
	}
}
