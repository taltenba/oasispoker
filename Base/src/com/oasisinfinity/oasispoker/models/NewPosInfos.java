package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.Immutable;

@Immutable
public final class NewPosInfos {
	private final PlayerInfos _playerInfos;
	private final int _oldPos;
	private final int _newPos;
	
	public NewPosInfos(PlayerInfos infos, int oldPos, int newPos) {
		_playerInfos = infos;
		_oldPos = oldPos;
		_newPos = newPos;
	}

	public PlayerInfos getPlayerInfos() {
		return _playerInfos;
	}

	public int getOldPos() {
		return _oldPos;
	}

	public int getNewPos() {
		return _newPos;
	}
}
