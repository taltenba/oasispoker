package com.oasisinfinity.oasispoker.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.oasisinfinity.oasispoker.models.Card.CardStrengthComparator;
import com.oasisinfinity.oasispoker.models.Card.FaceCard;
import com.oasisinfinity.oasispoker.models.Card.Suit;

import net.jcip.annotations.Immutable;

@Immutable
public final class Hand implements Comparable<Hand> {
	private static final FaceCard[] LOW_ACE_STRAIGHT_FACE_CARDS = new FaceCard[] {FaceCard.Two, FaceCard.Three, FaceCard.Four, FaceCard.Five};
	
	public enum HandType {
		StraightFlush ("Quinte flush", 5),
		LowAceStraightFlush ("Quinte flush", 5),
		FourOfAKind ("Carr�", 4),
		FullHouse ("Full", 5),
		Flush ("Couleur", 5),
		Straight ("Suite", 5),
		LowAceStraight("Suite", 5),
		ThreeOfAKind ("Brelan", 3),
		TwoPair ("Double paire", 4),
		Pair ("Paire", 2),
		HighCard ("Hauteur", 1);
	
		private final String _name;
		private final int _cardsCount;
		
		private HandType(String name, int cardsCount) {
			_name = name;
			_cardsCount = cardsCount;
		}
		
		public int getCardsCount() {
			return _cardsCount;
		}
		
		@Override
		public String toString() {
			return _name;
		}
	}

	private final List<Card> _cards;
	private final HandType _type;
	
	private Hand(List<Card> allCards, List<Card> handCards, HandType type) {
		_cards = new ArrayList<>();
		_type = type;
		
		for (int i = 0, typeCardsCount = type.getCardsCount(); i < typeCardsCount; ++i)
			_cards.add(handCards.get(i));
		
		for (int i = 0; _cards.size() < 5 && i < allCards.size(); ++i) {
			Card c = allCards.get(i);
			
			if (!_cards.contains(c))
				_cards.add(c);
		}
	}
	
	public HandType getType() {
		return _type;
	}
	
	public Card[] getCards() {
		return _cards.toArray(new Card[_cards.size()]);
	}
	
	public static Hand newHand(Card... cards) {
		return newHand(Arrays.asList(cards));
	}
	
	public static Hand newHand(List<Card> cards) {
		if (cards.size() == 0)
			throw new IllegalArgumentException("cards is empty");
		
		cards.sort(Card.getStrengthComparator());
		
		List<Card> flush = getFlush(cards);
		
		if (flush != null) {
			List<Card> uniqueFlush = removeMultipleExemplars(flush);
			
			List<Card> straightFlush = getStraight(uniqueFlush);
			
			if (straightFlush != null)
				return new Hand(cards, straightFlush, HandType.StraightFlush);
			
			List<Card> lowAceStraightFlush = getLowAceStraight(uniqueFlush);
			
			if (lowAceStraightFlush != null)
				return new Hand(cards, lowAceStraightFlush, HandType.LowAceStraightFlush);
		}
		
		Map<HandType, List<Card>> multipleCardsExemplarsByTypes = getMultipleExemplarsByTypes(cards);
		
		List<Card> fourOfAKind = multipleCardsExemplarsByTypes.get(HandType.FourOfAKind);
		
		if (fourOfAKind.size() != 0)
			return new Hand(cards, fourOfAKind, HandType.FourOfAKind);
		
		List<Card> threeOfAKind = multipleCardsExemplarsByTypes.get(HandType.ThreeOfAKind);
		List<Card> pairs = multipleCardsExemplarsByTypes.get(HandType.Pair);
		List<Card> fullHouse = getFullHouse(pairs, threeOfAKind);
		
		if (fullHouse != null)
			return new Hand(cards, fullHouse, HandType.FullHouse);
		
		if (flush != null)
			return new Hand(cards, flush, HandType.Flush);
			
		List<Card> uniqueCards = removeMultipleExemplars(cards);
		List<Card> straight = getStraight(uniqueCards);
		
		if (straight != null)
			return new Hand(cards, straight, HandType.Straight);
		
		List<Card> lowAceStraight = getLowAceStraight(uniqueCards);
		
		if (lowAceStraight != null)
			return new Hand(cards, lowAceStraight, HandType.LowAceStraight);
		
		if (threeOfAKind.size() != 0)
			return new Hand(cards, threeOfAKind, HandType.ThreeOfAKind);
		
		List<Card> twoPairs = multipleCardsExemplarsByTypes.get(HandType.TwoPair);
		
		if (twoPairs.size() != 0)
			return new Hand(cards, twoPairs, HandType.TwoPair);
		else if (pairs.size() != 0)
			return new Hand(cards, pairs, HandType.Pair);
		else
			return new Hand(cards, cards, HandType.HighCard);
	}
	
	public static List<Card> getFlush(List<Card> cards) {
		int cardsCount = cards.size();
		
		if (cardsCount < 5)
			return null;
		
		Suit[] suits = Suit.values();
		Map<Suit, List<Card>> cardsBySuit = new HashMap<>();

		for (int i = 0; i < suits.length; ++i)
			cardsBySuit.put(suits[i], new ArrayList<>());
		
		for (int i = 0; i < cards.size(); ++i) {
			Card c = cards.get(i);

			cardsBySuit.get(c.getSuit()).add(c);
		}
		
		for (Suit s : suits) {
			List<Card> list = cardsBySuit.get(s);
			
			if (list.size() >= 5)
				return list;
		}
		
		return null;
	}
	
	public static List<Card> getLowAceStraight(List<Card> uniqueCards) {
		int cardsCount = uniqueCards.size();
		
		if (cardsCount < 5 || uniqueCards.get(0).getFaceCard() != FaceCard.Ace)
			return null;
		
		List<Card> lowAceStraight = new ArrayList<>();
		
		for (int i = cardsCount - 1, j = 0; j < 4; --i, ++j) {
			Card c = uniqueCards.get(i);
			
			if (c.getFaceCard() != LOW_ACE_STRAIGHT_FACE_CARDS[j])
				return null;
			
			lowAceStraight.add(c);
		}
		
		lowAceStraight.add(uniqueCards.get(0));
		
		return lowAceStraight;
	}
	
	public static List<Card> getStraight(List<Card> uniqueCards) {
		int cardsCount = uniqueCards.size();
		
		if (cardsCount < 5)
			return null;
		
		int straightStart = 0, straightEnd = 0;
		boolean straight = false;
		
		for (int i = 0; i < cardsCount; ++i) {
			int ordinal = uniqueCards.get(i).getFaceCard().ordinal();
			
			straightStart = i;
					
			for (int j = i + 1, k = 1; j < cardsCount; ++j, ++k) {
				if (uniqueCards.get(j).getFaceCard().ordinal() != ordinal + k) {
					straightStart = j - 1;
					break;
				}
				
				if (k == 4) {
					straightEnd = j;
					straight = true;
					break;
				}
			}
			
			i = straightStart;
			
			if (straight || cardsCount - i <= 5)
				break;
		}
		
		if (!straight)
			return null;
		
		List<Card> straightCards = new ArrayList<>();
		
		for (int i = straightStart; i <= straightEnd; ++i)
			straightCards.add(uniqueCards.get(i));
		
		return straightCards;
		
	}
	
	public static List<Card> removeMultipleExemplars(List<Card> cards) {
		List<Card> uniqueCards = new ArrayList<>(cards);
		
		if (uniqueCards.isEmpty())
			return uniqueCards;
		
		CardStrengthComparator comparator = Card.getStrengthComparator();
		ListIterator<Card> it = uniqueCards.listIterator(uniqueCards.size());
		Card previous = it.previous();
		
		while (it.hasPrevious()) {
			Card current = it.previous();
			
			if (comparator.equal(current, previous))
				it.remove();
			else
				previous = current;
		}
		
		return uniqueCards;
	}
	
	public static List<Card> getFullHouse(List<Card> pairs, List<Card> threeOfAKind) {
		int threeOfAKindCount = threeOfAKind.size() / 3;
		
		List<Card> full = new ArrayList<>();
		
		if (threeOfAKindCount >= 1) {
			for (int i = 0; i < 3; ++i)
				full.add(threeOfAKind.get(i));
		} else
			return null;
		
		int pairsCount = pairs.size() / 2;
		
		if (threeOfAKindCount >= 2) {
			for (int i = 3; i < 5; ++i)
				full.add(threeOfAKind.get(i));
		} else if (pairsCount >= 1) {
			for (int i = 0; i < 2; ++i)
				full.add(pairs.get(i));
		} else 
			return null;
		
		return full;
	}
	
	public static Map<HandType, List<Card>> getMultipleExemplarsByTypes(List<Card> cards) {
		Map<HandType, List<Card>> multipleExemplarsByTypes = new HashMap<>();
		
		multipleExemplarsByTypes.put(HandType.Pair, new ArrayList<>());
		multipleExemplarsByTypes.put(HandType.TwoPair, new ArrayList<>());
		multipleExemplarsByTypes.put(HandType.ThreeOfAKind, new ArrayList<>());
		multipleExemplarsByTypes.put(HandType.FourOfAKind, new ArrayList<>());
		
		int cardsCount = cards.size();
		
		for (int i = 0; i < cardsCount; ++i) {
			Card c = cards.get(i);
			int continueIndex = i;
					
			for (int j = i + 1, k = 1; j <= cardsCount; ++j, ++k) {
				if (j == cardsCount || !Card.getStrengthComparator().equal(cards.get(j), c)) {
					if (k == 2) 
						multipleExemplarsByTypes.get(HandType.Pair).addAll(cards.subList(i, j));
					else if (k == 3)
						multipleExemplarsByTypes.get(HandType.ThreeOfAKind).addAll(cards.subList(i, j));
					
					continueIndex = j - 1;
					break;
				}
				
				if (k == 3) {
					multipleExemplarsByTypes.get(HandType.FourOfAKind).addAll(cards.subList(i, j + 1));
					continueIndex = j - 1;
					break;
				}
			}
			
			i = continueIndex;
			
			if (cardsCount - i <= 2)
				break;
		}
		
		List<Card> pairs = multipleExemplarsByTypes.get(HandType.Pair);
		
		if (pairs.size() > 2) {
			List<Card> doublePairs = multipleExemplarsByTypes.get(HandType.TwoPair);
			
			for (int i = 0; pairs.size() - i >= 4; i += 4) {
				for (int j = 0; j < 4; ++j)
					doublePairs.add(pairs.get(i + j));
			}
		}
		
		return multipleExemplarsByTypes;
	}
	
	public boolean isBetterThan(Hand other) {
		return compareTo(other) < 0;
	}

	/**
	 * Order is descendant
	 */
	@Override
	public int compareTo(Hand other) {
		int typeCompareResult = _type.compareTo(other._type);
		
		if (typeCompareResult == 0) {
			List<Card> otherCards = other._cards;
			
			for (int i = 0, size = _cards.size(); i < size; ++i) {
				int cardCompareResult = Card.getStrengthComparator().compare(_cards.get(i), otherCards.get(i));
				
				if (cardCompareResult != 0)
					return cardCompareResult;
			}
		}
		
		return typeCompareResult;
	}
	
	@Override
	public int hashCode() {
		int result = 1;
		CardStrengthComparator strengthComparator = Card.getStrengthComparator();
		
        for (Card c : _cards)
            result = 31 * result + strengthComparator.hash(c);

		return 31 * result + _type.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null || !getClass().equals(obj.getClass()))
			return false;
		
		Hand other = (Hand) obj;
		
		if (_type != other._type || _cards.size() != other._cards.size())
			return false;
		
		CardStrengthComparator strengthComparator = Card.getStrengthComparator();
		
		for (int i = 0; i < _cards.size(); ++i) {
			if (!strengthComparator.equal(_cards.get(i), other._cards.get(i)))
				return false;
		}
		
		return true;
	}

}
