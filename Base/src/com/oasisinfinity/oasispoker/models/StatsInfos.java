package com.oasisinfinity.oasispoker.models;

import java.util.EnumMap;
import java.util.Map;

import com.oasisinfinity.oasispoker.engines.ServerGameEngine.Phase;
import com.oasisinfinity.utils.io.IOUtil;

import net.jcip.annotations.Immutable;

@Immutable
public class StatsInfos {
	private final int _wonCash;
	private final int _lostCash;
	private final int _biggestWonPot;
	private final int _wonHands;
	private final int _lostHands;
	private final int _totalPlayedActions;
	private final int _raisesCount;
	private final Map<Phase, Integer> _foldedHands;
	private final Hand _bestHand;

	public StatsInfos() {
		_wonCash = 0;
		_lostCash = 0;
		_biggestWonPot = 0;
		_wonHands = 0;
		_lostHands = 0;
		_totalPlayedActions = 0;
		_raisesCount = 0;
		_foldedHands = new EnumMap<>(Phase.class);
		_bestHand = null;

		for (Phase p : Phase.values())
			_foldedHands.put(p, 0);
	}
	
	public StatsInfos(int wonCash, int lostCash, int biggestWonPot, int wonHands, int lostHands, int totalPlayedActions,
			int raisesCount, Map<Phase, Integer> foldedHands, Hand bestHand) {
		_wonCash = wonCash;
		_lostCash = lostCash;
		_biggestWonPot = biggestWonPot;
		_wonHands = wonHands;
		_lostHands = lostHands;
		_totalPlayedActions = totalPlayedActions;
		_raisesCount = raisesCount;
		_foldedHands = new EnumMap<>(foldedHands);
		_bestHand = bestHand;
	}

	public int getWonCash() {
		return _wonCash;
	}

	public int getLostCash() {
		return _lostCash;
	}

	public int getBiggestWonPot() {
		return _biggestWonPot;
	}

	public int getWonHands() {
		return _wonHands;
	}

	public int getLostHands() {
		return _lostHands;
	}
	
	public int getTotalPlayedHands() {
		return _wonHands + _lostHands;
	}

	public int getTotalPlayedActions() {
		return _totalPlayedActions;
	}

	public int getRaisesCount() {
		return _raisesCount;
	}

	public int getFoldedHands(Phase phase) {
		return _foldedHands.get(phase);
	}
	
	public Map<Phase, Integer> getFoldedHandsMap() {
		return new EnumMap<>(_foldedHands);
	}
	
	public int getTotalFoldedHands() {
		int total = 0;
		
		for (Phase p : Phase.values())
			total += _foldedHands.get(p);
		
		return total;
	}

	public Hand getBestHand() {
		return _bestHand;
	}
	
	@Override
	public String toString() {
		return IOUtil.toJson(this);
	}
}
