package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.Immutable;

@Immutable
public final class BustedPlayerInfos {
	private final int _pos;
	private final String _name;
	
	public BustedPlayerInfos(int pos, String name) {
		_pos = pos;
		_name = name;
	}

	public int getPos() {
		return _pos;
	}

	public String getName() {
		return _name;
	}
}
