package com.oasisinfinity.oasispoker.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oasisinfinity.oasispoker.models.Card.FaceCard;
import com.oasisinfinity.oasispoker.models.Card.Suit;
import com.oasisinfinity.utils.ImmutablePair;
import com.oasisinfinity.utils.io.RuntimeTypeAdapterFactory;

import net.jcip.annotations.Immutable;

public final class PocketHands {
	public interface PocketHand {
		public Set<ImmutablePair<Card>> toPocketCards();
	}
	
	@Immutable
	private static abstract class PocketSet implements PocketHand {
		protected final FaceCard _mainCard;
		protected final FaceCard _minKickerCard;
		
		protected PocketSet(FaceCard mainCard, FaceCard minKickerCard) {
			if (minKickerCard.isBetterThan(mainCard))
				throw new IllegalArgumentException("The main card is stricly worse than the lowest kicker");
			
			_mainCard = Objects.requireNonNull(mainCard);
			_minKickerCard = Objects.requireNonNull(minKickerCard);
		}

		@Override
		public int hashCode() {
			int result = 1;
			result = 31 * result + _mainCard.hashCode();
			result = 31 * result + _minKickerCard.hashCode();
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (!(obj instanceof PocketSet))
				return false;
			
			PocketSet other = (PocketSet) obj;
			
			return _mainCard == other._mainCard && _minKickerCard == other._minKickerCard;
		}
		
		@Override
		public String toString() {
			return _mainCard.name() + _minKickerCard.name() + "+";
		}
	}
	
	@Immutable
	private static final class PocketPair implements PocketHand {
		private final FaceCard _minCard;
		
		//For Gson serialization
		@SuppressWarnings("unused")
		private final String _type = "PocketPair";
		
		private PocketPair(FaceCard minCard) {
			_minCard = Objects.requireNonNull(minCard);
		}
		
		@Override
		public Set<ImmutablePair<Card>> toPocketCards() {
			Set<ImmutablePair<Card>> pocketCards = new HashSet<>();
			Suit[] suits = Suit.values();
			FaceCard[] faceCards = FaceCard.values();
			
			for (int i = _minCard.ordinal(); i >= 0; --i) {
				FaceCard pairCard = faceCards[i];
				
				for (int j = 0; j < 3; ++j) {
					for (int k = j + 1; k < 4; ++k)
						pocketCards.add(createPocketCards(pairCard, pairCard, suits[j], suits[k]));
				}
			}
			
			return pocketCards;
		}

		@Override
		public int hashCode() {
			return 31 + _minCard.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			
			if (!(obj instanceof PocketPair))
				return false;

			return _minCard == ((PocketPair) obj)._minCard;
		}
		
		@Override
		public String toString() {
			return _minCard.name() + _minCard.name() + "+";
		}
	}
	
	@Immutable
	private static final class PocketSuited extends PocketSet {
		//For Gson serialization
		@SuppressWarnings("unused")
		private final String _type = "PocketSuited";
				
		private PocketSuited(FaceCard mainCard, FaceCard minKickerCard) {
			super(mainCard, minKickerCard);
		}
		
		@Override
		public Set<ImmutablePair<Card>> toPocketCards() {
			Set<ImmutablePair<Card>> pocketCards = new HashSet<>();
			Suit[] suits = Suit.values();
			FaceCard[] faceCards = FaceCard.values();
			
			for (int i = _minKickerCard.ordinal(); i > _mainCard.ordinal(); --i) {
				for (int j = 0; j < 4; ++j) {
					Suit suit = suits[j];
					pocketCards.add(createPocketCards(_mainCard, faceCards[i], suit, suit));
				}
			}
			
			return pocketCards;
		}
		
		@Override
		public String toString() {
			return super.toString() + "s";
		}
	}
	
	@Immutable
	private static final class PocketOffSuit extends PocketSet {
		//For Gson serialization
		@SuppressWarnings("unused")
		private final String _type = "PocketOffSuit";
		
		private PocketOffSuit(FaceCard mainCard, FaceCard minKickerCard) {
			super(mainCard, minKickerCard);
		}
		
		@Override
		public Set<ImmutablePair<Card>> toPocketCards() {
			Set<ImmutablePair<Card>> pocketCards = new HashSet<>();
			Suit[] suits = Suit.values();
			FaceCard[] faceCards = FaceCard.values();
			
			for (int i = _minKickerCard.ordinal(); i >= _mainCard.ordinal(); --i) {
				for (int j = 0; j < 4; ++j) {
					for (int k = 0; k < 4; ++k) {
						if (j != k)
							pocketCards.add(createPocketCards(_mainCard, faceCards[i], suits[j], suits[k]));
					}
				}
			}
			
			return pocketCards;
		}
		
		@Override
		public String toString() {
			return super.toString() + "o";
		}
	}
	
	private PocketHands() { /* Empty */ }
	
	public static Gson getGson() {
		RuntimeTypeAdapterFactory<PocketHand> typeAdapterFactory = 
				RuntimeTypeAdapterFactory.of(PocketHand.class, "_type");
		
		typeAdapterFactory.registerSubtype(PocketPair.class)
						  .registerSubtype(PocketSuited.class)
		                  .registerSubtype(PocketOffSuit.class);
		
		return new GsonBuilder().registerTypeAdapterFactory(typeAdapterFactory).create();
	}
	
	public static PocketHand newPocketPairs(FaceCard minCard) {
		return new PocketPair(minCard);
	}
	
	public static PocketHand newSuitedPocketHands(FaceCard mainCard, FaceCard minKickerCard) {
		return new PocketSuited(mainCard, minKickerCard);
	}
	
	public static PocketHand newOffSuitPocketHands(FaceCard mainCard, FaceCard minKickerCard) {
		return new PocketOffSuit(mainCard, minKickerCard);
	}
	
	/*private static void addAllOffSuitPocketCards(Set<ImmutablePair<Card>> container, FaceCard mainCard, FaceCard kicker, Suit[] suits) {
		for (int i = 0; i < 3; ++i) {
			for (int j = i + 1; j < 4; ++j)
				container.add(createPocketCards(mainCard, kicker, suits[i], suits[j]));
		}
	}*/
	
	private static ImmutablePair<Card> createPocketCards(FaceCard main, FaceCard kicker, Suit mainSuit, Suit kickerSuit) {
		return new ImmutablePair<Card>(
				new Card(main, mainSuit),
				new Card(kicker, kickerSuit));
	}
}
