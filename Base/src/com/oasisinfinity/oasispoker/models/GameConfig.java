package com.oasisinfinity.oasispoker.models;

import net.jcip.annotations.Immutable;
import net.jcip.annotations.NotThreadSafe;

@Immutable
public final class GameConfig {
	public static final int INFINITE_DUR = Integer.MAX_VALUE;
	
	private final int _humanPlayersCount;
	private final int _botsCount;
	private final int _initialCash;
	private final int _initialSB;
	private final int _initialAntes;
	private final int _blindsIncreaseFrequency;
	private final int _antesIncreaseFrequency; 
	private final int _blindsIncreaseMultiplier;
	private final int _antesIncreaseMultiplier;
	private final int _antesDelay;
	private final int _playingTime;
	private final int _maxAfkDuration;
	
	public GameConfig(int humanPlayersCount, int botsCount, int initialCash, int initialSB, int initialAntes, int blindsIncreaseFrequency, int antesIncreaseFrequency, 
			int blindsIncreaseMultiplier, int antesIncreaseMultiplier, int antesDelay, int playingTime, int maxAfkDuration) {
		_humanPlayersCount = humanPlayersCount;
		_botsCount = botsCount;
		_initialCash = initialCash;
		_initialSB = initialSB;
		_initialAntes = initialAntes;
		_blindsIncreaseFrequency = blindsIncreaseFrequency;
		_antesIncreaseFrequency = antesIncreaseFrequency;
		_blindsIncreaseMultiplier = blindsIncreaseMultiplier;
		_antesIncreaseMultiplier = antesIncreaseMultiplier;
		_antesDelay = antesDelay;
		_playingTime = playingTime;
		_maxAfkDuration = maxAfkDuration;
	}

	public int getTotalPlayersCount() {
		return _humanPlayersCount + _botsCount;
	}
	
	public int getHumanPlayersCount() {
		return _humanPlayersCount;
	}

	public int getBotsCount() {
		return _botsCount;
	}

	public int getInitialCash() {
		return _initialCash;
	}

	public int getInitialSmallBlind() {
		return _initialSB;
	}

	public int getInitialAntes() {
		return _initialAntes;
	}

	public int getBlindsIncreaseFrequency() {
		return _blindsIncreaseFrequency;
	}

	public int getAntesIncreaseFrequency() {
		return _antesIncreaseFrequency;
	}

	public int getBlindsIncreaseMultiplier() {
		return _blindsIncreaseMultiplier;
	}

	public int getAntesIncreaseMultiplier() {
		return _antesIncreaseMultiplier;
	}

	public int getAntesDelay() {
		return _antesDelay;
	}
	
	public int getPlayingTime() {
		return _playingTime;
	}
	
	public int getMaxAfkDuration() {
		return _maxAfkDuration;
	}
	
	@NotThreadSafe
	public static class Builder {
		private int _humanPlayersCount = 1;
		private int _botsCount = 7;
		private int _initialCash = 1000;
		private int _initialSB = 5;
		private int _initialAntes = 0;
		private int _blindsIncreaseFrequency = 10;
		private int _antesIncreaseFrequency = 10;
		private int _blindsIncreaseMultiplier = 2;
		private int _antesIncreaseMultiplier = 2;
		private int _antesDelay = 10;
		private int _playingTime = 15;
		private int _maxAfkDuration = INFINITE_DUR;
		
		public Builder setHumanPlayersCount(int value) {
			_humanPlayersCount = value;
			return this;
		}
		
		public Builder setBotsCount(int value) {
			_botsCount = value;
			return this;
		}
		
		public Builder setInitialCash(int value) {
			_initialCash = value;
			return this;
		}
		
		public Builder setInitialSmallBlind(int value) {
			_initialSB = value;
			return this;
		}
		
		public Builder setInitialAntes(int value) {
			_initialAntes = value;
			return this;
		}
		
		public Builder setBlindsIncreaseFrequency(int value) {
			_blindsIncreaseFrequency = value;
			return this;
		}
		
		public Builder setAntesIncreaseFrequency(int value) {
			_antesIncreaseFrequency = value;
			return this;
		}
		
		public Builder setBlindsIncreaseMultiplier(int value) {
			_blindsIncreaseMultiplier = value;
			return this;
		}
		
		public Builder setAntesIncreaseMultiplier(int value) {
			_antesIncreaseMultiplier = value;
			return this;
		}
		
		public Builder setAntesDelay(int value) {
			_antesDelay = value;
			return this;
		}
		
		public Builder setPlayingTime(int value) {
			_playingTime = value;
			return this;
		}
		
		public Builder setMaxAfkDuration(int value) {
			_maxAfkDuration = value;
			return this;
		}
		
		public GameConfig build() {
			return new GameConfig(_humanPlayersCount, _botsCount, _initialCash, _initialSB, _initialAntes, _blindsIncreaseFrequency, _antesIncreaseFrequency, 
					_blindsIncreaseMultiplier, _antesIncreaseMultiplier, _antesDelay, _playingTime, _maxAfkDuration);
		}
	}
}
