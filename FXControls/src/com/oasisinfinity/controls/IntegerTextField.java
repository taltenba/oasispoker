package com.oasisinfinity.controls;

import java.text.NumberFormat;
import java.util.Optional;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.NumberStringConverter;

public class IntegerTextField extends TextField {
	private final IntegerProperty _min;
	private final IntegerProperty _max;
	private final ReadOnlyObjectWrapper<Integer> _value;
	private final ReadOnlyBooleanWrapper _isValid;
	
	public IntegerTextField() {
		this("0", Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	public IntegerTextField(String text) {
		this(text, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	public IntegerTextField(int min, int max) {
		this(String.valueOf(min), min, max);
	}
	
	public IntegerTextField(String text, int min, int max) {
		super(text);
		
		if (min > max)
			throw new IllegalArgumentException("min > max");
		
		_min = new SimpleIntegerProperty(this, "min", min);
		_max = new SimpleIntegerProperty(this, "max", max);
		_value = new ReadOnlyObjectWrapper<Integer>(text.equals("") ? null : Integer.valueOf(text));
		_isValid = new ReadOnlyBooleanWrapper(validateValue());
		
		_value.addListener((observable, oldValue, newValue) -> _isValid.set(validateValue()));
		
		setTextFormatter(new TextFormatter<Number>(new NumberStringConverter(NumberFormat.getInstance())));
		
		textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.equals(""))
				_value.set(null);
			else
				_value.set(((Number) getTextFormatter().getValueConverter().fromString(newValue)).intValue());
		});
	}
	
	@Override
    public void replaceText(int start, int end, String text) {
        if (validateText(text))
            super.replaceText(start, end, text);
    }

    @Override
    public void replaceSelection(String text) {
        if (validateText(text))
            super.replaceSelection(text);
    }

    private boolean validateText(String text) {
    	return text.matches("[0-9]*");
    }
    
	private boolean validateValue() {
		Integer value = _value.get();
		
		if (value == null)
			return false;
		
		int intValue = value;
		
		return intValue >= _min.get() && intValue <= _max.get();
	}

    public ReadOnlyObjectProperty<Integer> valueProperty() {
    	return _value.getReadOnlyProperty();
    }
    
    public IntegerProperty minProperty() {
    	return _min;
    }
    
    public IntegerProperty maxProperty() {
    	return _max;
    }
    
    public ReadOnlyBooleanProperty isValidProperty() {
    	return _isValid.getReadOnlyProperty();
    }
    
    public int getMin() {
    	return _min.get();
    }
    
    public int getMax() {
    	return _max.get();
    }
   
	public Optional<Integer> getValue() {
    	return Optional.ofNullable(_value.get());
    }
	
	public boolean isValid() {
		return _isValid.get();
	}
	
	public boolean isSet() {
		return _value.get() != null;
	}
	
    public void setMin(int min) {
    	if (min > _max.get())
    		throw new IllegalArgumentException("min > max");
    	
    	_min.set(min);
    }
    
    public void setMax(int max) {
    	if (max < _min.get())
    		throw new IllegalArgumentException("max < min");
    	
    	_max.set(max);
    }
    
    public void setValue(int value) {
    	_value.set(value);
    	setText(String.valueOf(value));
    }
}
