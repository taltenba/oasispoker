package com.oasisinfinity.controls;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;

public class NoDelayTooltip extends Tooltip {
	public NoDelayTooltip(Node parent) {
		this(parent, "");
	}

	public NoDelayTooltip(Node parent, String text) {
		super(text);

		if (parent == null)
			return;
		
		parent.setOnMouseEntered(e -> {
			Point2D p = parent.localToScreen(parent.getLayoutBounds().getMaxX(), parent.getLayoutBounds().getMaxY());
	        show(parent, p.getX(), p.getY());
		});
		
		parent.setOnMouseExited(e -> hide());
	}
}
