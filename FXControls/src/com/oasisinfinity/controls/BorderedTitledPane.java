package com.oasisinfinity.controls;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * https://gist.github.com/jewelsea/2838292
 * @author jewelsea
 * @author Modified by : OasisInfinity
 */
public class BorderedTitledPane extends StackPane {
	private final StringProperty _title;
	
	public BorderedTitledPane() {
		this("");
	}
	
	public BorderedTitledPane(String title) {
		this(title, new Node[0]);		
	}
	
	public BorderedTitledPane(String title, Node... content) {
		super();
		
		_title = new SimpleStringProperty(this, "title", title);
		
		Label titleLabel = new Label();	
		titleLabel.textProperty().bind(_title);
		titleLabel.getStyleClass().add("bordered-titled-title");
		
		StackPane.setAlignment(titleLabel, Pos.TOP_CENTER);
		
		for (Node n : content)
			n.getStyleClass().add("bordered-titled-content");
		
		VBox contentPane = new VBox();		
		contentPane.getChildren().addAll(content);

		getStylesheets().add(BorderedTitledPane.class.getResource("/css/BorderedTitledPane.css").toExternalForm());
		getStyleClass().add("bordered-titled-border");
		getChildren().addAll(content);
		getChildren().addAll(titleLabel);
	}
	
	public StringProperty getTitleProperty() {
		return _title;
	}
  
	public String getTitle() {
		return _title.get();
	}
	
	public void setTitle(String title) {
		_title.set(title);
	}
}
