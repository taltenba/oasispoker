package com.oasisinfinity.controls;

import java.text.NumberFormat;
import java.text.ParseException;

import javafx.collections.ObservableList;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

public class AutoCommitSpinner extends Spinner<Integer> {
	public AutoCommitSpinner() {
        super();
        init();
    }

    public AutoCommitSpinner(int min, int max, int initialValue) {
        super(min, max, initialValue);
        init();
    }

    public AutoCommitSpinner(int min, int max, int initialValue, int amountToStepBy) {
        super(min, max, initialValue, amountToStepBy);
        init();
    }

    public AutoCommitSpinner(double min, double max, double initialValue) {
        super(min, max, initialValue);
        init();
    }

    public AutoCommitSpinner(double min, double max, double initialValue, double amountToStepBy) {
        super(min, max, initialValue, amountToStepBy);
        init();
    }

    public AutoCommitSpinner(ObservableList<Integer> items) {
        super(items);
        init();
    }

    public AutoCommitSpinner(SpinnerValueFactory<Integer> valueFactory) {
        super(valueFactory);
        init();
    }
    
    private void init() { 	
    	getEditor().setTextFormatter(new TextFormatter<Number>(new NumberStringConverter(NumberFormat.getInstance())));		
    	
    	valueFactoryProperty().addListener((observable, oldValue, newValue) -> {
    		newValue.setConverter(new FormattedIntegerStringConverter());
    	});
    	
    	if (getValueFactory() != null)
    		getValueFactory().setConverter(new FormattedIntegerStringConverter());
    	
    	addListeners();
    }

    private void addListeners() {
        getEditor().setOnKeyReleased(event -> {
    		String newChar = event.getText();
    		
    		if (event.isShortcutDown() || newChar.equals(""))
    			return;
    		
        	try {
	        	Integer.parseInt(newChar);
	        } catch (NumberFormatException e) {
	        	TextField editor = getEditor();
	        	String text = editor.getText();
	        	
	            editor.setText(text.replace(newChar, ""));;
	        }
        });
        
        focusedProperty().addListener((observable, oldValue, newValue) -> commitEditorText());
    }

    protected void commitEditorText() {
        if (!isEditable()) return;
        
        String text = getEditor().getText();
        SpinnerValueFactory<Integer> valueFactory = getValueFactory();
        
        if (valueFactory != null) {
    		StringConverter<Integer> converter = valueFactory.getConverter();
            
        	try {
                Integer value = converter.fromString(text);
                
                if (value == null) {
                	getEditor().setText(converter.toString(getValue()));
                	return;
                }
                
                valueFactory.setValue(value);
        	} catch (NumberFormatException e) {
        		getEditor().setText(converter.toString(getValue()));
        	}
        }
    }
    
    private static class FormattedIntegerStringConverter extends StringConverter<Integer> {
    	private final NumberFormat _format;
    	
    	public FormattedIntegerStringConverter() {
    		this(NumberFormat.getInstance());
    	}
    	
    	public FormattedIntegerStringConverter(NumberFormat format) {
    		super();
    		
    		_format = format;
    	}
    	
		@Override
		public Integer fromString(String string) throws NumberFormatException {
			try {
				if (string.isEmpty())
					return null;
				
				return _format.parse(string).intValue();
			} catch (ParseException e) {
				NumberFormatException ex = new NumberFormatException();
				ex.initCause(e);
				
				throw ex;
			}
		}

		@Override
		public String toString(Integer value) {
			return _format.format(value);
		}
    }
}
