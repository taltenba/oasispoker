package com.oasisinfinity.oasispoker.controllers;

import java.util.Map;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.models.AppInfos;
import com.oasisinfinity.oasispoker.models.AppInfos.AppName;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

public class ChangelogDialogController extends BaseController {
	@FXML private ToggleButton _clientToggle;
	@FXML private ToggleButton _serverToggle;
	@FXML private ToggleButton _launcherToggle;
	@FXML private ToggleGroup _appPartsGroup;
	@FXML private TextArea _changelogArea;
	@FXML private Button _closeButton;
	
	private Stage _dialogStage;
	
	public ChangelogDialogController() {
		super();
	}
	
	@FXML
	@Override
	protected void initialize() {
		_appPartsGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue == null)
				oldValue.setSelected(true);
			else
				_changelogArea.setText((String) newValue.getUserData());
		});
		
		_closeButton.setOnMouseClicked(e -> _dialogStage.close());
	}

	@Override
	public void setMainApp(BaseApp mainApp) {
		super.setMainApp(mainApp);
		
		Map<AppName, AppInfos> appInfos = getMainApp().getAppInfos();
		
		_clientToggle.setUserData(appInfos.get(AppName.Client).getChangelog());
		_serverToggle.setUserData(appInfos.get(AppName.Server).getChangelog());
		_launcherToggle.setUserData(appInfos.get(AppName.Launcher).getChangelog());
		
		_appPartsGroup.selectToggle(_clientToggle);
	}
	
	public void setDialogStage(Stage dialogStage) {
		_dialogStage = dialogStage;
	}
}
