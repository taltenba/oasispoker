package com.oasisinfinity.oasispoker.controllers;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import javax.mail.MessagingException;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.GoogleMail;
import com.oasisinfinity.utils.GoogleMail.Attachement;
import com.oasisinfinity.utils.PlatformUtil;
import com.oasisinfinity.utils.io.ZipUtil;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class FeedbackDialogController extends BaseController {
	private static final String ATTACHEMENT_ZIP_PATH = System.getProperty("java.io.tmpdir") + "/OasisPoker_Logs.zip";
	
	@FXML private ComboBox<String> _appComboBox;
	@FXML private ComboBox<String> _typeComboBox;
	@FXML private ComboBox<String> _priorityComboBox;
	@FXML private TextField _emailField;
	@FXML private TextArea _msgArea;
	@FXML private CheckBox _includeLogsCheckBox;
	@FXML private Button _sendButton;
	@FXML private Button _closeButton;
	
	private Stage _dialogStage;
	
	public FeedbackDialogController() {
		super();
	}

	@FXML
	@Override
	protected void initialize() {
		_typeComboBox.valueProperty().addListener((observable, oldValue, newValue) -> { 
					boolean enable = newValue.equals("Bogues/Probl�mes");
					
					_includeLogsCheckBox.setDisable(!enable);
					_includeLogsCheckBox.setSelected(enable);
		});
		
		_msgArea.textProperty().addListener((observable, oldValue, newValue) -> _sendButton.setDisable(newValue.isEmpty()));
		
		_sendButton.setOnMouseClicked(e -> sendFeedback());
		_closeButton.setOnMouseClicked(e -> _dialogStage.close());
	}
	
	public void setDialogStage(Stage dialogStage) {
		_dialogStage = dialogStage;
	}
	
	private void sendFeedback() {
		final String[] msg = composeMsg();
		final boolean includeLogs = !_includeLogsCheckBox.isDisable() && _includeLogsCheckBox.isSelected();
		
		BasicThreadFactory.createNewThread(() -> {
			Attachement attachement = null;
			
			if (includeLogs)
				attachement = prepareAttachement();
			
			try {
				GoogleMail.send(BaseApp.REPORT_MAIL_ACCOUNT, new String[] {BaseApp.REPORT_MAIL_ACCOUNT.getAddress()}, msg[0], msg[1], attachement == null ? null : new Attachement[] {attachement});
			} catch (MessagingException e) {
				BaseApp.logException(Level.SEVERE, e);
				
				Platform.runLater(() ->
					BaseApp.ALERT_UTIL.showErrorMsg("Envoi impossible", "Une erreur est survenue pendant l'envoi du message. Consultez les logs pour plus de d�tails"));
				
				return;
			} finally {
				if (includeLogs)
					new File(ATTACHEMENT_ZIP_PATH).delete();
			}
		}, "SendFeedbackThread", BaseApp.FX_CRASH_HANDLER).start();
		
		_dialogStage.close();
	}
	
	private String[] composeMsg() {
		String app = _appComboBox.getValue();
		String type = _typeComboBox.getValue();
		String priority = _priorityComboBox.getValue();
		
		String subject = "OasisPoker - " + app + " - " + type + " - " + priority;
		String body = "Application : OasisPoker - " + app + "\n" +
					"Type : " + type + "\n" +
					"Priority : " + priority + "\n" +
					"OS Infos : " + PlatformUtil.getOSInfos() + "\n" +
					"Sender email : " + _emailField.getText() + "\n" +
					"Message :\n" +
					_msgArea.getText();
		
		return new String[] {subject, body};
	}
	
	private Attachement prepareAttachement() {
		File logsDir = new File(BaseApp.LOGS_OUTPUT_PATH);
		
		if (!logsDir.exists())
			return null;
		
		try {
			ZipUtil.zip(logsDir, new File(ATTACHEMENT_ZIP_PATH), ".lck");
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			return null;
		}

		return new Attachement("Logs.zip", ATTACHEMENT_ZIP_PATH);
	}
}
