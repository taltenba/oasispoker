package com.oasisinfinity.oasispoker.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpTransport;
import com.oasisinfinity.controls.BorderedTitledPane;
import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.LauncherApp;
import com.oasisinfinity.oasispoker.models.AppInfos;
import com.oasisinfinity.oasispoker.models.AppInfos.AppName;
import com.oasisinfinity.oasispoker.models.SubPane;
import com.oasisinfinity.oasispoker.models.User;
import com.oasisinfinity.utils.ArrayUtil;
import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.StringUtil;
import com.oasisinfinity.utils.io.IOUtil;
import com.oasisinfinity.utils.javafx.AnimUtil;
import com.oasisinfinity.utils.javafx.DialogUtil;
import com.oasisinfinity.utils.javafx.DialogUtil.StageInfos;
import com.oasisinfinity.utils.javafx.NodeUtil;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LauncherMainController extends BaseController {
	private static final HttpTransport HTTP_TRANSPORT;
	private static final String DOWNLOADED_FILES_PATH = System.getProperty("java.io.tmpdir");
	private static final String APP_INFOS_FILE_ID = "0B72NafoPXqmVZzcxckhTVXEtMjg";
	private static final String[] JAR_FILES_IDS = new String[] {"0B72NafoPXqmVU25mUWlXbEYtVVk", "0B72NafoPXqmVUVI4Mm5LVDFtX3M", "0B72NafoPXqmVMHltZUhWV3htVEk", "0B72NafoPXqmVZjhyWU9pb0tPY00"};
 	private static final String[] JAR_FILES_NAMES = new String[] {"Updater.jar", "Client.jar", "Server.jar", "Launcher.jar"};
	private static final int MIN_ACCOUNT_NAME_LENGHT = 1;
	private static final int MAX_ACCOUNT_NAME_LENGTH = 15;
	
	static {
		HttpTransport tmp = null;
		
        try {
            tmp = GoogleNetHttpTransport.newTrustedTransport();
        } catch (IOException | GeneralSecurityException e) {
            e.printStackTrace();
        }
        
        HTTP_TRANSPORT = tmp;
    }
	
	public enum PaneName {
		NewAccount("Nouveau compte"),
		AccountsManagement("Gestion des comptes");
		
		private final String _name;
		
		private PaneName(String name) {
			_name = name;
		}
		
		@Override
		public String toString() { return _name; }
	}
	
	@FXML private ImageView _titleImageView;
	@FXML private Label _updateStatusLabel;
	@FXML private Label _versionLabel;
	@FXML private Button _changelogButton;
	@FXML private Button _feedbackButton;
	@FXML private Button _startServerButton;
	@FXML private Button _createAccountButton;
	@FXML private Button _newAccountButton;
	@FXML private Button _accountsManagementButton;
	@FXML private Button _closeNewAccountPaneButton;
	@FXML private Button _closeAccountsManagementPaneButton;
	@FXML private Button _renameAccountButton;
	@FXML private Button _deleteAccountButton;
	@FXML private Button _connectionButton;
	@FXML private TextField _newAccountNameField;
	@FXML private TextField _newAccountNameConfirmField;
	@FXML private TextField _managementAccountNameField;
	@FXML private TextField _renameAccountNameField;
	@FXML private TextField _renameAccountNameConfirmField;
	@FXML private TextField _connectionAccountNameField;
	@FXML private BorderPane _rootPane;
	@FXML private HBox _panesBox;
	@FXML private BorderedTitledPane _updatePane;
	@FXML private BorderedTitledPane _connectionPane;
	
	private final Map<AppName, Label> _appStatusLabels;
	private final List<SubPane> _subPanes;
	
	public LauncherMainController() {
		super();
		
		_appStatusLabels = new HashMap<>();
		_subPanes = new ArrayList<>();
	}

	@FXML
	@Override
	protected void initialize() {
		for (AppName app : AppName.values()) {
			Node label = _updatePane.lookup(StringUtil.getVariableName("#_", app.name(), "StatusLabel"));
			
			if (label != null)
				_appStatusLabels.put(app, (Label) label);
		}
		
		for (PaneName name : PaneName.values()) {
			BorderedTitledPane pane = (BorderedTitledPane) _panesBox.lookup(StringUtil.getVariableName("#_", name.name(), "Pane"));
			pane.setTitle(name.toString());
			
			_subPanes.add(new SubPane(pane));
		}
		
		_titleImageView.fitHeightProperty().bind(_rootPane.heightProperty().divide(3));
		_updatePane.setTitle("Mise � jour");
		_connectionPane.setTitle("Connexion");
		
		_newAccountButton.setOnMouseClicked(e -> showPane(PaneName.NewAccount));
		_accountsManagementButton.setOnMouseClicked(e -> showPane(PaneName.AccountsManagement));
		_closeNewAccountPaneButton.setOnMouseClicked(e -> hidePane(PaneName.NewAccount));
		_closeAccountsManagementPaneButton.setOnMouseClicked(e -> hidePane(PaneName.AccountsManagement));
		
		NodeUtil.addTextLimiter(_newAccountNameField, MAX_ACCOUNT_NAME_LENGTH);
		NodeUtil.addTextLimiter(_newAccountNameConfirmField, MAX_ACCOUNT_NAME_LENGTH);
		NodeUtil.addTextLimiter(_managementAccountNameField, MAX_ACCOUNT_NAME_LENGTH);
		NodeUtil.addTextLimiter(_renameAccountNameField, MAX_ACCOUNT_NAME_LENGTH);
		NodeUtil.addTextLimiter(_renameAccountNameConfirmField, MAX_ACCOUNT_NAME_LENGTH);
		NodeUtil.addTextLimiter(_connectionAccountNameField, MAX_ACCOUNT_NAME_LENGTH);
		
		ChangeListener<? super String> newAccountNameListener = (observable, oldValue, newValue) ->
							_createAccountButton.setDisable(!areAccountNameFieldsValid(_newAccountNameField, _newAccountNameConfirmField));
		
		_newAccountNameField.textProperty().addListener(newAccountNameListener);
		_newAccountNameConfirmField.textProperty().addListener(newAccountNameListener);
		
		_managementAccountNameField.textProperty().addListener((observable, oldValue, newValue) -> 
							_deleteAccountButton.setDisable(!areAccountNameFieldsValid(_managementAccountNameField)));
		
		ChangeListener<? super String> managementAccountNameListener = (observable, oldValue, newValue) ->
							_renameAccountButton.setDisable(!areAccountNameFieldsValid(_managementAccountNameField, _renameAccountNameField, _renameAccountNameConfirmField));
				
		_managementAccountNameField.textProperty().addListener(managementAccountNameListener);
		_renameAccountNameField.textProperty().addListener(managementAccountNameListener);
		_renameAccountNameConfirmField.textProperty().addListener(managementAccountNameListener);
		
		_connectionAccountNameField.textProperty().addListener((observable, oldValue, newValue) -> 
							_connectionButton.setDisable(!areAccountNameFieldsValid(_connectionAccountNameField)));
		
		_createAccountButton.setOnMouseClicked(e -> createAccount());
		_deleteAccountButton.setOnMouseClicked(e -> deleteAccount());
		_renameAccountButton.setOnMouseClicked(e -> renameAccount());
		_connectionButton.setOnAction(e -> connectToAccount());
		
		_startServerButton.setOnMouseClicked(e -> { 
			try {
				Runtime.getRuntime().exec("java -jar " + JAR_FILES_NAMES[AppName.Server.ordinal()]);
			} catch (Exception ex) {
				BaseApp.logException(Level.SEVERE, ex);
				BaseApp.ALERT_UTIL.showErrorMsg("Impossible de d�marrer le serveur", "Une erreur s'est produite pendant l'op�ration, consultez les logs pour plus de d�tails.");
			}
		});
		
		_changelogButton.setOnMouseClicked(e -> {
			try {
				 BaseApp mainApp = getMainApp();
				 
			     StageInfos<ChangelogDialogController> dialogInfos = DialogUtil.createDialogStage(
			    		 StageStyle.UNDECORATED,
			    		 LauncherApp.class.getResource("/fxml/ChangelogDialog.fxml"), 
			    		 "",
			    		 false, 
			    		 Modality.WINDOW_MODAL, 
			    		 mainApp.getStage());
			     
			     ChangelogDialogController controller = dialogInfos.getController();
			     Stage dialogStage = dialogInfos.getStage();
			     
			     controller.setMainApp(mainApp);
			     controller.setDialogStage(dialogStage);
			     
			     DialogUtil.showCentered(dialogStage);
			} catch (IOException ex) {
				BaseApp.logException(Level.SEVERE, ex);
				BaseApp.ALERT_UTIL.corruptedInstallation();
			}
		});
		
		_feedbackButton.setOnMouseClicked(e -> {
			try {
				StageInfos<FeedbackDialogController> dialogInfos = DialogUtil.createDialogStage(
						StageStyle.UNDECORATED, 
						LauncherApp.class.getResource("/fxml/FeedbackDialog.fxml"), 
						"", 
						false, 
						Modality.WINDOW_MODAL, 
						getMainApp().getStage());
				
				Stage dialogStage = dialogInfos.getStage();
				
				dialogInfos.getController().setDialogStage(dialogStage);
				
				DialogUtil.showCentered(dialogStage);
			} catch (IOException ex) {
				BaseApp.logException(Level.SEVERE, ex);
				BaseApp.ALERT_UTIL.corruptedInstallation();
			}
		});
	}
	
	private void createAccount() {
		String accountName = _newAccountNameField.getText();
		
		if (!accountName.equals(_newAccountNameConfirmField.getText())) {
			BaseApp.ALERT_UTIL.showMsg(AlertType.WARNING, "Cr�ation impossible", "Les noms de comptes sont diff�rents. Veuillez r�essayer.");
			return;
		}
		
		String accountFilePath = BaseApp.createAccountPath(accountName);
		
		if (Files.exists(Paths.get(accountFilePath))) {
			BaseApp.ALERT_UTIL.showMsg(AlertType.WARNING, "Cr�ation impossible", "Ce nom de compte n'est pas disponible.");
			return;
		}
		
		try {
			IOUtil.saveJson(accountFilePath, new User(accountName));
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.showErrorMsg("Cr�ation impossible", "Une erreur s'est produite pendant la cr�ation, consultez les logs pour plus de d�tails.");
			return;
		}
		
		_subPanes.get(PaneName.NewAccount.ordinal()).hide();
		_newAccountNameField.clear();
		_newAccountNameConfirmField.clear();
	}
	
	private void deleteAccount() {
		if (!BaseApp.ALERT_UTIL.showConfirmationMsg("Supprimer", "Voulez vraiment supprimer ce compte ?"))
			return;
		
		File accountFile = new File(BaseApp.createAccountPath(_managementAccountNameField.getText()));
		
		if (!accountFile.exists()) {
			BaseApp.ALERT_UTIL.showMsg(AlertType.WARNING, "Suppression impossible", "Ce compte n'existe pas.");
			return;
		}
		
		try {
			Files.delete(accountFile.toPath());
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.showErrorMsg("Suppression impossible", "Une erreur s'est produite pendant la suppression, consultez les logs pour plus de d�tails.");
			return;
		}
		
		_managementAccountNameField.clear();
	}
	
	private void renameAccount() {
		File accountFile = new File(BaseApp.createAccountPath(_managementAccountNameField.getText()));
		
		if (!accountFile.exists()) {
			BaseApp.ALERT_UTIL.showMsg(AlertType.WARNING, "Op�ration impossible", "Ce compte n'existe pas.");
			return;
		}
		
		String newAccountName = _renameAccountNameField.getText();
		
		if (!newAccountName.equals(_renameAccountNameConfirmField.getText())) {
			BaseApp.ALERT_UTIL.showMsg(AlertType.WARNING, "Op�ration impossible", "Les nouveaux noms de comptes sont diff�rents. Veuillez r�essayer.");
			return;
		}
		
		Path newAccountFilePath = Paths.get(BaseApp.createAccountPath(newAccountName));
		
		if (Files.exists(newAccountFilePath)) {
			BaseApp.ALERT_UTIL.showMsg(AlertType.WARNING, "Op�ration impossible", "Ce nom de compte n'est pas disponible.");
			return;
		}
		
		try {
			Files.move(accountFile.toPath(), newAccountFilePath);
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.showErrorMsg("Op�ration impossible", "Une erreur s'est produite pendant l'op�ration, consultez les logs pour plus de d�tails.");
			return;
		}
		
		_managementAccountNameField.clear();
		_renameAccountNameField.clear();
		_renameAccountNameConfirmField.clear();
	}
	
	private void connectToAccount() {
		String accountName = _connectionAccountNameField.getText();
		
		if (!Files.exists(Paths.get(BaseApp.createAccountPath(accountName)))) {
			BaseApp.ALERT_UTIL.showMsg(AlertType.WARNING, "Connexion impossible", "Ce compte n'existe pas.");
			return;
		}
		
		try {
			Runtime.getRuntime().exec("java -jar " + JAR_FILES_NAMES[AppName.Client.ordinal()] + " --" + BaseApp.USER_NAME_ARG_KEY + "=" + accountName);
			Platform.exit();
		} catch (IOException e) {
			BaseApp.logException(Level.SEVERE, e);
			BaseApp.ALERT_UTIL.showErrorMsg("Impossible de d�marrer le client", "Une erreur s'est produite pendant l'op�ration, consultez les logs pour plus de d�tails.");
			return;
		}
		
		_connectionAccountNameField.clear();
	}

	@Override
	public void setMainApp(BaseApp mainApp) {
		super.setMainApp(mainApp);
		
		_versionLabel.setText("v" + getMainApp().getVersion(AppName.Launcher) + " - by OasisInfinity");
		
		checkForUpdate();
	}
	
	private void download(String fileId, String outputPath, MediaHttpDownloaderProgressListener progressListener) throws IOException {
		try (OutputStream out = new FileOutputStream(outputPath)) {
			MediaHttpDownloader downloader = new MediaHttpDownloader(HTTP_TRANSPORT, null);
			downloader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE * 2);
			downloader.setProgressListener(progressListener);
			downloader.download(new GenericUrl("https://docs.google.com/uc?authuser=0&id=" + fileId + "&export=downloadurl"), out);
		}
	}
	
	private void checkForUpdate() {
		BasicThreadFactory.createNewThread(() -> {
			deleteDownloadedFiles(); // Delete old downloaded files if exist
			
			boolean closeAfterDownload = false;
			
			try {
				if (HTTP_TRANSPORT == null) {
					BaseApp.logMsg(Level.WARNING, "HTTP_TRANSPORT is null");
					return;
				}
				
				download(APP_INFOS_FILE_ID, getDownloadedFilePath(BaseApp.APP_INFOS_FILE_NAME), null);
				
				Map<AppName, AppInfos> oldInfos = getMainApp().getAppInfos();
				Map<AppName, AppInfos> newInfos = AppInfos.appInfosListToMap(ArrayUtil.toArrayList(IOUtil.loadJsonFromFile(getDownloadedFilePath(BaseApp.APP_INFOS_FILE_NAME), AppInfos[].class)));
				List<AppInfos> infosToWrite = new ArrayList<>();
				boolean updated = false;
				
				for (AppName app : AppName.values()) {
					AppInfos upToDate = newInfos.get(app);
					
					if (oldInfos == null || !oldInfos.get(app).getVersion().equals(upToDate.getVersion())) {
						if (update(app, upToDate)) {
							updated = true;
							
							if (app == AppName.Launcher) {
								try {
									IOUtil.saveJson(LauncherApp.LAUNCHER_INFOS_TEMP_PATH, upToDate);
									closeAfterDownload = true;
								} catch (IOException e) {
									refreshStatusLabelText(app, "Erreur");
									BaseApp.logException(Level.SEVERE, e);
								}
							} else {
								infosToWrite.add(upToDate);
								continue;
							}
						}
					} else
						refreshStatusLabelText(app, "� jour");
					
					infosToWrite.add(oldInfos.get(app));
				}
				
				if (updated) {
					IOUtil.saveJson(BaseApp.APP_INFOS_FILE_NAME, infosToWrite);
					
					if (!closeAfterDownload)	
						getMainApp().reloadAppInfos();
				}
			} catch (IOException e) {
				BaseApp.logException(Level.SEVERE, e);
				Platform.runLater(() -> _updateStatusLabel.setText("Erreur"));
			} finally {
				deleteDownloadedFiles(JAR_FILES_NAMES[AppName.Launcher.ordinal()]);
				
				if (closeAfterDownload)
					Platform.runLater(() -> Platform.exit());
				else
					Platform.runLater(() -> updateChecksCompleted());
			}
		}, "UpdateThread", BaseApp.FX_CRASH_HANDLER).start();
	}
	
	private boolean update(AppName app, AppInfos newInfos) {
		try {
			int appOrdinal = app.ordinal();
			String jarName = JAR_FILES_NAMES[appOrdinal];
			
			refreshStatusLabelText(app, String.format("%,.2f %%", 0.0));
			
			download(JAR_FILES_IDS[appOrdinal], getDownloadedFilePath(jarName), (downloader) -> {
				switch (downloader.getDownloadState()) {
				    case MEDIA_IN_PROGRESS:
				    	refreshStatusLabelText(app, String.format("%,.2f %%", downloader.getProgress() * 100.0));
					    break;
				    default:
				    	break;
				}
			});
			
			if (app != AppName.Launcher) {
				Files.move(Paths.get(getDownloadedFilePath(jarName)), Paths.get(jarName), StandardCopyOption.REPLACE_EXISTING);
				
				refreshStatusLabelText(app, "Mis � jour");
			} else {
				String updateJarName = JAR_FILES_NAMES[AppName.Updater.ordinal()];
				
				if (!new File(updateJarName).exists())
					update(AppName.Updater, null);
				
				Runtime.getRuntime().exec("java -jar " + updateJarName + " \"" + getDownloadedFilePath(jarName) + "\" \"" + new File(jarName).getAbsolutePath() + "\"");
			}
		} catch (IOException e) {
			BaseApp.logException(Level.WARNING, e);
			refreshStatusLabelText(app, "Erreur");
			return false;
		}
		
		return true;
	}
	
	private void updateChecksCompleted() {
		AnimUtil.switchNode(_updatePane, _connectionPane, SubPane.PANE_SWITCH_DURATION);

		_changelogButton.setDisable(false);
		_feedbackButton.setDisable(false);
		_startServerButton.setDisable(false);
	}
	
	private void refreshStatusLabelText(AppName app, String text) {
		Platform.runLater(() -> {
			Label label = _appStatusLabels.get(app);
			
			if (label != null)
				label.setText(text);
		});
	}
	
	private void showPane(PaneName name) {
		int paneIndex = name.ordinal();
		
		List<SubPane> toHide = new ArrayList<>();
		
		for (int i = 0; i < _subPanes.size(); ++i) {
			if (i != paneIndex)
				toHide.add(_subPanes.get(i));
		}
		
		SubPane.hideAll(toHide, e -> _subPanes.get(name.ordinal()).show());
	}
	
	private void hidePane(PaneName name) {
		_subPanes.get(name.ordinal()).hide();
	}
	
	private static void deleteDownloadedFiles() {
		deleteDownloadedFiles(null);
	}
	
	private static void deleteDownloadedFiles(String except) {
		new File(getDownloadedFilePath(BaseApp.APP_INFOS_FILE_NAME)).delete();
		
		for (String jarName : JAR_FILES_NAMES) {
			if (except == null || !jarName.equals(except))
				new File(getDownloadedFilePath(jarName)).delete();
		}
	}
	
	private static String getDownloadedFilePath(String fileName) {
		return DOWNLOADED_FILES_PATH + File.separator + fileName;
	}
	
	private static boolean areAccountNameFieldsValid(TextField... fields) {
		for (TextField field : fields) {
			String fieldText = field.getText();
			
			if (fieldText.length() < MIN_ACCOUNT_NAME_LENGHT || !fieldText.matches("[\\w\\p{L}]+"))
				return false;
		}
		
		return true;
	}
}
