package com.oasisinfinity.oasispoker;
	
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.oasisinfinity.oasispoker.models.AppInfos;
import com.oasisinfinity.utils.io.IOUtil;

import javafx.stage.Stage;

public class LauncherApp extends BaseApp {
	public static final String LAUNCHER_MAIN_SCENE = "LauncherMain";
	public static final String LAUNCHER_INFOS_TEMP_PATH = System.getProperty("java.io.tmpdir") + "/OasisPoker_Launcher_UpdateInfos.json";
	
	public LauncherApp() {
		super(LAUNCHER_MAIN_SCENE);
	}
	
	@Override
	public void start(Stage primaryStage) {
		super.start(primaryStage, "OasisPoker - Launcher", false);
	}
	
	@Override
	protected void loadScenes() throws IOException {
		loadScene(LAUNCHER_MAIN_SCENE);	
	}
	
	public static void main(String[] args) {
		if (args.length > 0 && args[0].equals(UpdaterApp.UPDATE_SUCCESS)) {
			try {
				AppInfos newUpdateInfos = IOUtil.loadJsonFromFile(LAUNCHER_INFOS_TEMP_PATH, AppInfos.class);
				AppInfos[] oldInfos = IOUtil.loadJsonFromFile(BaseApp.APP_INFOS_FILE_NAME, AppInfos[].class);
				
				for (int i = 0; i < oldInfos.length; ++i) {
					if (oldInfos[i].getName() == newUpdateInfos.getName())
						oldInfos[i] = newUpdateInfos;
				}
				
				IOUtil.saveJson(BaseApp.APP_INFOS_FILE_NAME, oldInfos);
				
				Files.delete(Paths.get(LAUNCHER_INFOS_TEMP_PATH));
			} catch (IOException | JsonSyntaxException | JsonIOException e) {
				BaseApp.logException(Level.SEVERE, e);
			}
		}
		
		launch(args);
	}
}
