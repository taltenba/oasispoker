package com.oasisinfinity.oasispoker.models;

import java.util.ArrayList;
import java.util.List;

import com.oasisinfinity.utils.javafx.AnimUtil;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class SubPane {
	public static final Duration PANE_SWITCH_DURATION = Duration.millis(500);
	
	private final Pane _pane;
	private boolean _isShowed;
	
	public SubPane(Pane pane) {
		_pane = pane;
		_isShowed = false;
	}
	
	public Pane getPane() {
		return _pane;
	}
	
	public boolean isShowed() {
		return _isShowed;
	}
	
	public void show() {
		if (_isShowed)
			return;
		
		AnimUtil.showNode(_pane, PANE_SWITCH_DURATION);
		_isShowed = true;
	}
	
	public void hide() {
		if (!_isShowed)
			return;
		
		AnimUtil.hideNode(_pane, PANE_SWITCH_DURATION, true);
		_isShowed = false;
	}
	
	private void setIsShowed(boolean isShowed) {
		_isShowed = isShowed;
	}
	
	public static void hideAll(List<SubPane> panes, EventHandler<ActionEvent> onFinished) {
		List<Node> nodes = new ArrayList<>();
		
		for (SubPane pane : panes) {
			if (pane.isShowed()) {
				nodes.add(pane.getPane());
				pane.setIsShowed(false);
			}
		}
		
		AnimUtil.hideNodesParallel(nodes, PANE_SWITCH_DURATION, true, onFinished);
	}
}
