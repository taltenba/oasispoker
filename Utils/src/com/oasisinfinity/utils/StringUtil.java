package com.oasisinfinity.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class StringUtil {
	private StringUtil() { /* Empty */ }

	public static boolean isValidIP(String s) {
		return s.matches("^([0-9]{1,3}\\.){3}[0-9]{1,3}$");
	}
	
	public static String getVariableName(String prefix, String name, String suffix) {
		name = name.substring(0, 1).toLowerCase() + (name.length() > 1 ? name.substring(1) : "");
		return prefix + name + suffix;
	}
	
	public static byte[] toSha256(String str) {
		try {
			 return MessageDigest.getInstance("SHA-256").digest(str.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static String toSha256Hex(String str) {
		return String.format("%064x", new BigInteger(1, toSha256(str)));
	}
	
	public static String stackTraceToString(Throwable t) {
		StringWriter sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
}
