package com.oasisinfinity.utils;

import java.math.BigDecimal;

public class MathUtil {

	private MathUtil() { /* Empty */ }
	
	/**
	 * Round faster than {@code round(float, int)}, but may be inaccurate and work only
	 * for "small" numbers and numbers of decimals
	 */
	public static float roundFast(float f, int nbrOfDecimals) {
		float pow = (float) Math.pow(10, nbrOfDecimals);
        return Math.round(f * pow) / pow;
    }
	
	/**
	 * Round faster than {@code round(double, int)}, but may be inaccurate and work only
	 * for "small" numbers and numbers of decimals
	 */
	public static double roundFast(double d, int nbrOfDecimals) {
        double pow = (double) Math.pow(10, nbrOfDecimals);
        return Math.round(d * pow) / pow;
    }
	
	public static float round(float f, int nbrOfDecimals) {
        BigDecimal bd = new BigDecimal(Float.toString(f));
        bd = bd.setScale(nbrOfDecimals, BigDecimal.ROUND_HALF_UP);
        
        return bd.floatValue();
    }
	
	public static double round(double d, int nbrOfDecimals) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(nbrOfDecimals, BigDecimal.ROUND_HALF_UP);
        
        return bd.doubleValue();
    }
	
	public static int floor(int n, int precision) {
		return n - (n % precision);
	}
	
	public static long floor(long n, long precision) {
		return n - (n % precision);
	}
	
	public static float floor(float n, float precision) {
		return n - (n % precision);
	}
	
	public static double floor(double n, double precision) {
		return n - (n % precision);
	}
}
