package com.oasisinfinity.utils.logging;

import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.mail.MessagingException;

import com.oasisinfinity.utils.GoogleMail;
import com.oasisinfinity.utils.GoogleMail.GoogleAccount;
import com.oasisinfinity.utils.PlatformUtil;
import com.oasisinfinity.utils.StringUtil;

public class AsyncFileLogger extends Logger implements ExceptionLogger {
	public static final int DEFAULT_FILE_SIZE_LIMIT = 100000;
	public static final int DEFAULT_MAX_FILES_COUNT = 2;
	
	private final AsyncFileHandler _handler;
	private final GoogleAccount _account;
	
	public AsyncFileLogger(String baseName) throws SecurityException, IOException {
		this(baseName, DEFAULT_FILE_SIZE_LIMIT, DEFAULT_MAX_FILES_COUNT, null);
	}
	
	public AsyncFileLogger(String baseName, GoogleAccount account) throws SecurityException, IOException {
		this(baseName, DEFAULT_FILE_SIZE_LIMIT, DEFAULT_MAX_FILES_COUNT, account);
	}
	
	/**
	 * Create a file logger which write logs asynchronously
	 * @param baseName The base name of the log file
	 * @param fileSizeLimit The maximum size of the log file (number of bytes)
	 * @param maxFilesCount The maximum number of log files
	 * @param account Account use for send stacktrace when the logException method is called. Set null if you don't want.
	 * @throws SecurityException
	 * @throws IOException
	 */
	public AsyncFileLogger(String baseName, int fileSizeLimit, int maxFilesCount, GoogleAccount account) throws SecurityException, IOException {
		super(baseName + "Logger", null);
		
		_account = account;
		
		_handler = new AsyncFileHandler(baseName + " - %g.log", fileSizeLimit, maxFilesCount, true);
		_handler.setFormatter(new SimpleFormatter());
		
		addHandler(_handler);
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> close(), "LoggerHandlersCloserHookThread"));
	}
	
	@Override
	public void log(Level lvl, String msg) {
		System.out.println(lvl.getName() + " : " + msg);
		
		super.log(lvl, msg);
	}
	
	public void logException(Level lvl, Throwable e) {
		logException(lvl, e, false);
	}
	
	public void logException(Level lvl, Throwable e, boolean sendReportEmail) {
		log(lvl, e.getMessage(), e);
		e.printStackTrace();
		
		if (sendReportEmail && _account != null) {
			try {
				GoogleMail.send(_account, _account.getAddress(), "Error repport from : " + super.getName(), createErrorInfosMsg(e));
			} catch (MessagingException ex) {
				logException(lvl, e, false);
			}
		}
	}
	
	/**
	 * Close all the handlers
	 */
	public void close() {
		for (Handler h : getHandlers())
			h.close();
	}
	
	/**
	 * Create a message in which is written the OS informations and the stacktrace of the throwable e
	 * @param e The throwable
	 */
	public static String createErrorInfosMsg(Throwable e) {
		return "OS Infos : " + PlatformUtil.getOSInfos() + "\n" + StringUtil.stackTraceToString(e);
	}
}
