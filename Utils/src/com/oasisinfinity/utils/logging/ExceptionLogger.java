package com.oasisinfinity.utils.logging;

import java.util.logging.Handler;
import java.util.logging.Level;

public interface ExceptionLogger {
	void logException(Level lvl, Throwable e);
	void logException(Level lvl, Throwable e, boolean sendReportEmail);
	Handler[] getHandlers();
}
