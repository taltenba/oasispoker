package com.oasisinfinity.utils.logging;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;

public class LogPolicy implements RejectedExecutionHandler {
	private final ExceptionLogger _logger;
	
	/**
	 * 
	 * @param logger the logger to use, if null do nothing when exception is rejected
	 */
	public LogPolicy(ExceptionLogger logger) {
		_logger = logger;
	}
	
	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		if (_logger != null)
			_logger.logException(Level.INFO, new RejectedExecutionException("Task " + r.toString() + " rejected from " + executor.toString()));
	}
}
