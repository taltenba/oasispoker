package com.oasisinfinity.utils.logging;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.logging.Level;

public class CrashHandler implements UncaughtExceptionHandler {
	protected final ExceptionLogger _logger;
		
	public CrashHandler(ExceptionLogger logger) {
		_logger = logger;
	}
	
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		_logger.logException(Level.SEVERE, e, true);
	}
}
