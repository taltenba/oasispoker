package com.oasisinfinity.utils.logging;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.LogRecord;

public class AsyncFileHandler extends FileHandler {
	private final ExecutorService _writeThread = Executors.newSingleThreadExecutor(r -> {
													return new Thread(r, "AsyncFileHandlerWriterThread");
												});
	
	public AsyncFileHandler() throws IOException, SecurityException {
		super();
	}

	public AsyncFileHandler(String pattern) throws IOException, SecurityException {
		super(pattern);
	}

	public AsyncFileHandler(String pattern, boolean append) throws IOException, SecurityException {
		super(pattern, append);
	}

	public AsyncFileHandler(String pattern, int limit, int count) throws IOException, SecurityException {
		super(pattern, limit, count);
	}

	public AsyncFileHandler(String pattern, int limit, int count, boolean append) throws IOException, SecurityException {
		super(pattern, limit, count, append);
	}
	
	@Override
	public void publish(LogRecord record) {
		if (record == null)
			throw new NullPointerException("record is null");
		
		_writeThread.execute(new LogWriterRunnable(record));
	}

	/**
	 * Shutdown the ExecutorService, the files will be closed next to the shutdown or when the timeout has expired
	 * @param timeout - the timeout
	 * @param unit - the time unit of the timeout
	 */
	public synchronized void close(final long timeout, final TimeUnit unit) {
		if (_writeThread.isShutdown())
			return;
		
		_writeThread.shutdown();
		
		new Thread(() -> {
			try {
				_writeThread.awaitTermination(timeout, unit);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (!_writeThread.isTerminated())
				_writeThread.shutdownNow();
			
			AsyncFileHandler.super.close();
		}, "AsyncFileHandlerCloserThread").start();
	}
	
	/**
	 * Shutdown the ExecutorService, the files will be closed next to the shutdown
	 */
	@Override
	public void close() {
		close(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
	}
	
	private class LogWriterRunnable implements Runnable {
		private final LogRecord _record;
		
		public LogWriterRunnable(LogRecord record) {
			_record = record;
		}
		
		@Override
		public void run() {
			AsyncFileHandler.super.publish(_record);
		}	
	}
}
