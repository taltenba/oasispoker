package com.oasisinfinity.utils.concurrent;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.OverlappingFileLockException;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class SingleAppInstanceLock {
    private final RandomAccessFile _accessFile;
    
    @GuardedBy("this") private boolean _isLocked = false;

    public SingleAppInstanceLock(String lockName) throws IOException {
    	synchronized (this) {
        	File lockFile = new File(System.getProperty("java.io.tmpdir") + File.separator + lockName + ".lock");
        	
    		if (!lockFile.exists())
            	lockFile.createNewFile();
        	
        	_accessFile = new RandomAccessFile(lockFile, "rw");
        	
        	Runtime.getRuntime().addShutdownHook(new Thread(() -> {
    		    try {
    		    	_accessFile.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
        	}));
        	
        	lockFile.deleteOnExit();
    	}
    }

    /**
     * Creates the lock file if it's not present and requests its deletion on
     * program termination or informs that the program is already running if
     * that's the case.
     * @return true - if the operation was successful or if the program already has the lock.<br>
     * false - if the program is already running
     * @throws IOException
     */
    public synchronized boolean lock() throws IOException {
        if (_isLocked) 
        	return true;

        try {
        	if (_accessFile.getChannel().tryLock() == null)
        		return false;
        	
            _isLocked = true;
        } catch (OverlappingFileLockException e) {
        	return false;
        }
        
        return true;
    }
    
    public synchronized void unlock() throws IOException {
    	if (!_isLocked) 
        	return;
    	
    	_accessFile.close();
    }
    
    public synchronized void waitForLock() throws IOException {
    	if (_isLocked) 
    		return;

    	_accessFile.getChannel().lock();
        _isLocked = true;
    }
}
