package com.oasisinfinity.utils.concurrent;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import com.oasisinfinity.utils.logging.ExceptionLogger;

public final class ConcurrentUtil {

	private ConcurrentUtil() { /* Empty */ }
	
	public static <T> T getSubmitResult(ExecutorService exec, Callable<T> submit) {
		return getSubmitResult(exec, submit, null);
	}
	
	public static <T> T getSubmitResult(ExecutorService exec, Callable<T> submit, ExceptionLogger logger) {
		Future<T> buildingFuture = exec.submit(submit);
		
		T newInstance = null;
		
		try {
			newInstance = buildingFuture.get();
		} catch (InterruptedException | ExecutionException e) {
			if (logger != null)
				logger.logException(Level.WARNING, e);
		}
		
		return newInstance;
	}
	
	public static ThreadPoolExecutor newFixedCrashHandlerThreadPoolExecutor(int nThreads, UncaughtExceptionHandler crashHandler) {
		return new CrashHandlerThreadPoolExecutor(nThreads, nThreads,
                                      0L, TimeUnit.MILLISECONDS,
                                      new LinkedBlockingQueue<Runnable>(),
                                      crashHandler);
	}
	
	public static ThreadPoolExecutor newFixedCrashHandlerThreadPoolExecutor(int nThreads, ThreadFactory threadFactory, UncaughtExceptionHandler crashHandler) {
		return new CrashHandlerThreadPoolExecutor(nThreads, nThreads,
                                      0L, TimeUnit.MILLISECONDS,
                                      new LinkedBlockingQueue<Runnable>(),
                                      threadFactory, crashHandler);
	}
	
	public static ExecutorService newCachedCrashHandlerThreadPool(UncaughtExceptionHandler crashHandler) {
        return new CrashHandlerThreadPoolExecutor(0, Integer.MAX_VALUE,
                                      60L, TimeUnit.SECONDS,
                                      new SynchronousQueue<Runnable>(),
                                      crashHandler);
    }

    public static ExecutorService newCachedCrashHandlerThreadPool(ThreadFactory threadFactory, UncaughtExceptionHandler crashHandler) {
        return new CrashHandlerThreadPoolExecutor(0, Integer.MAX_VALUE,
                                      60L, TimeUnit.SECONDS,
                                      new SynchronousQueue<Runnable>(),
                                      threadFactory,
                                      crashHandler);
    }
    
    public static ScheduledThreadPoolExecutor newCrashHandlerScheduledThreadPool(int corePoolSize, UncaughtExceptionHandler crashHandler) {
        return new CrashHandlerScheduledThreadPoolExecutor(corePoolSize, crashHandler);
    }

    public static ScheduledThreadPoolExecutor newCrashHandlerScheduledThreadPool(int corePoolSize, ThreadFactory threadFactory, UncaughtExceptionHandler crashHandler) {
        return new CrashHandlerScheduledThreadPoolExecutor(corePoolSize, threadFactory, crashHandler);
    }
	
	private static class CrashHandlerThreadPoolExecutor extends ThreadPoolExecutor {
		private final UncaughtExceptionHandler _crashHandler;
		
		public CrashHandlerThreadPoolExecutor(int corePoolSize,
								              int maximumPoolSize,
								              long keepAliveTime,
								              TimeUnit unit,
								              BlockingQueue<Runnable> workQueue,
								              UncaughtExceptionHandler crashHandler) {
			super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
			_crashHandler = Objects.requireNonNull(crashHandler, "crashHandler is null");
		}
		
		public CrashHandlerThreadPoolExecutor(int corePoolSize,
								              int maximumPoolSize,
								              long keepAliveTime,
								              TimeUnit unit,
								              BlockingQueue<Runnable> workQueue,
								              ThreadFactory threadFactory,
								              UncaughtExceptionHandler crashHandler) {
			super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
			_crashHandler = Objects.requireNonNull(crashHandler, "crashHandler is null");
		}
		
		@Override
		protected void afterExecute(Runnable r, Throwable t) {
		      super.afterExecute(r, t);
		      handleCrash(r, t, _crashHandler);
		}
	}
	
	private static class CrashHandlerScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor {
		private final UncaughtExceptionHandler _crashHandler;
		
		public CrashHandlerScheduledThreadPoolExecutor(int corePoolSize, UncaughtExceptionHandler crashHandler) {
			super(corePoolSize);
			_crashHandler = Objects.requireNonNull(crashHandler, "crashHandler is null");
		}
		
		public CrashHandlerScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory, UncaughtExceptionHandler crashHandler) {
			super(corePoolSize, threadFactory);
			_crashHandler = Objects.requireNonNull(crashHandler, "crashHandler is null");
		}
		
		@Override
		protected void afterExecute(Runnable r, Throwable t) {
		      super.afterExecute(r, t);
		      handleCrash(r, t, _crashHandler);
		}
	}
	
	private static void handleCrash(Runnable r, Throwable t, UncaughtExceptionHandler crashHandler) {
		if (t == null && r instanceof Future<?>) {
	        try {
	          Future<?> future = (Future<?>) r;
	          
	          if (future.isDone())
	            future.get();
	        } catch (CancellationException ce) {
	            // ignore
	        } catch (ExecutionException ee) {
	            t = ee.getCause();
	        } catch (InterruptedException ie) {
	            Thread.currentThread().interrupt(); // ignore/reset
	        }
	      }
	      
	      if (t != null)
	    	  crashHandler.uncaughtException(Thread.currentThread(), t);
	}
}
