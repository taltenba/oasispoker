package com.oasisinfinity.utils;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class BasicThreadFactory implements ThreadFactory {
	private final AtomicInteger _count;
	
	private final String _prefix;
	private final UncaughtExceptionHandler _exHandler;
	private final boolean _daemon;
	private final int _priority;
	
	public BasicThreadFactory(String namePrefix) {
		this(namePrefix, null);
	}
	
	public BasicThreadFactory(String namePrefix, UncaughtExceptionHandler exHandler) {
		this(namePrefix, exHandler, false);
	}
	
	public BasicThreadFactory(String namePrefix, UncaughtExceptionHandler exHandler, boolean deamon) {
		this(namePrefix, exHandler, deamon, Thread.NORM_PRIORITY);
	}
	
	public BasicThreadFactory(String namePrefix, UncaughtExceptionHandler exHandler, boolean daemon, int priority) {
		checkPriority(priority);
		
		_prefix = namePrefix;
		_exHandler = exHandler;
		_daemon = daemon;
		_priority = priority;
		_count = new AtomicInteger();
	}

	@Override
	public Thread newThread(Runnable r) {
		return createNewThread(r, _prefix + " - " + _count.incrementAndGet(), _exHandler, _daemon, _priority);
	}
	
	public static Thread createNewThread(Runnable r, String name) {
		return createNewThread(r, name, null);
	}
	
	public static Thread createNewThread(Runnable r, String name, UncaughtExceptionHandler exHandler) {
		return createNewThread(r, name, exHandler, false);
	}
	
	public static Thread createNewThread(Runnable r, String name, UncaughtExceptionHandler exHandler, boolean daemon) {
		return createNewThread(r, name, exHandler, daemon, Thread.NORM_PRIORITY);
	}
	
	public static Thread createNewThread(Runnable r, String name, UncaughtExceptionHandler exHandler, boolean daemon, int priority) {
		checkPriority(priority);
		
		Thread t = new Thread(r, name);
		t.setUncaughtExceptionHandler(exHandler);
		t.setDaemon(daemon);
		t.setPriority(priority);
		
		return t;
	}
	
	private static void checkPriority(int priority) {
		if (priority < Thread.MIN_PRIORITY || priority > Thread.MAX_PRIORITY)
			throw new IllegalArgumentException("priority is not in the range MIN_PRIORITY to MAX_PRIORITY");
	}
}
