package com.oasisinfinity.utils.javafx;

import java.util.List;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.util.Duration;

public final class AnimUtil {
	public static final Duration DEFAULT_DURATION = Duration.millis(400);
	
	private AnimUtil() { /* Empty */ }
	
	public static FadeTransition fadeAnim(double from, double to) {
		return fadeAnim(from, to, null, DEFAULT_DURATION);
	}
	
	public static FadeTransition fadeAnim(double from, double to, Node n) {
		return fadeAnim(from, to, n, DEFAULT_DURATION);
	}
	
	public static FadeTransition fadeAnim(double from, double to, Node n, Duration dur) {
		FadeTransition anim = new FadeTransition(dur, n);
		anim.setFromValue(from);
		anim.setToValue(to);
		
		return anim;
	}
	
	public static FadeTransition fadeOutAnim() {
		return fadeOutAnim(null, DEFAULT_DURATION);
	}
	
	public static FadeTransition fadeOutAnim(Node n) {
		return fadeOutAnim(n, DEFAULT_DURATION);
	}
	
	public static FadeTransition fadeOutAnim(Duration dur) {
		return fadeOutAnim(null, dur);
	}
	
	public static FadeTransition fadeOutAnim(Node n, Duration dur) {
		return fadeAnim(n == null ? 1.0 : n.getOpacity(), 0.0, n, dur);
	}
	
	public static FadeTransition fadeInAnim() {
		return fadeInAnim(null, DEFAULT_DURATION);
	}
	
	public static FadeTransition fadeInAnim(Node n) {
		return fadeInAnim(n, DEFAULT_DURATION);
	}
	
	public static FadeTransition fadeInAnim(Duration dur) {
		return fadeInAnim(null, dur);
	}
	
	public static FadeTransition fadeInAnim(Node n, Duration dur) {
		return fadeInAnim(n, dur, 1.0);
	}
	
	public static FadeTransition fadeInAnim(Node n, Duration dur, double finalOpacity) {
		return fadeAnim(0.0, finalOpacity, n, dur);
	}
	
	public static FadeTransition[] fadeAnims(double from, double to, Node... nodes) {
		return fadeAnims(from, to, DEFAULT_DURATION, nodes);
	}
	
	public static FadeTransition[] fadeAnims(double from, double to, List<Node> nodes) {
		return fadeAnims(from, to, DEFAULT_DURATION, nodes);
	}
	
	public static FadeTransition[] fadeAnims(double from, double to, Duration dur, Node... nodes) {
		int nodesCount = nodes.length;
		FadeTransition[] anims = new FadeTransition[nodesCount];
		
		for (int i = 0; i < nodesCount; ++i)
			anims[i] = fadeAnim(from, to, nodes[i], dur);
		
		return anims;
	}
	
	public static FadeTransition[] fadeAnims(double from, double to, Duration dur, List<Node> nodes) {
		int nodesCount = nodes.size();
		FadeTransition[] anims = new FadeTransition[nodesCount];
		
		for (int i = 0; i < nodesCount; ++i)
			anims[i] = fadeAnim(from, to, nodes.get(i), dur);
		
		return anims;
	}

	public static ParallelTransition parallelFadeAnims(double from, double to, Duration dur, Node... nodes) {
		return new ParallelTransition(fadeAnims(from, to, dur, nodes));
	}
	
	public static ParallelTransition parallelFadeAnims(double from, double to, Duration dur, List<Node> nodes) {
		return new ParallelTransition(fadeAnims(from, to, dur, nodes));
	}
	
	public static ParallelTransition parallelFadeInAnims(Duration dur, List<Node> nodes) {
		return parallelFadeAnims(0.0, 1.0, dur, nodes);
	}
	
	public static ParallelTransition parallelFadeInAnims(Duration dur, Node... nodes) {
		return parallelFadeAnims(0.0, 1.0, dur, nodes);
	}
	
	public static ParallelTransition parallelFadeOutAnims(Duration dur, List<Node> nodes) {
		return parallelFadeAnims(1.0, 0.0, dur, nodes);
	}
	
	public static ParallelTransition parallelFadeOutAnims(Duration dur, Node... nodes) {
		return parallelFadeAnims(1.0, 0.0, dur, nodes);
	}
	
	public static SequentialTransition sequentialFadeAnims(double from, double to, Duration dur, Node... nodes) {
		return new SequentialTransition(fadeAnims(from, to, dur, nodes));
	}
	
	public static SequentialTransition sequentialFadeAnims(double from, double to, Duration dur, List<Node> nodes) {
		return new SequentialTransition(fadeAnims(from, to, dur, nodes));
	}
	
	public static SequentialTransition sequentialFadeInAnims(Duration dur, List<Node> nodes) {
		return sequentialFadeAnims(0.0, 1.0, dur, nodes);
	}
	
	public static SequentialTransition sequentialFadeInAnims(Duration dur, Node... nodes) {
		return sequentialFadeAnims(0.0, 1.0, dur, nodes);
	}
	
	public static SequentialTransition sequentialFadeOutAnims(Duration dur, List<Node> nodes) {
		return sequentialFadeAnims(1.0, 0.0, dur, nodes);
	}
	
	public static SequentialTransition sequentialFadeOutAnims(Duration dur, Node... nodes) {
		return sequentialFadeAnims(1.0, 0.0, dur, nodes);
	}
	
	public static void showNode(Node n, Duration dur) {
		showNode(n, dur, null, 1.0);
	}
	
	public static void showNode(Node n, Duration dur, EventHandler<ActionEvent> onFinished) {
		showNode(n, dur, onFinished, 1.0);
	}
	
	public static void showNode(Node n, Duration dur, double finalOpacity) {
		showNode(n, dur, null, finalOpacity);
	}
	
	public static void showNode(Node n, Duration dur, EventHandler<ActionEvent> onFinished, double finalOpacity) {
		n.setOpacity(0.0);
		n.setManaged(true);
		n.setVisible(true);
		
		FadeTransition anim = fadeInAnim(n, dur, finalOpacity);
		anim.setOnFinished(onFinished);
		
		anim.play();
	}
	
	public static void hideNode(Node n, Duration dur, boolean unmanage) {
		hideNode(n, dur, unmanage, null);
	}
	
	public static void hideNode(Node n, Duration dur, boolean unmanage, EventHandler<ActionEvent> onFinished) {
		FadeTransition anim = fadeOutAnim(n, dur);
		
		anim.setOnFinished(e -> {
			if (unmanage)
				n.setManaged(false);
			
			n.setVisible(false);
			
			if (onFinished != null)
				onFinished.handle(e);
		});
		
		anim.play();
	}
	
	public static void hideNodesParallel(List<Node> nodes, Duration dur, boolean unmanage, EventHandler<ActionEvent> onFinished) {
		ParallelTransition anims = parallelFadeOutAnims(dur, nodes);
		
		anims.setOnFinished(e -> {
			for (Node n : nodes) {
				if (unmanage)
					n.setManaged(false);
				
				n.setVisible(false);
			}
			
			if (onFinished != null)
				onFinished.handle(e);
		});
		
		anims.play();
	}
	
	public static void switchNode(Node toHide, Node toShow, Duration halfDur) {
		hideNode(toHide, halfDur, true, e -> showNode(toShow, halfDur));
	}
} 
