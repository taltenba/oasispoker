package com.oasisinfinity.utils.javafx;

import java.util.concurrent.Executor;

import javafx.application.Platform;

public class FxThreadExecutor implements Executor {
	private static final FxThreadExecutor INSTANCE = new FxThreadExecutor();
	
	private FxThreadExecutor() { /* Empty */ }
	
	@Override
	public void execute(final Runnable r) {
		run(r);
	}
	
	public static FxThreadExecutor getInstance() {
		return INSTANCE;
	}
	
	public static void run(final Runnable r) {
		if (Platform.isFxApplicationThread())
			r.run();
		else
			Platform.runLater(r);
	}
}
