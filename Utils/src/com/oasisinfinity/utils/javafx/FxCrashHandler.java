package com.oasisinfinity.utils.javafx;

import java.util.logging.Handler;

import com.oasisinfinity.utils.logging.CrashHandler;
import com.oasisinfinity.utils.logging.ExceptionLogger;

import javafx.application.Platform;
import javafx.stage.StageStyle;

public class FxCrashHandler extends CrashHandler {
	public FxCrashHandler(ExceptionLogger logger) {
		super(logger);
	}
	
	@Override
	public void uncaughtException(final Thread t, final Throwable e) {
		super.uncaughtException(t, e);
		
		for (Handler h : _logger.getHandlers())
			h.close();
		
		FxThreadExecutor.run(() -> {
			showCrashMsg("Une erreur est survenue, l'application doit quitter.");
			
			Platform.exit();
			System.exit(-1);
		});
	}
	
	/**
	 * Must be executed in FxThread
	 */
	public static void showCrashMsg(String explanation) {
			AlertUtil.showErrorMsg("Erreur fatale", "Erreur fatale", 
					explanation +  "\nPour plus d'information veuillez consulter le dernier fichier .log : " + System.getProperty("user.dir"), 
					null, StageStyle.DECORATED, null);
	}
}
