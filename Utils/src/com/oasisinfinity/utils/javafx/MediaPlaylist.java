package com.oasisinfinity.utils.javafx;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.oasisinfinity.utils.RandomUtil;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class MediaPlaylist {
	private final List<MediaPlayer> _players;
	private final boolean _randomPlay;
	private final Runnable _endOfMediaRunnable = () -> nextTrack();
	
	private int _currentTrack;
	
	private MediaPlaylist(boolean randomPlay) {
		_players = new ArrayList<>();
		_randomPlay = randomPlay;
		_currentTrack = -1;
	}
	
	public MediaPlaylist(File tracksDir) throws IOException {
		this(tracksDir, false);
	}
	
	public MediaPlaylist(URI[] tracksUris) {
		this(tracksUris, false);
	}
	
	public MediaPlaylist(File tracksDir, boolean randomPlay) throws IOException {
		this(randomPlay);
		
		if (!tracksDir.isDirectory())
			throw new IllegalArgumentException("tracksDir must be a directory");

		File[] tracksFiles = tracksDir.listFiles();
		
		if (tracksFiles == null)
			throw new IOException();
		
		for (File f : tracksFiles) {
			MediaPlayer trackPlayer = new MediaPlayer(new Media(f.toURI().toString()));
			
			trackPlayer.setOnEndOfMedia(_endOfMediaRunnable);
			
			_players.add(trackPlayer);
		}
	}
	
	public MediaPlaylist(URI[] tracksUris, boolean randomPlay) {
		this(randomPlay);
		
		for (URI uri : tracksUris) {
			MediaPlayer trackPlayer = new MediaPlayer(new Media(uri.toString()));
			
			trackPlayer.setOnEndOfMedia(_endOfMediaRunnable);
			
			_players.add(trackPlayer);
		}
	}
	
	public void play() {
		if (_randomPlay)
			shuffle();
		
		nextTrack();
	}
	
	public void dispose() {
		for (MediaPlayer player : _players)
			player.dispose();
	}
	
	public void setVolume(double value) {
		for (MediaPlayer player : _players)
			player.setVolume(value);
	}

	private void nextTrack() {
		if (_currentTrack >= _players.size() - 1) {
			_currentTrack = -1;
			
			if (_randomPlay)
				shuffle();
		}
		
		++_currentTrack;
		
		_players.get(_currentTrack).play();
	}
	
	private void shuffle() {
		RandomUtil.shuffle(_players);
	}
}
