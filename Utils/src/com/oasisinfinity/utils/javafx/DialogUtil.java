package com.oasisinfinity.utils.javafx;

import java.io.IOException;
import java.net.URL;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class DialogUtil {

	private DialogUtil() { /* Empty */ }

	public static <T> StageInfos<T> createDialogStage(StageStyle dialogStyle, URL fxmlLocation, String title, boolean resizable, Modality modality, Window owner) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(fxmlLocation);
		
		Stage dialogStage = new Stage(dialogStyle);
		dialogStage.setTitle(title);
		dialogStage.setResizable(resizable);
		dialogStage.initModality(modality);
	    dialogStage.initOwner(owner);
	    dialogStage.setScene(new Scene(loader.load()));

	    return new StageInfos<T>(dialogStage, loader.getController());
	}
	
	public static <T> StageInfos<T> createDialogStage(URL fxmlLocation, String title, boolean resizable, Modality modality, Window owner) throws IOException {
		return createDialogStage(StageStyle.DECORATED, fxmlLocation, title, resizable, modality, owner);
	}
	
	public static void showDialogStage(URL fxmlLocation, String title, boolean resizable, Modality modality, Window owner) throws IOException {
		showCentered(createDialogStage(fxmlLocation, title, resizable, modality, owner).getStage());
	}
	
	public static void showDialogStage(StageStyle dialogStage, URL fxmlLocation, String title, boolean resizable, Modality modality, Window owner) throws IOException {
		showCentered(createDialogStage(dialogStage, fxmlLocation, title, resizable, modality, owner).getStage());
	}

	public static void showCentered(Stage dialogStage) {
		dialogStage.show();
		
		centerInOwner(dialogStage);
	}
	
	public static void showAndWaitCentered(final Stage dialogStage) {
		Platform.runLater(() -> centerInOwner(dialogStage));
		
		dialogStage.showAndWait();
	}
	
	private static void centerInOwner(Stage dialogStage) {
		Window owner = dialogStage.getOwner();
		
		if (owner == null)
			return;
		
		dialogStage.setX(owner.getX() + (owner.getWidth() / 2) - (dialogStage.getWidth() / 2)); 
		dialogStage.setY(owner.getY() + (owner.getHeight() / 2) - (dialogStage.getHeight() / 2));
	}
	
	public static final class StageInfos<T> {
		private final Stage _stage;
		private final T _controller;
		
		public StageInfos(Stage stage, T controller) {
			_stage = stage;
			_controller = controller;
		}

		public Stage getStage() {
			return _stage;
		}

		public T getController() {
			return _controller;
		}
	}
}
