package com.oasisinfinity.utils.javafx;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public final class AlertUtil {
	private final String _stylesheet;
	private final StageStyle _stageStyle;
	
	private Window _parentWindow;
	
	public AlertUtil() {
		this(null, StageStyle.DECORATED);
	}
	
	public AlertUtil(String stylesheet, StageStyle stageStyle) {
		this(stylesheet, stageStyle, null);
	}
	
	public AlertUtil(String stylesheet, StageStyle stageStyle, Window parent) {
		_stylesheet = stylesheet;
		_stageStyle = stageStyle;
		_parentWindow = parent;
	}
	
	public void setParentWindow(Window parent) {
		_parentWindow = parent;
	}
	
	public void showMsg(AlertType type, String title, String content) {
		showMsg(type, title, title, content);
	}
	
	public void showMsg(AlertType type, String title, String header, String content) {
		showMsg(type, title, header, content, _stylesheet, _stageStyle, _parentWindow);
	}
	
	public void showErrorMsg(String header, String content) {
		showErrorMsg("Erreur", header, content);
	}
	
	public void showErrorMsg(String title, String header, String content) {
		showMsg(AlertType.ERROR, title, header, content);
	}
	
	public boolean showQuitConfirmationMsg(String msg) {
		return showQuitConfirmationMsg(msg, _stylesheet, _stageStyle, _parentWindow);
	}
	
	public boolean showConfirmationMsg(String title, String content) {
		return showConfirmationMsg(title, title, content);
	}
	
	public boolean showConfirmationMsg(String title, String header, String content) {
		return showConfirmationMsg(title, header, content, _stylesheet, _stageStyle, _parentWindow);
	}
	
	public void corruptedInstallation() {
		corruptedInstallation(_stylesheet, _stageStyle, _parentWindow);
	}
	
	public static CenteredAlert getMsg(AlertType type, String title, String header, String content, String stylesheet, StageStyle stageStyle, Window parent) {
		CenteredAlert alert = new CenteredAlert(type, parent);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.initStyle(stageStyle);
		
		DialogPane alertPane = alert.getDialogPane();
		
		if (stylesheet != null)
			alertPane.getStylesheets().add(stylesheet);
		
		return alert;
	}
	
	public static boolean showConfirmationMsg(String title, String header, String content, String stylesheet, StageStyle stageStyle, Window parent) {
		CenteredAlert alert = getMsg(AlertType.CONFIRMATION, title, header, content, stylesheet, stageStyle, parent);
		
		ObservableList<ButtonType> buttons = alert.getButtonTypes();		
		buttons.clear();
		buttons.addAll(ButtonType.YES, ButtonType.NO);
		
		alert.showAndWaitCentered();
		
		if (alert.getResult().equals(ButtonType.NO))
			return false;
		
		return true;
	}
	
	public static void showMsg(AlertType type, String title, String header, String content, String stylesheet, StageStyle stageStyle, Window parent) {
		getMsg(type, header, title, content, stylesheet, stageStyle, parent).showAndWaitCentered();
	}
	
	public static void showErrorMsg(String title, String header, String content, String stylesheet, StageStyle stageStyle, Window parent) {
		showMsg(AlertType.ERROR, title, header, content, stylesheet, stageStyle, parent);
	}
	
	public static boolean showQuitConfirmationMsg(String msg, String stylesheet, StageStyle stageStyle, Window parent) {
		String quit = "Quitter ?";
		return showConfirmationMsg(quit, quit, msg, stylesheet, stageStyle, parent);
	}
	
	public static void corruptedInstallation(String stylesheet, StageStyle stageStyle, Window parent) {
		String impossibleInit = "Initialisation impossible";
		
		showErrorMsg(impossibleInit, impossibleInit, "Votre installation est corrompue, merci de réinstaller l'application.", stylesheet, stageStyle, parent);
		
		Platform.exit();
		System.exit(-1);
	}
}
