package com.oasisinfinity.utils.javafx;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class UnmodifiableObservableList<E> implements ObservableList<E> {
	private ObservableList<E> _list;
	
	public UnmodifiableObservableList(ObservableList<E> list) {
		if (list == null)
			throw new NullPointerException("list is null");
		
		_list = list;	
	}

	@Override
	public boolean add(E e) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public void add(int index, E element) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean contains(Object o) {
		return _list.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return _list.containsAll(c);
	}

	@Override
	public E get(int index) {
		return _list.get(index);
	}

	@Override
	public int indexOf(Object o) {
		return _list.indexOf(o);
	}

	@Override
	public boolean isEmpty() {
		return _list.isEmpty();
	}

	@Override
	public Iterator<E> iterator() {
		return _list.iterator();
	}

	@Override
	public int lastIndexOf(Object o) {
		return _list.lastIndexOf(o);
	}

	@Override
	public ListIterator<E> listIterator() {
		return _list.listIterator();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return _list.listIterator(index);
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public E remove(int index) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public E set(int index, E element) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public int size() {
		return _list.size();
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		return _list.subList(fromIndex, toIndex);
	}

	@Override
	public Object[] toArray() {
		return _list.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return _list.toArray(a);
	}

	@Override
	public void addListener(InvalidationListener listener) {
		_list.addListener(listener);
	}

	@Override
	public void removeListener(InvalidationListener listener) {
		_list.removeListener(listener);
	}

	@Override
	public boolean addAll(@SuppressWarnings("unchecked") E... elements) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public void addListener(ListChangeListener<? super E> listener) {
		_list.addListener(listener);
	}

	@Override
	public void remove(int from, int to) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean removeAll(@SuppressWarnings("unchecked") E... elements) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public void removeListener(ListChangeListener<? super E> listener) {
		_list.removeListener(listener);
	}

	@Override
	public boolean retainAll(@SuppressWarnings("unchecked") E... elements) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean setAll(@SuppressWarnings("unchecked") E... elements) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

	@Override
	public boolean setAll(Collection<? extends E> col) {
		throw new UnsupportedOperationException("List is unmodifiable");
	}

}
