package com.oasisinfinity.utils.javafx;

import javafx.application.Platform;
import javafx.beans.NamedArg;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Window;

public class CenteredAlert extends Alert {
	private final Window _parentWindow;
	
	public CenteredAlert(@NamedArg("alertType") AlertType alertType, Window parentWindow) {
		super(alertType);
		
		_parentWindow = parentWindow;
	}
	
	public CenteredAlert(@NamedArg("alertType") AlertType alertType, @NamedArg("contentText") String contentText, Window parentWindow, ButtonType... buttons) {
		super(alertType, contentText, buttons);
		
		_parentWindow = parentWindow;
	}
	
	private void center() {
		if (_parentWindow != null) {
			setX(_parentWindow.getX() + (_parentWindow.getWidth() / 2) - (getWidth() / 2)); 
			setY(_parentWindow.getY() + (_parentWindow.getHeight() / 2) - (getHeight() / 2));
		}
	}

	public void showCentered() {
		super.show();
		
		center();
	}
	
	public void showAndWaitCentered() {
		Platform.runLater(() -> center());
		
		super.showAndWait();
	}
}
