package com.oasisinfinity.utils.javafx;

import javafx.scene.control.TextField;

public class NodeUtil {

	private NodeUtil() { /* Empty */ }

	public static void addTextLimiter(final TextField tf, final int maxLength) {
	    tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (tf.getText().length() > maxLength)
            	tf.setText(tf.getText().substring(0, maxLength));
	    });
	}
}
