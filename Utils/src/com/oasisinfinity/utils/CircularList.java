package com.oasisinfinity.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

public class CircularList<E> implements List<E>, Serializable {
	private static final long serialVersionUID = 3182852072042287406L;
	
	protected List<E> _list;
	
	private CircularList(List<E> list) {
		if (list == null)
			throw new NullPointerException("list is null");
		
		_list = list;
	}
	
	public int toRealIndex(int index) {
		int size = size();
		
		if (size == 0)
			return 0;
		
		index %= size;
		
		if (index < 0)
			index += size;
		
		return index;
	}
	
	public static int toRealIndex(int index, int size) {
		if (size == 0)
			return 0;
		
		index %= size;
		
		if (index < 0)
			index += size;
		
		return index;
	}
	
	@Override
	public E get(int index) {
		return _list.get(toRealIndex(index));	
	}

	@Override
	public boolean add(E e) {
		return _list.add(e);
	}

	@Override
	public void add(int index, E element) {
		_list.add(toRealIndex(index), element);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		return _list.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return _list.addAll(toRealIndex(index), c);
	}

	@Override
	public void clear() {
		_list.clear();
	}

	@Override
	public boolean contains(Object o) {
		return _list.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return _list.containsAll(c);
	}

	@Override
	public int indexOf(Object o) {
		return _list.indexOf(o);
	}

	@Override
	public boolean isEmpty() {
		return _list.isEmpty();
	}

	@Override
	public Iterator<E> iterator() {
		return _list.iterator();
	}

	@Override
	public int lastIndexOf(Object o) {
		return _list.lastIndexOf(o);
	}

	@Override
	public ListIterator<E> listIterator() {
		return _list.listIterator();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return _list.listIterator(toRealIndex(index));
	}

	@Override
	public boolean remove(Object o) {
		return _list.remove(o);
	}

	@Override
	public E remove(int index) {
		return _list.remove(toRealIndex(index));
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return _list.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return _list.retainAll(c);
	}

	@Override
	public E set(int index, E element) {
		return _list.set(toRealIndex(index), element);
	}

	@Override
	public int size() {
		return _list.size();
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		return new CircularList<>(_list.subList(toRealIndex(fromIndex), toRealIndex(toIndex)));
	}

	@Override
	public Object[] toArray() {
		return _list.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return _list.toArray(a);
	}
	
	public static <T> CircularList<T> wrap(List<T> list) {
		return list instanceof RandomAccess ?
				new RandomAccessCircularList<T>(list) :
				new CircularList<T>(list);
	}
	
	public static <T> CircularList<T> wrapArrayList() {
		return new RandomAccessCircularList<T>(new ArrayList<>());
	}
	
	public static <T> CircularList<T> wrapLinkedList() {
		return new CircularList<T>(new LinkedList<>());
	}
	
	private static class RandomAccessCircularList<E> extends CircularList<E> implements RandomAccess {
		private static final long serialVersionUID = -7344778493304552554L;

		private RandomAccessCircularList(List<E> list) {
			super(list);
		}
		
		@Override
		public List<E> subList(int fromIndex, int toIndex) {
			return new RandomAccessCircularList<>(_list.subList(toRealIndex(fromIndex), toRealIndex(toIndex)));
		}
	}
}
