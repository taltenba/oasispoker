package com.oasisinfinity.utils;

import java.util.Collection;
import java.util.NoSuchElementException;

import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class Deck<T> {
	private final Object[] _elements;
	private int _curSize;
	
	public Deck(Collection<? extends T> elements) {
		_elements = elements.toArray();
		_curSize = elements.size();
	}
	
	public Deck(Deck<T> other) {
		_elements = other._elements.clone();
		_curSize = _elements.length;
	}
	
	@SuppressWarnings("unchecked")
	protected T get(int pos) {
		return (T) _elements[pos];
	}
	
	protected boolean contains(int pos) {
		return pos < _curSize;
	}
	
	protected boolean kill(int pos) {
		if (!contains(pos))
			return false;
		
		killSafe(pos);
		return true;
	}
	
	@SuppressWarnings("unchecked")
	protected T killSafe(int pos) {
		T tmp = (T) _elements[pos];
		_elements[pos] = _elements[--_curSize];
		_elements[_curSize] = tmp;
		return tmp;
	}
	
	public void refill() {
		_curSize = _elements.length;
	}
	
	public T draw() {
		if (_curSize == 0)
			throw new NoSuchElementException();

		return killSafe(RandomUtil.generateRandom(_curSize));
	}
	
	public boolean isFull() {
		return _curSize == _elements.length;
	}
	
	public boolean isEmpty() {
		return _curSize == 0;
	}
	
	public int size() {
		return _curSize;
	}
}
