package com.oasisinfinity.utils;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import net.jcip.annotations.Immutable;

public class GoogleMail {
    private GoogleMail() { /* Empty */ }

    public static void send(GoogleAccount from, String[] to, String subject, String body, Attachement[] attachements) throws MessagingException {
    	Properties props = System.getProperties();
    	
    	String host = "smtp.gmail.com";
    	String fromAddress = from.getAddress();
    	String fromPass = from.getPassword();
    	
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", host);
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", fromAddress);
        props.put("mail.smtp.password", fromPass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props);
        MimeMessage message = new MimeMessage(session);
        
        message.setFrom(new InternetAddress(fromAddress));
        
        int toLength = to.length;
        
        InternetAddress[] toAddress = new InternetAddress[toLength];

        for (int i = 0; i < toLength; ++i)
            toAddress[i] = new InternetAddress(to[i]);

        for (int i = 0; i < toAddress.length; ++i)
            message.addRecipient(Message.RecipientType.TO, toAddress[i]);

        message.setSubject(subject);
        
        if (attachements != null) {
        	Multipart multipart = new MimeMultipart();
        	
        	MimeBodyPart textPart = new MimeBodyPart();
        	textPart.setText(body);
        	multipart.addBodyPart(textPart);
        	
        	for (Attachement attachement : attachements) {
        		MimeBodyPart messageBodyPart = new MimeBodyPart();
                messageBodyPart.setDataHandler(new DataHandler(new FileDataSource(attachement.getPath())));
                messageBodyPart.setFileName(attachement.getName());
                
                multipart.addBodyPart(messageBodyPart);
        	}
        	
        	message.setContent(multipart);
        } else
        	message.setText(body);
        
        Transport transport = session.getTransport("smtp");
        transport.connect(host, fromAddress, fromPass);
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();   
    }
    
    public static void send(GoogleAccount from, String[] to, String subject, String body) throws MessagingException {
    	send(from, to, subject, body, null);
    }
    
    public static void send(GoogleAccount from, String to, String subject, String body) throws MessagingException {
    	send(from, new String[] {to}, subject, body);
    }
    
    @Immutable
    public static class Attachement {
    	private final String _name;
    	private final String _path;
    	
    	public Attachement(String name, String path) {
    		_name = name;
    		_path = path;
    	}

		public String getName() {
			return _name;
		}

		public String getPath() {
			return _path;
		}
    }
    
    @Immutable
    public static class GoogleAccount {
    	private final String _address;
    	private final String _password;
    	
    	public GoogleAccount(String address, String password) {
    		_address = address;
    		_password = password;
    	}

		public String getAddress() {
			return _address;
		}

		public String getPassword() {
			return _password;
		}	
    }
}
