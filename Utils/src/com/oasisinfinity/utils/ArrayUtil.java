package com.oasisinfinity.utils;

import java.util.ArrayList;
import java.util.Arrays;

public final class ArrayUtil {
	private ArrayUtil() { /* Empty */ }
	
	public static double[] average(double[] first, double[] second) {
		int length = Math.min(first.length, second.length);
		double[] average = new double[length];
		
		for (int i = 0; i < length; ++i)
			average[i] = (first[i] + second[i]) / 2.0;
		
		return average;
	}
	
	public static double[] average(int[] first, int[] second) {
		int length = Math.min(first.length, second.length);
		double[] average = new double[length];
		
		for (int i = 0; i < length; ++i)
			average[i] = (first[i] + second[i]) / 2.0;
		
		return average;
	}
	
	public static <T> ArrayList<T> toArrayList(T[] array) {
		return new ArrayList<T>(Arrays.asList(array));
	}
}
