package com.oasisinfinity.utils;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public final class RandomUtil {
	private RandomUtil() { /* Empty */ }
	
	/**
	 * Generate random integer in [0; max[
	 * @param max
	 * @return A random integer in [0; max[
	 */
	public static int generateRandom(int max) {
		return ThreadLocalRandom.current().nextInt(max);
	}
	
	/**
	 * Generate random integer in [min; max[
	 * @param min
	 * @param max
	 * @return A random integer in [min; max[
	 */
	public static int generateRandom(int min, int max) {
		return ThreadLocalRandom.current().nextInt(max - min) + min;
	}
	
	public static int[] generateDistinct(int min, int max, int count) {
		return ThreadLocalRandom.current().ints(min, max).distinct().limit(count).toArray();
	}
	
	public static void shuffle(List<?> collection) {
		Collections.shuffle(collection, ThreadLocalRandom.current());
	}
}
