package com.oasisinfinity.utils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.jcip.annotations.Immutable;

@Immutable
public final class ImmutablePair<E> implements Serializable {
	private static final long serialVersionUID = -602298145730562701L;
	
	private final List<E> _elements;
	
	public ImmutablePair(E first, E second) {
		_elements = Collections.unmodifiableList(Arrays.asList(first, second));
	}
	
	public E getFirst() {
		return _elements.get(0);
	}
	
	public E getSecond() {
		return _elements.get(1);
	}
	
	public List<E> toList() {
		return _elements;
	}
	
	@Override
	public int hashCode() {
		return 31 + _elements.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (!(obj instanceof ImmutablePair))
			return false;
		
		return _elements.equals(((ImmutablePair<?>) obj)._elements);
	}

	@Override
	public String toString() {
		return Arrays.toString(_elements.toArray());
	}
}
