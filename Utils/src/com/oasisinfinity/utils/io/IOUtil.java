package com.oasisinfinity.utils.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.google.gson.Gson;

public final class IOUtil {
	private static final Gson GSON = new Gson();
	
	private IOUtil() { /* Empty */ }

	public static String getAppDataDir(String subPath) {
		String appDataDir;
		String OS = System.getProperty("os.name").toUpperCase();
	
		if (OS.contains("WIN")) {
		    appDataDir = System.getenv("AppData"); // $codepro.audit.disable environmentVariableAccess
		    
		    if (!appDataDir.contains("Roaming"))
		    	appDataDir += "/Roaming";
		} else { 
		    appDataDir = System.getProperty("user.home");
		    appDataDir += "/Library/Application Support";
		}
		
		appDataDir += "/OasisInfinity/" + subPath;
		
		File f = new File(appDataDir);
		
		if (!f.exists())
			f.mkdirs();
		
		return appDataDir;
	}

	public static <T> T loadXML(String path, Class<T> tClass) throws JAXBException {
	    return loadXML(new File(path), tClass);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T loadXML(File xml, Class<T> tClass) throws JAXBException {
	    JAXBContext context = JAXBContext.newInstance(tClass);
	    Unmarshaller um = context.createUnmarshaller();
	
	    return (T) um.unmarshal(xml); // $codepro.audit.disable unnecessaryCast
	}

	public static <T> void saveXML(String path, T object, Class<T> tClass) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(tClass);
	    Marshaller m = context.createMarshaller();
	    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	
	    m.marshal(object, new File(path));
	}

	public static <T> T loadJsonFromFile(String path, Class<T> tClass) throws IOException {
		return loadJsonFromFile(new File(path), tClass);
	}
	
	public static <T> T loadJsonFromFile(File file, Class<T> tClass) throws IOException {
		return loadJsonFromFile(new FileInputStream(file), tClass);
	}
	
	public static <T> T loadJsonFromFile(InputStream is, Class<T> tClass) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
			return GSON.fromJson(reader, tClass);
		}
	}
	
	public static <T> T loadJsonFromFile(InputStream is, Type type) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
			return GSON.fromJson(reader, type);
		}
	}
	
	public static <T> T loadJsonFromFile(InputStream is, Type type, Gson gson) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
			return gson.fromJson(reader, type);
		}
	}
	
	public static <T> T loadJsonFromFile(String path, Type type) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), StandardCharsets.UTF_8))) {
			return GSON.fromJson(reader, type);
		}
	}

	public static <T> T loadJson(String json, Class<T> tClass) {
		return GSON.fromJson(json, tClass);
	}
	
	public static <T> T loadJson(String json, Type typeOfT) {
		return GSON.fromJson(json, typeOfT);
	}
	
	public static String toJson(Object object) {
		return GSON.toJson(object);
	}
	
	public static void saveJson(String path, Object object) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(path)), StandardCharsets.UTF_8))) {
			writer.write(toJson(object));
		}
	}
	
	public static String getFileExtension(String fileName) {
		if (!fileName.contains("."))
			return "";
		
		return fileName.substring(fileName.lastIndexOf('.'));
	}
}
