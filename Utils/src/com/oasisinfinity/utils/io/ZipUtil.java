package com.oasisinfinity.utils.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtil {

	private ZipUtil() { /* Emtpy */ }

	//TODO: Add sub-directory support (currently, sub-directory are ignored, all files which stay in that directories are added in the root directory)
    public static void zip(File source, File outputFile) throws IOException {
		zip(source, outputFile, (String[]) null);
    }
    
    public static void zip(File source, File outputFile, String... excludedFilesExt) throws IOException {
		zip(source, outputFile, excludedFilesExt == null ? null : new ArrayList<>(Arrays.asList(excludedFilesExt)));
    }
    
    /**
     * Zip a file/dir
     * @param source The source file/dir to zip
     * @param outputFile The output zip file
     * @param excludeFileExt The extensions of files to exclude (set null if you don't need). Ignored if source is not a dir
     * @throws IOException
     */
    public static void zip(File source, File outputFile, List<String> excludedFileExts) throws IOException {
		byte[] buffer = new byte[1024];
	    	
	    ZipOutputStream zos = null;
	    
	    try {
	    	zos = new ZipOutputStream(new FileOutputStream(outputFile));
	    
		    if (source.isDirectory()) {
		    	List<File> filesList = generateFilesList(source, excludedFileExts);
		    	
		       	for (File file : filesList)
		       		zipEntry(file, zos, buffer);
		    } else
		    	zipEntry(source, zos, buffer);
	    } finally {
		    try {
		    	if (zos != null) {
		    		zos.closeEntry();
		    		zos.close();
		    	}
		    } catch (IOException e) {
		    	e.printStackTrace();
		    }
	    }
    }
    
    private static void zipEntry(File file, ZipOutputStream zos, byte[] buffer) throws IOException {
    	ZipEntry ze = new ZipEntry(file.getName());
    	zos.putNextEntry(ze);

    	try (FileInputStream in = new FileInputStream(file)) {  
        	int len;
        	
        	while ((len = in.read(buffer)) > 0)
        		zos.write(buffer, 0, len);
    	}
    }
    
    private static List<File> generateFilesList(File node, List<String> excludedFilesExt) {
    	List<File> files = new ArrayList<>();
    	
    	if (node.isFile()) {
    		if (excludedFilesExt == null || !excludedFilesExt.contains(IOUtil.getFileExtension(node.getName())))
    			files.add(node);
    	} else {
    		File[] subNodes = node.listFiles();
    		
    		if (subNodes != null) {
        		for (File f : subNodes)
        			files.addAll(generateFilesList(f, excludedFilesExt));
    		}
    	}
    	
    	return files;
    }
}
