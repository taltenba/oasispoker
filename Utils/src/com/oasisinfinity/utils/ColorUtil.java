package com.oasisinfinity.utils;

import javafx.scene.paint.Color;

public final class ColorUtil {
	private ColorUtil() { /* Empty */ }

	public static String toHexColor(Color color) {
		return String.format("#%02X%02X%02X",
	            (int)(color.getRed() * 255),
	            (int)(color.getGreen() * 255),
	            (int)(color.getBlue() * 255));
	}
}
