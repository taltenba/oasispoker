package com.oasisinfinity.utils;

public class PlatformUtil {

	private PlatformUtil() { /* Empty */ }

	public static String getOSInfos() {
		return System.getProperty ("os.name") + ", " + System.getProperty ("os.version") + ", " + System.getProperty ("os.arch");
	}
}
