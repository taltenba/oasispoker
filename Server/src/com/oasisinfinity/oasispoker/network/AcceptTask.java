package com.oasisinfinity.oasispoker.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;

import javax.swing.event.EventListenerList;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.listeners.AcceptListener;

import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class AcceptTask {
	private final ExecutorService _exec;
	private final ServerSocket _serverSocket;
	private final EventListenerList _listeners;
	private final AtomicBoolean _isRunning;
	private final Runnable _task;
	
	public AcceptTask(int port, ExecutorService exec) throws IOException {
		_serverSocket = new ServerSocket(port);
		_exec = exec;
		_listeners = new EventListenerList();
		_isRunning = new AtomicBoolean(false);
		
		_task = () -> run();
	}
	
	public void start() {
		if (!_isRunning.compareAndSet(false, true))
			throw new IllegalStateException("Already started");

		//new Thread(this, "AcceptThread").start();
		_exec.execute(_task);
	}	
	
	private void run() {
		try {
			while (true) {
				Client.Builder clientBuilder = new Client.Builder(_serverSocket.accept());
				Client client = null;
				
				try {
					client = clientBuilder.build();
				} catch (IOException | IllegalStateException e) {
					BaseApp.logException(Level.WARNING, e);
					continue;
				}
					
				ClientTask task = new ClientTask(client);
				
				_exec.execute(task);
				
				clientAccepted(client, task);
			}
		} catch (IOException e) {
			// Socket is closed
			
			//BaseApp.logException(Level.INFO, e);
			
			try {
				cancelIfRunning();
			} catch (IOException e1) {
				//BaseApp.logException(Level.INFO, e1);
			}
		}
	}
	
	public void cancel() throws IOException {
		if (!_isRunning.compareAndSet(true, false))
			throw new IllegalStateException("The thread is not running");
		
		_serverSocket.close();
	}
	
	public void cancelIfRunning() throws IOException {
		if (!_isRunning.compareAndSet(true, false))
			return;
		
		_serverSocket.close();
	}
	
	public boolean isRunning() {
		return _isRunning.get();
	}
	
	public void addAcceptListener(AcceptListener listener) {
		_listeners.add(AcceptListener.class, listener);
	}
	
	public void removeAcceptListener(AcceptListener listener) {
		_listeners.remove(AcceptListener.class, listener);
	}
	
	private void clientAccepted(Client c, ClientTask task) {
		for (AcceptListener listener : _listeners.getListeners(AcceptListener.class))
			listener.clientAccepted(c, task);
	}
}
