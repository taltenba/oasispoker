package com.oasisinfinity.oasispoker.network;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.oasisinfinity.oasispoker.network.Message.Reason;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

/**
 * The only lock used is {@code this}
 * @author OasisInfinity
 * @date 20 juil. 2016
 */
@ThreadSafe
public final class ClientsSet {
	private final Set<Client> _clients;
	private final int _maxClientsCount;
	private final int _maxPlayersCount;
	private final int _maxSpectatorsCount;

	@GuardedBy("this") private int _playersCount;
	@GuardedBy("this") private int _spectatorsCount;
	@GuardedBy("this") private boolean _acceptPlayers;
	
	public ClientsSet(int maxPlayersCount, int maxSpectatorsCount) {
		_clients = new HashSet<Client>();
		_maxClientsCount = maxPlayersCount + maxSpectatorsCount;
		_maxPlayersCount = maxPlayersCount;
		_maxSpectatorsCount = maxSpectatorsCount;
		
		synchronized (this) {
			_acceptPlayers = true;
		}
	}
	
	public synchronized AddResult addClient(Client c) {
		if (_clients.contains(c))
			return AddResult.newUnsuccessfulResult(Reason.IdenticalNames, getInfos());
		
		boolean isSpectator = c.isSpectator();
		
		if (!isSpectator && !_acceptPlayers)
			return AddResult.newUnsuccessfulResult(Reason.GameAlreadyStarted, getInfos());
		
		if ((isSpectator && _spectatorsCount >= _maxSpectatorsCount) || (!isSpectator && _playersCount >= _maxPlayersCount))
			return AddResult.newUnsuccessfulResult(Reason.ServerFull, getInfos());

		_clients.add(c);
		
		if (isSpectator)
			++_spectatorsCount;
		else
			++_playersCount;
		
		if (_playersCount == _maxPlayersCount)
			_acceptPlayers = false;
		
		return AddResult.newSuccessfulResult(getInfos());
	}
	
	public synchronized boolean removeClient(Client c) {
		boolean removed = _clients.remove(c);
		
		if (removed) {
			if (c.isSpectator())
				--_spectatorsCount;
			else
				--_playersCount;
		}
		
		return removed;
	}
	
	public synchronized ChangeResult removeClientAndGetInfos(Client c) {
		return new ChangeResult(removeClient(c), getInfos());
	}
	
	public synchronized Client[] getClientsCopy() {
		return _clients.toArray(new Client[_clients.size()]);
	}

	public Set<Client> getClientsUnmodifiable() {
		return Collections.unmodifiableSet(_clients);
	}
	
	public synchronized int getPlayersCount() {
		return _playersCount;
	}
	
	public synchronized int getSpectatorsCount() {
		return _spectatorsCount;
	}
	
	public synchronized int getClientsCount() {
		return _clients.size();
	}
	
	public synchronized ClientsInfos getInfos() {
		return new ClientsInfos(_clients.size(), _playersCount, _spectatorsCount);
	}
	
	/**
	 * ThreadSafe
	 * @return
	 */
	public int getMaxPlayersCount() {
		return _maxPlayersCount;
	}
	
	/**
	 * ThreadSafe
	 * @return
	 */
	public int getMaxSpectatorsCount() {
		return _maxSpectatorsCount;
	}
	
	/**
	 * ThreadSafe
	 * @return
	 */
	public int getMaxClientsCount() {
		return _maxClientsCount;
	}
	
	@Immutable
	public static class ChangeResult {
		private final boolean _isSuccessful;
		private final ClientsInfos _clientsInfos;
		
		public ChangeResult(boolean isSuccessful, ClientsInfos clientsInfos) {
			_isSuccessful = isSuccessful;
			_clientsInfos = clientsInfos;
		}
		
		public boolean isSuccessful() {
			return _isSuccessful;
		}
		
		public ClientsInfos getClientsInfos() {
			return _clientsInfos;
		}
	}
	
	@Immutable
	public static class AddResult extends ChangeResult {
		private final Reason _rejectionReason;
		
		private AddResult(boolean isSuccessful, Reason rejectionReason, ClientsInfos clientsInfos) {
			super(isSuccessful, clientsInfos);
			
			_rejectionReason = rejectionReason;
		}
		
		public Reason getRejectionReason() {
			return _rejectionReason;
		}

		public static AddResult newSuccessfulResult(ClientsInfos clientsInfos) {
			return new AddResult(true, null, clientsInfos);
		}
		
		public static AddResult newUnsuccessfulResult(Reason reason, ClientsInfos clientsInfos) {
			return new AddResult(false, reason, clientsInfos);
		}
	}
	
	@Immutable
	public static class ClientsInfos {
		private final int _clientsCount;
		private final int _playersCount;
		private final int _spectatorsCount;
		
		public ClientsInfos(int clientsCount, int playersCount, int spectatorsCount) {
			_clientsCount = clientsCount;
			_playersCount = playersCount;
			_spectatorsCount = spectatorsCount;
		}

		public int getClientsCount() {
			return _clientsCount;
		}

		public int getPlayersCount() {
			return _playersCount;
		}

		public int getSpectatorsCount() {
			return _spectatorsCount;
		}
	}
}
