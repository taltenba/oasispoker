package com.oasisinfinity.oasispoker.network;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.engines.GameEngineInterface;
import com.oasisinfinity.oasispoker.engines.ServerGameEngine;
import com.oasisinfinity.oasispoker.listeners.AcceptListener;
import com.oasisinfinity.oasispoker.listeners.GameProgressListener;
import com.oasisinfinity.oasispoker.listeners.NewMessageListener;
import com.oasisinfinity.oasispoker.listeners.ServerStateListener;
import com.oasisinfinity.oasispoker.models.ActionInfos;
import com.oasisinfinity.oasispoker.models.BustedPlayerInfos;
import com.oasisinfinity.oasispoker.models.Card;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.IPInfos;
import com.oasisinfinity.oasispoker.models.NewBoardCardsInfos;
import com.oasisinfinity.oasispoker.models.NewPosInfos;
import com.oasisinfinity.oasispoker.models.PlayerInfos;
import com.oasisinfinity.oasispoker.models.PlayerRank;
import com.oasisinfinity.oasispoker.models.ServerInfos;
import com.oasisinfinity.oasispoker.network.ClientsSet.AddResult;
import com.oasisinfinity.oasispoker.network.ClientsSet.ChangeResult;
import com.oasisinfinity.oasispoker.network.ClientsSet.ClientsInfos;
import com.oasisinfinity.oasispoker.network.Message.Instruction;
import com.oasisinfinity.oasispoker.network.Message.Reason;
import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.concurrent.ConcurrentUtil;
import com.oasisinfinity.utils.io.IOUtil;
import com.oasisinfinity.utils.javafx.FxThreadExecutor;

import javafx.application.Platform;
import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

@ThreadSafe
public class GameServer implements AcceptListener, NewMessageListener, GameProgressListener {
	private static final int COMPATIBILITY_VERSION = 1;
	
	private final int _port;
	private final GameConfig _gameConfig;
	private final ServerGameEngine.Builder _engineBuilder;
	private final AtomicBoolean _isRunning;
	private final AtomicInteger _nonReadyPlayers;
	private final ClientsSet _clients;
	private final AcceptTask _acceptTask;
	
	private volatile ServerGameEngine _engine;
	
	@GuardedBy("_engineBuilder") private boolean _engineBuilded;
	
	/* MUST BE CONFINED IN FxThread */
	private GameEngineInterface _engineInterface;
	
	private final ExecutorService _clientsListenersExec;
	private final ExecutorService _clientsSenderExec;
	private final List<ServerStateListener> _listeners;
	
	public GameServer(int port, int maxSpectators, GameConfig config) throws IOException {
		_port = port;
		_gameConfig = config;
		_engineBuilder = new ServerGameEngine.Builder(config);
		_isRunning = new AtomicBoolean();
		_listeners = new CopyOnWriteArrayList<>();
		
		_clientsListenersExec = ConcurrentUtil.newCachedCrashHandlerThreadPool(new BasicThreadFactory("GameServer - ListenerThreads"), BaseApp.FX_CRASH_HANDLER);
		_clientsSenderExec = ConcurrentUtil.newFixedCrashHandlerThreadPoolExecutor(1, new BasicThreadFactory("GameServerThread - SenderThread"), BaseApp.FX_CRASH_HANDLER);
		
		_acceptTask = new AcceptTask(_port, _clientsListenersExec);
		_acceptTask.addAcceptListener(this);
		
		int maxPlayers = config.getTotalPlayersCount() - config.getBotsCount();
		
		_nonReadyPlayers = new AtomicInteger(maxPlayers);
		_clients = new ClientsSet(maxPlayers, maxSpectators);
	}
	
	public void startServer() {
		if (!_isRunning.compareAndSet(false, true))
			throw new IllegalStateException("Server has already started");
		
		if (_clientsListenersExec.isShutdown())
			throw new IllegalStateException("Server cannot be restarted");
		
		//_engine.addPlayerActionListener(this);
			
		_acceptTask.start();

		onServerStart(new IPInfos());
	}
	
	public void stopServer() {
		if (!_isRunning.get())
			return;
		
		_clientsSenderExec.shutdown();
		_clientsListenersExec.shutdown();
		
		if (!_isRunning.compareAndSet(true, false))
			return;
		
		try {
			_acceptTask.cancelIfRunning();
		} catch (IOException e) {
			BaseApp.logException(Level.WARNING, e);
		}
		
		if (_engine != null)
			_engine.shutdown();
		
		synchronized (_clients) {
			Client[] clients = _clients.getClientsCopy();

			for (Client c : clients) {
				_clients.removeClient(c);
				disconnectClient(c);
				
				if (c.isSpectator())
					onSpectatorDisconnected(c.getName(), _clients.getSpectatorsCount());
				else
					onPlayerDisconnected(c.getName(), _clients.getPlayersCount());	
			}
		}
		
		onServerStop();
	}
	
	private void buildEngine() {
		synchronized (_engineBuilder) {
			if (_engineBuilded)
				return;
			
			_engine = _engineBuilder.build();
			_engineBuilded = true;
		}
		
		Platform.runLater(() -> {
			_engineInterface = _engine.addEngineInterface(FxThreadExecutor.getInstance(), this);
			
			_engineInterface.getGameInfosProperty().addListener((observable, oldValue, newValue) -> {
				sendToAll(new Message(Instruction.NewGameInfos, newValue));
			});

			sendToAll(new Message(Instruction.Ready, _engineInterface.getGameInfos()));
		});
	}
	
	public boolean isRunning() {
		return _isRunning.get();
	}
	
	private void sendToAll(final Message msg) {
		if (_clientsSenderExec.isShutdown())
			return;
		
		_clientsSenderExec.execute(() -> {
			synchronized (_clients) {
				Set<Client> clients = _clients.getClientsUnmodifiable();
				
				for (Client c : clients)
					c.write(msg.toString());
			}
		});
	}
	
	private void sendTo(Client c, Message msg) {
		c.write(msg.toString());
	}
	
	private void disconnectClient(Client client) {
		try {
			client.disconnect();
		} catch (IOException e) {
			BaseApp.logException(Level.WARNING, e);
		}
	}
	
	private void rejectConnection(Client client, Reason reason) {
		sendTo(client, new Message(Instruction.ConnectionRejected, reason.name()));
	}
	
	private boolean addClient(Client client) {
		if (client.getCompatibilityVersion() != COMPATIBILITY_VERSION) {
			rejectConnection(client, Reason.Incompatible);
			return false;
		}
		
		boolean isPlayer = !client.isSpectator();
		
		AddResult result = _clients.addClient(client);
		
		if (!result.isSuccessful()) {
			rejectConnection(client, result.getRejectionReason());
			return false;
		}
		
		ClientsInfos clientsInfos = result.getClientsInfos();

		if (isPlayer) {
			_engineBuilder.addHumanPlayer(client.getName(), client.getStats());
			onPlayerConnected(client.getName(), clientsInfos.getPlayersCount());
		} else
			onSpectatorConnected(client.getName(), clientsInfos.getSpectatorsCount());
		
		if (clientsInfos.getPlayersCount() == _clients.getMaxPlayersCount())
			buildEngine();
		
		return true;
	}
	
	private void removeClient(Client client) {
		ChangeResult result = _clients.removeClientAndGetInfos(client);
		
		if (!result.isSuccessful())
			return;
		
		boolean isPlayer = !client.isSpectator();
		ClientsInfos clientsInfos = result.getClientsInfos();
		
		if (isPlayer) {
			if (client.isReady())
				_nonReadyPlayers.incrementAndGet();

			onPlayerDisconnected(client.getName(), clientsInfos.getPlayersCount());
		} else
			onSpectatorDisconnected(client.getName(), clientsInfos.getSpectatorsCount());
		
		synchronized (_engineBuilder) {
			if (isPlayer && !_engineBuilded)
				_engineBuilder.removeHumanPlayer(client.getName());
			
			if (_engineBuilded) {
				if (isPlayer)
					_engine.setPlayerDisconnected(client.getName());
				
				if (clientsInfos.getClientsCount() == 0) {
					stopServer();
					return;
				}
			}
		}
		
		disconnectClient(client);
	}
	
	@Override
	public void clientAccepted(Client c, ClientTask task) {
		if (addClient(c)) {
			task.addNewMessageListener(this);
			task.addDisconnectedListener(client -> removeClient(client));
			
			if (!c.isConnected()) //If client had been disconnected before the disconnected listener has been added
				removeClient(c);
			
			sendTo(c, new Message(Instruction.ConnectionAccepted, new ServerInfos(_gameConfig, _clients.getPlayersCount(), _clients.getMaxPlayersCount())));
		} else
			disconnectClient(c);
	}
	
	@Override
	public void newMessage(Client c, Message msg) {
		switch (msg.getInstruction()) {
			case Ready:
				if (!c.isSpectator() && _nonReadyPlayers.decrementAndGet() == 0)
					_engine.startGame();
				break;
			case NewAction:
				play(IOUtil.loadJson(msg.getArg(), ActionInfos.class));
				break;
			case NewChatMessage:
				sendToAll(msg);
				break;
			default:
				throw new AssertionError();
		}
	}
	
	private void play(ActionInfos actionInfos) {
		Platform.runLater(() -> {
			if (!_engineInterface.isCurrentPlayer(actionInfos.getFromName()))
				return;
			
			try {
				switch(actionInfos.getAction()) {
					case Fold:
						_engine.fold();
						break;
					case Check:
						_engine.check();
						break;
					case Call:
						_engine.call();
						break;
					case Raise:
						_engine.raise(actionInfos.getAmount());
						break;
					default:
						throw new AssertionError();
				}
			} catch (IllegalStateException e) {
				BaseApp.logException(Level.INFO, e);
			}
		});
	}
	
	/*@Override
	public void newAction(String playerName, Action a, int amount) {
		Client player = null;
		
		for (Client c : _clients) {
			if (c.getName().equals(playerName)) {
				player = c;
				break;
			}
		}
		
		sendToOther(player, new Message(Instruction.valueOf(a.toString()), amount == 0 ? null : String.valueOf(amount)));
	}*/
	
	public void addStateListener(ServerStateListener listener) {
		_listeners.add(listener);
	}
	
	public void removeStateListener(ServerStateListener listener) {
		_listeners.remove(listener);
	}
	
	private void onServerStart(final IPInfos ips) {
		for (ServerStateListener listener : _listeners)
			listener.started(ips, _port);
	}
	
	private void onServerStop() {
		for (ServerStateListener listener : _listeners)
			listener.stopped();
	}
	
	private void onPlayerConnected(String playerName, int playersCount) {
		sendToAll(new Message(Instruction.NewPlayersCount, String.valueOf(playersCount)));
		
		for (ServerStateListener listener : _listeners)
			listener.playerConnected(playerName, playersCount, _clients.getMaxPlayersCount());
	}
	
	private void onPlayerDisconnected(String playerName, int remainingPlayers) {
		sendToAll(new Message(Instruction.NewPlayersCount, String.valueOf(remainingPlayers)));
		
		for (ServerStateListener listener : _listeners)
			listener.playerDisconnected(playerName, remainingPlayers, _clients.getMaxPlayersCount());
	}
	
	private void onSpectatorConnected(String spectatorName, int spectatorsCount) {
		for (ServerStateListener listener : _listeners)
			listener.spectatorConnected(spectatorName, spectatorsCount, _clients.getMaxSpectatorsCount());
	}
	
	private void onSpectatorDisconnected(String spectatorName, int remainingSpectators) {
		for (ServerStateListener listener : _listeners)
			listener.spectatorDisconnected(spectatorName, remainingSpectators, _clients.getMaxSpectatorsCount());
	}
	
	@Override
	public void gameStarted() {
		sendToAll(Message.MSG_GAME_STARTED);
	}

	@Override
	public void roundStarted(boolean isFirstRound) {
		sendToAll(new Message(Instruction.RoundStarted, isFirstRound));
	}

	@Override
	public void turnStarted(int firstPlayerPos) {
		sendToAll(new Message(Instruction.TurnStarted, firstPlayerPos));
	}

	@Override
	public void nextPos(PlayerInfos p, int oldPos, int newPos) {
		sendToAll(new Message(Instruction.NextPos, new NewPosInfos(p, oldPos, newPos)));
	}

	@Override
	public void turnCompleted(int lastPlayerPos) {
		sendToAll(new Message(Instruction.TurnCompleted, lastPlayerPos));
	}

	@Override
	public void roundCompleted(boolean onlyOneNonFoldedPlayer) {
		sendToAll(new Message(Instruction.RoundCompleted, onlyOneNonFoldedPlayer));
	}

	@Override
	public void gameOver(List<PlayerRank> rankings) {
		sendToAll(new Message(Instruction.GameOver, rankings));
	}

	@Override
	public void newBoardCards(int addIndex, List<Card> newCards) {
		sendToAll(new Message(Instruction.NewBoardCards, new NewBoardCardsInfos(addIndex, newCards)));
	}

	@Override
	public void boardCleared() {
		sendToAll(Message.MSG_BOARD_CLEARED);
	}

	@Override
	public void playerBusted(int pos, String name) {
		sendToAll(new Message(Instruction.PlayerBusted, new BustedPlayerInfos(pos, name)));
	}
}
