package com.oasisinfinity.oasispoker.controllers;

import com.oasisinfinity.controls.BorderedTitledPane;
import com.oasisinfinity.controls.NoDelayTooltip;
import com.oasisinfinity.oasispoker.ServerApp;
import com.oasisinfinity.oasispoker.models.GameConfig;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;

public class ServerConfigurationController extends BaseController {
	@FXML private Label _initialSmallBlindLabel;
	@FXML private Label _initialAntesLabel;
	@FXML private Label _portLabel;
	@FXML private Spinner<Integer> _totalPlayersCountSpinner;
	@FXML private Spinner<Integer> _botsCountSpinner;
	@FXML private Spinner<Integer> _initialCashSpinner;
	@FXML private Spinner<Integer> _initialSmallBlindSpinner;
	@FXML private Spinner<Integer> _initialAntesSpinner;
	@FXML private Spinner<Integer> _blindsIncreaseFrequencySpinner;
	@FXML private Spinner<Integer> _antesIncreaseFrequencySpinner;
	@FXML private Spinner<Integer> _blindsIncreaseMultiplierSpinner;
	@FXML private Spinner<Integer> _antesIncreaseMultiplierSpinner;
	@FXML private Spinner<Integer> _antesDelaySpinner;
	@FXML private Spinner<Integer> _playingTimeSpinner;
	@FXML private Spinner<Integer> _maxAfkDurationSpinner;
	@FXML private Spinner<Integer> _portSpinner;
	@FXML private Spinner<Integer> _maxSpectatorsSpinner;
	@FXML private Button _startButton;
	@FXML private BorderedTitledPane _gameConfigPane;
	@FXML private BorderedTitledPane _serverConfigPane;
	@FXML private HBox _configPane;
	
	public ServerConfigurationController() {
		super();
	}

	@FXML
	@Override
	protected void initialize() {
		_gameConfigPane.setTitle("Partie");
		_serverConfigPane.setTitle("Serveur");
		
		_initialSmallBlindLabel.setTooltip(new NoDelayTooltip(_initialSmallBlindLabel, "La valeur de la grosse blind sera le double de celle-ci"));
		_initialAntesLabel.setTooltip(new NoDelayTooltip(_initialAntesLabel, "Les antes sont des mises initiales obligatoires pour tous les joueurs"));
		_portLabel.setTooltip(new NoDelayTooltip(_portLabel, "Veillez � ce que le port soit disponible et ouvert pour tous vos pare-feux dans l'optique d'une connection TCP"));
		
		_totalPlayersCountSpinner.valueProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue <= _botsCountSpinner.getValue())
				_botsCountSpinner.getValueFactory().setValue(newValue - 1);
		});
		
		_botsCountSpinner.valueProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue >= _totalPlayersCountSpinner.getValue())
				_totalPlayersCountSpinner.getValueFactory().setValue(newValue + 1);
		});
		
		_startButton.setOnAction(e -> {
			_startButton.setDisable(true);
			
			((ServerInfosController) getMainApp().getController(ServerApp.SERVER_INFOS_SCENE))
				.updateConfig(_portSpinner.getValue(), _maxSpectatorsSpinner.getValue(), getGameConfig());
			
			goToNewScene(ServerApp.SERVER_INFOS_SCENE);
		});
	}
	
	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		_startButton.setDisable(false);
	}
	
	private GameConfig getGameConfig() {
		int botsCount = _botsCountSpinner.getValue();
		
		return new GameConfig.Builder()
				.setHumanPlayersCount(_totalPlayersCountSpinner.getValue() - botsCount)
				.setBotsCount(botsCount)
				.setInitialCash(_initialCashSpinner.getValue())
				.setInitialSmallBlind(_initialSmallBlindSpinner.getValue())
				.setInitialAntes(_initialAntesSpinner.getValue())
				.setBlindsIncreaseFrequency(_blindsIncreaseFrequencySpinner.getValue())
				.setAntesIncreaseFrequency(_antesIncreaseFrequencySpinner.getValue())
				.setBlindsIncreaseMultiplier(_blindsIncreaseMultiplierSpinner.getValue())
				.setAntesIncreaseMultiplier(_antesIncreaseMultiplierSpinner.getValue())
				.setAntesDelay(_antesDelaySpinner.getValue())
				.setPlayingTime(_playingTimeSpinner.getValue())
				.setMaxAfkDuration(_maxAfkDurationSpinner.getValue())
				.build();
	}
}
