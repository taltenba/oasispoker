package com.oasisinfinity.oasispoker.controllers;

import java.io.IOException;
import java.net.BindException;

import com.oasisinfinity.oasispoker.BaseApp;
import com.oasisinfinity.oasispoker.ServerApp;
import com.oasisinfinity.oasispoker.listeners.ServerStateListener;
import com.oasisinfinity.oasispoker.models.GameConfig;
import com.oasisinfinity.oasispoker.models.IPInfos;
import com.oasisinfinity.oasispoker.network.GameServer;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.WindowEvent;

public class ServerInfosController extends BaseController implements ServerStateListener {
	private static final String STARTING_MSG = "----- SERVEUR D�MARR� -----\n"
			+ "IPs :%n"
			+ "   LOCALHOST: %s%n"
			+ "   LOCALE: %s%n"
			+ "   PUBLIQUE: %s%n"
			+ "PORT: %d%n"
			+ "---------------------------%n"
			+ "En attente de connexion...%n";
	
	private static final String START_ERROR_MSG = "----- ERREUR -----%n"
			+ "Impossible de d�marrer le serveur%n"
			+ "Raison : %s%n"
			+ "-----------------%n";

	
	@FXML private TextArea _logArea;
	@FXML private Button _stopButton;
	
	private GameServer _server;
	
	private final EventHandler<WindowEvent> _closeEvent = new EventHandler<WindowEvent>() {
		@Override
		public void handle(WindowEvent e) {
			if (_server != null && _server.isRunning()) {
				if (!showQuitConfirmationDialog())
					e.consume();
				else
					_server.stopServer();
			}
		}
	};
	
	public ServerInfosController() {
		super();
	}
	
	@FXML
	@Override
	protected void initialize() {
		_stopButton.setOnMouseClicked(e -> {
			if (_stopButton.getText().equals("Retour")) {
				goToNewScene(ServerApp.SERVER_CONFIGURATION_SCENE);
				return;
			}
			
			if (!showQuitConfirmationDialog())
				return;
			
			if (_server.isRunning())
				_server.stopServer();
		});
	}

	public void updateConfig(int port, int maxSpectators, GameConfig config) {
		if (_server != null && _server.isRunning())
			_server.stopServer();
		
		try {
			_server = new GameServer(port, maxSpectators, config);
			
			_server.addStateListener(this);
			_server.startServer();
		} catch (IOException e) {
			//BaseApp.logException(Level.INFO, e);
			
			if (e instanceof BindException)
				logStartError("Le port n'est pas disponible (" + e.getMessage() + ")");
			else
				logStartError(e.getLocalizedMessage());
		}
	}
	
	private void logStartError(String msg) {
		_logArea.appendText(String.format(START_ERROR_MSG, msg));
		
		stopped();
	}
	
	@Override
	public void onNavigateTo() {
		super.onNavigateTo();
		
		getMainApp().getStage().addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
		
		_stopButton.setText("Arr�ter le serveur");
	}
	
	@Override
	protected boolean onNavigateFrom() {	
		_logArea.clear();
		
		getMainApp().getStage().removeEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, _closeEvent);
		
		return true;
	}
	
	@Override
	public void started(IPInfos ips, int port) {
		Platform.runLater(() -> {
			_logArea.clear();
			
			_logArea.appendText(String.format(STARTING_MSG,
					IPInfos.getLocalHostAddress().getHostAddress(),
					ips.getLocalAddress().getHostAddress(),
					(ips.getPublicAddress() == null ? "INCONNUE" : ips.getPublicAddress().getHostAddress()),
					port));
		});
	}

	@Override
	public void stopped() {
		Platform.runLater(() -> {
			_logArea.appendText("------ SERVEUR ARR�T� ------\n");
			_server = null;
			
			_stopButton.setText("Retour");
		});
	}

	@Override
	public void playerConnected(String playerName, int playersCount, int maxPlayers) {
		Platform.runLater(() -> {
			_logArea.appendText("Joueur connect� : " + playerName + " (" + playersCount + "/" + maxPlayers + ")\n");
			
			if (playersCount == maxPlayers)
				_logArea.appendText("D�marrage de la partie, en attente des joueurs...\n");
		});
	}

	@Override
	public void playerDisconnected(String playerName, int remainingPlayers, int maxPlayers) {
		Platform.runLater(() -> {
			_logArea.appendText("Joueur d�connect� : " + playerName + " (" + remainingPlayers + "/" + maxPlayers + ")\n");
		});
	}
	
	@Override
	public void spectatorConnected(String spectatorName, int spectatorsCount, int maxSpectators) {
		Platform.runLater(() -> {
			_logArea.appendText("Spectateur connect� : " + spectatorName + " (" + spectatorsCount + "/" + maxSpectators + ")\n");
		});
	}

	@Override
	public void spectatorDisconnected(String spectatorName, int remainingSpectators, int maxSpectators) {
		Platform.runLater(() -> {
			_logArea.appendText("Spectateur d�connect� : " + spectatorName + " (" + remainingSpectators + "/" + maxSpectators + ")\n");
		});
	}
	
	private boolean showQuitConfirmationDialog() {
		return BaseApp.ALERT_UTIL.showQuitConfirmationMsg("Voulez-vous r�ellement arr�ter le serveur ?");
	}
}
