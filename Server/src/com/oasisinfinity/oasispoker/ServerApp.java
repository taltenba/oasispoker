package com.oasisinfinity.oasispoker;

import java.io.IOException;

import javafx.stage.Stage;

public class ServerApp extends BaseApp {
	public static final String
		SERVER_CONFIGURATION_SCENE = "ServerConfiguration",
		SERVER_INFOS_SCENE = "ServerInfos";
	
	public ServerApp() {
		super(SERVER_CONFIGURATION_SCENE);
	}
	
	@Override
	public void start(Stage primaryStage) {
		super.start(primaryStage, "OasisPoker - Serveur", true);
	}

	@Override
	protected void loadScenes() throws IOException {
		loadScene(SERVER_CONFIGURATION_SCENE);
		loadScene(SERVER_INFOS_SCENE);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
