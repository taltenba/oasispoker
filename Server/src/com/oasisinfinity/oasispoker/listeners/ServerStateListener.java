package com.oasisinfinity.oasispoker.listeners;

import java.util.EventListener;

import com.oasisinfinity.oasispoker.models.IPInfos;

public interface ServerStateListener extends EventListener {
	public void started(IPInfos ips, int port);
	public void stopped();
	public void playerConnected(String playerName, int playersCount, int maxPlayers);
	public void playerDisconnected(String playerName, int remainingPlayers, int maxPlayers);
	public void spectatorConnected(String spectatorName, int spectatorsCount, int maxSpectators);
	public void spectatorDisconnected(String spectatorName, int remainingSpectators, int maxSpectators);
}
