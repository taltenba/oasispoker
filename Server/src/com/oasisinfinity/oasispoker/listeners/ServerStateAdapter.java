package com.oasisinfinity.oasispoker.listeners;

import com.oasisinfinity.oasispoker.models.IPInfos;

public abstract class ServerStateAdapter implements ServerStateListener {
	@Override
	public void started(IPInfos ips, int port) { /* Empty */ }

	@Override
	public void stopped() { /* Empty */ }
	
	@Override
	public void playerConnected(String playerName, int playersCount, int maxPlayers) { /* Empty */ }
	
	@Override
	public void playerDisconnected(String playerName, int remainingPlayers, int maxPlayers) { /* Empty */ }
	
	@Override
	public void spectatorConnected(String spectatorName, int spectatorsCount, int maxSpectators) { /* Empty */ }
	
	@Override
	public void spectatorDisconnected(String spectatorName, int remainingSpectators, int maxSpectators) { /* Empty */ }
}
