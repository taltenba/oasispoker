package com.oasisinfinity.oasispoker.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.logging.Level;

import com.oasisinfinity.oasispoker.BaseApp;

import net.jcip.annotations.Immutable;

@Immutable
public class IPInfos {
	private final InetAddress _publicAddr;
	private final InetAddress _localAddr;
	
	public IPInfos() {
		_publicAddr = checkPublicAddress();
		
		InetAddress local = null;
		
		try {
			 local = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			BaseApp.logException(Level.INFO, e);
		}
		
		_localAddr = local;
	}
	
	public IPInfos(InetAddress publicAddr, InetAddress localAddr) {
		_publicAddr = publicAddr;
		_localAddr = localAddr;
	}
	
	public InetAddress getPublicAddress() {
		return _publicAddr;
	}
	
	public InetAddress getLocalAddress() {
		return _localAddr;
	}

	public static InetAddress getLocalHostAddress() {
		try {
			return InetAddress.getByName("127.0.0.1");
		} catch (UnknownHostException e) {
			BaseApp.logException(Level.INFO, e);
		}
		
		return null;
	}
	
	public static InetAddress checkPublicAddress() {
		URL aws;
		BufferedReader in = null;
		
		try {
			aws = new URL("http://checkip.amazonaws.com");
			in = new BufferedReader(new InputStreamReader(aws.openStream()));
			
			return InetAddress.getByName(in.readLine());
		} catch (IOException e) {
			BaseApp.logException(Level.INFO, e);
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (IOException e) {
				BaseApp.logException(Level.INFO, e);
			}
		}
		
		return null;
	}
}
