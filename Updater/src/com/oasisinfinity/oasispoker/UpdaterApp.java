package com.oasisinfinity.oasispoker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;

import com.oasisinfinity.utils.BasicThreadFactory;
import com.oasisinfinity.utils.concurrent.SingleAppInstanceLock;

import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class UpdaterApp extends BaseApp {
	public static final String UPDATE_SUCCESS = "UpdateSuccess";
	public static final String UPDATER_MAIN_SCENE = "UpdaterMain";
	
	private static volatile String[] ARGS;
	
	public UpdaterApp() {
		super(UPDATER_MAIN_SCENE);
	}
	
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setResizable(false);
		primaryStage.initStyle(StageStyle.UNDECORATED);
		
		super.start(primaryStage, "OasisPoker - Updater", false);
		
		update(ARGS);
	}
	
	@Override
	protected void loadScenes() throws IOException {
		loadScene(UPDATER_MAIN_SCENE, false);
	}
	
	private static void update(final String[] args) {
		BasicThreadFactory.createNewThread(() -> {
			if (args.length < 2) {
				showErrorAndExit("Arguments invalides", "Les arguments doivent �tre au nombre de deux");
				return;
			}
				
			Path downloadedFilePath = Paths.get(args[0]);
			
			if (!Files.exists(downloadedFilePath)) {
				showErrorAndExit("Fichier introuvable", "Le fichier de mise � jour n'existe pas.");
				return;
			}
			
			SingleAppInstanceLock lock = null;
			
			try {
				lock = new SingleAppInstanceLock("OasisPoker - Launcher");
				lock.waitForLock();
			} catch (IOException e) {
				BaseApp.logException(Level.SEVERE, e);
				showErrorAndExit("Cr�ation du v�rrou impossible", "Impossible de cr�er un v�rrou exclusif. Consultez les logs pour plus de d�tails.");
				return;
			}
			
			try {
				for (int i = 0; i < 10; ++i) { //If file is still locked, retry
					try {
						Files.move(downloadedFilePath, Paths.get(args[1]), StandardCopyOption.REPLACE_EXISTING);
						break;
					} catch (IOException e) { 
						if (i == 9)
							throw e;
						else
							logException(Level.INFO, e);
					}
						
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						logException(Level.INFO, e);
					}
				}
				
				lock.unlock();
				
				Runtime.getRuntime().exec("java -jar \"" + args[1] + "\" " + UPDATE_SUCCESS);
			} catch (IOException e) {
				logException(Level.SEVERE, e);
				showErrorAndExit("Impossible de proc�der � la mise � jour", "La mise � jour n'a pas pu �tre effectu�e. Consultez les logs pour plus de d�tails.");
			}
			
			Platform.runLater(() -> Platform.exit());
		}, "UpdateThread", BaseApp.FX_CRASH_HANDLER).start(); 
	}
	
	private static void showErrorAndExit(String msgHeader, String msgContent) {
		logMsg(Level.SEVERE, msgHeader + " : " + msgContent);
		
		Platform.runLater(() -> {
			BaseApp.ALERT_UTIL.showErrorMsg(msgHeader, msgContent);
			Platform.exit();
		});
	}
	
	public static void main(String[] args) {
		ARGS = args;
		launch(args);
	}
}
